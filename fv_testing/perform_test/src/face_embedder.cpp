#include "../include/face_embedder.hpp"

face_embedder::face_embedder()
{
    try
    {
        /* set default implementations for the interfaces */
        set_imagery_handler(std::move(std::make_unique<io_image>()));
        set_face_detector(std::move(std::make_unique<HOG>()));
        set_embedder(std::move(std::make_unique<DNN_ResNet>()));

    }
    catch (const std::exception &e)
    {
    }
}

face_embedder::~face_embedder()
{
}

// dummy function
dlib::matrix<dlib::rgb_pixel> face_embedder::get_cropped_image()
{
    return _face_detector->get_image_cropped_face();
}

face_embedder::face_embedder(const std::string &input)
{
    try
    {
        /* set default implementations for the interfaces */
        set_imagery_handler(std::move(std::make_unique<io_image>()));
        set_face_detector(std::move(std::make_unique<HOG>()));
        set_embedder(std::move(std::make_unique<DNN_ResNet>()));

        // load webcam (and capture frame immediately) or image depending on chosen implementation
        load_imagery(input);

    }
    catch (const std::exception &e)
    {
    }
}

dlib::matrix<dlib::rgb_pixel> face_embedder::load_imagery(const std::string &input)
{
    // load webcam (and capture frame immediately) or image depending on chosen implementation
    _imagery_handler->load(input);
    detect_face();
    return _imagery_handler->get_image();
}

dlib::matrix<float, 0, 1> face_embedder::embed(const std::string &input)
{
    load_imagery(input);
    return _embedder->embed_face(_face_detector->get_image_cropped_face());
}

dlib::matrix<dlib::rgb_pixel> face_embedder::detect_face()
{
    return _face_detector->detect_face(_imagery_handler->get_image());
}

dlib::matrix<float, 0, 1> face_embedder::embed_face()
{
    return _embedder->embed_face(_face_detector->get_image_cropped_face());
}

dlib::matrix<float, 0, 1> face_embedder::get_face_embedding()
{
    return _embedder->get_face_embedding();
}

void face_embedder::operator<<(dlib::matrix<dlib::rgb_pixel> set_img)
{
    _face_detector->set_image(set_img);
}

void face_embedder::operator>>(dlib::matrix<float, 0, 1> &get_emb)
{
    get_emb = _embedder->get_face_embedding();
}

dlib::matrix<float, 0, 1> face_embedder::operator()()
{
    return _embedder->get_face_embedding();
}

dlib::matrix<float, 0, 1> face_embedder::operator()(const std::string &input)
{
    load_imagery(input);
    return _embedder->embed_face(_face_detector->get_image_cropped_face());
}

void face_embedder::set_imagery_handler( std::unique_ptr<i_io_imagery> obj)
{
    _imagery_handler = std::move(obj);
}

void face_embedder::set_face_detector( std::unique_ptr<i_face_detector> obj)
{
    _face_detector = std::move(obj);
}

void face_embedder::set_embedder( std::unique_ptr<i_face_to_embedding> obj)
{
    _embedder = std::move(obj);
}

void face_embedder::clear()
{
    _imagery_handler->clear();
    _embedder->clear();
    _face_detector->clear();
}
