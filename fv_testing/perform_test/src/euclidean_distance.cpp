#include "../include/euclidean_distance.hpp"

euclidean_distance::euclidean_distance() : _threshold(0.6)
{
}

euclidean_distance::~euclidean_distance()
{
}

bool euclidean_distance::match_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
    return match_embeddings();
}

bool euclidean_distance::operator()(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
    return match_embeddings();
}

void euclidean_distance::set_embedding1(const dlib::matrix<float, 0, 1> face_emb1)
{
    _face_emb1 = std::move(face_emb1);
}

void euclidean_distance::set_embedding2(const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb2 = std::move(face_emb2);
}

void euclidean_distance::set_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
}

bool euclidean_distance::match_embeddings() const
{
    if (_face_emb1.size() == 0 | _face_emb2.size() == 0)
    {
    }
    try
    {
        if (dlib::length((_face_emb1 - _face_emb2)) < _threshold)
        {
            std::cout <<  "threshold= " << _threshold << std::endl; 
            return true;
        }
        else
        {
            return false;
        }
    }
    catch (const std::exception &e)
    {
        return false;
    }
}

void euclidean_distance::set_threshold(const float threshold)
{
    // @todo uitzoeken of threshold tussen bepaald limiet moet liggen? bv tussen 0 en 20?
    // testen in unit test.
    _threshold = threshold;
}

float euclidean_distance::get_threshold() const
{
    return _threshold;
}

void euclidean_distance::clear()
{
    _face_emb1 = 0;
    _face_emb2 = 0;
}