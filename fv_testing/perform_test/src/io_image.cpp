#include "../include/io_image.hpp"

io_image::io_image()
{
}
io_image::~io_image()
{
}

dlib::matrix<dlib::rgb_pixel> io_image::load(const std::string input)
{
    try
    {
        dlib::load_image(_image, input);
    }
    catch (const std::exception &e)
    {
        std::cout <<" load image error :" << input << std::endl;
        std::cout << e.what() << std::endl;
    }

    return _image;
}

dlib::matrix<dlib::rgb_pixel> io_image::get_image() const
{
    return _image;
}

void io_image::operator>>(dlib::matrix<dlib::rgb_pixel> &get_img)
{
    get_img = this->_image;
}

size_t io_image::size() const
{
    return _image.size();
}

void io_image::set_image(const dlib::matrix<dlib::rgb_pixel> img)
{
    _image = img;
}

void io_image::operator<<(const dlib::matrix<dlib::rgb_pixel> set_img)
{
    this->_image = set_img;
}

void io_image::clear()
{
    _image.set_size(0, 0);
}