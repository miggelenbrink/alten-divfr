#include "../include/HOG.hpp"
#include "../include/error_handling.hpp"

HOG::HOG() : _face_detector(dlib::get_frontal_face_detector())
{
    try
    {
        dlib::deserialize(PATH_PREDICTOR_MODEL) >> _sp;
    }
    catch (const std::exception &e)
    {
    }
}

HOG::~HOG()
{
}

dlib::matrix<dlib::rgb_pixel> HOG::detect_face(const dlib::matrix<dlib::rgb_pixel> in_image)
{
    set_image(std::move(in_image));
    detect_faces();
    return get_image_cropped_face();
}

void HOG::set_image(const dlib::matrix<dlib::rgb_pixel> image)
{
    _image = std::move(image);
    detect_faces();
}

void HOG::operator<<(const dlib::matrix<dlib::rgb_pixel> set_img)
{
    _image = std::move(set_img);
    detect_faces();
}

dlib::matrix<dlib::rgb_pixel> HOG::get_image()
{
    return _image;
}

dlib::matrix<dlib::rgb_pixel> HOG::get_image_cropped_face()
{
    if (_image_cropped_face.size() == 0)
    {
    }
    return _image_cropped_face;
}

void HOG::operator>>(dlib::matrix<dlib::rgb_pixel> &get_cropped_img)
{
    get_cropped_img = _image_cropped_face;
}

void HOG::detect_faces()
{
    std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
    for (auto face : _face_detector(_image))
    {
        auto shape = _sp(_image, face);
        // We can also extract copies of each face that are cropped, rotated upright,
        // and scaled to a standard size as shown here:
        // http: //dlib.net/face_landmark_detection_ex.cpp.html
        dlib::matrix<dlib::rgb_pixel> face_chip;
        dlib::extract_image_chip(_image, dlib::get_face_chip_details(shape, 150, 0.25), face_chip);
        faces.push_back(std::move(face_chip));
    }
    /*exactly one face*/
    if (faces.size() == 1)
    {
        _image_cropped_face = faces.back();
        faces.pop_back();
    }
    /*no face*/
    else if (faces.size() == 0)
    {
        std::cout << " No face detected " << std::endl;
        throw fv_test_excp(error_code::err_no_face);
    }
    else
    /*more than one face*/
    {
        std::cout << " More than one face detected " << std::endl;

        //int temp = faces.size();
        //faces.clear();
        throw fv_test_excp(error_code::err_more_face);
    }
}

void HOG::clear()
{
    _image.set_size(0, 0);
    _image_cropped_face.set_size(0, 0);
}