#include "../include/DNN_ResNet.hpp"

DNN_ResNet::DNN_ResNet()
{
    try
    {
        dlib::deserialize(PATH_RESNET_MODEL) >> _ResNet_dnn; /**< the trained network weights/parameters/model dlib_face_recognition_resnet_model_v1.dat is/are loaded into the network */
    }
    catch (const std::exception &e)
    {
    }
}

DNN_ResNet::~DNN_ResNet()
{
}

void DNN_ResNet::set_face_image(dlib::matrix<dlib::rgb_pixel> image)
{
    _face_image = std::move(image);
    create_face_embedding();
}

void DNN_ResNet::operator<<(dlib::matrix<dlib::rgb_pixel> set_img)
{
    _face_image = std::move(set_img);
    create_face_embedding();
}

dlib::matrix<dlib::rgb_pixel> DNN_ResNet::get_face_image()
{
    return _face_image;
}

dlib::matrix<float, 0, 1> DNN_ResNet::get_face_embedding()
{
    return _face_embedding;
}

void DNN_ResNet::operator>>(dlib::matrix<float, 0, 1> &get_face_embedding)
{
    get_face_embedding = _face_embedding;
}

dlib::matrix<float, 0, 1> DNN_ResNet::embed_face(const dlib::matrix<dlib::rgb_pixel> in_face_image)
{
    // Convert face image into a 128D vector, aka face embedding.
    // The _ResNet_dnn Deep Neural Network implementation computes the embedding.
    _face_image = std::move(in_face_image);
    create_face_embedding();

    return _face_embedding;
}

void DNN_ResNet::create_face_embedding()
{
    try
    {
        if (_face_image.size() == 0)
        {
        }
        else
        {
            // Convert face image into a 128D vector, aka face embedding.
            // The _ResNet_dnn Deep Neural Network implementation computes the embedding.
            _face_embedding = _ResNet_dnn(_face_image);
        }
    }
    catch (const std::exception &e)
    {
    }
}

void DNN_ResNet::clear()
{
    _face_embedding = 0;
    _face_image.set_size(0,0);
}
