#include <iostream>
#include <fstream>
#include <istream>
#include "../include/face_embedder.hpp"  /* Class to test */
#include "../include/face_verifier.hpp"  /* Class to test */
#include "../include/error_handling.hpp"

int main(int argc, char **argv)
{
    /**
    * Validate command line options.
    */
    if (argc != 3)
    {
        std::cout << "Wrong input, the format is as following:" << std::endl;
        std::cout << "./main <threshold> <out_file_name>" << std::endl;
        exit(0);
    }
    else
    {
        for (int i = 0; i < argc; i++)
            std::cout << argv[i] << " ";
    }
    std::cout << std::endl;

    /**
    * Start measuring execution time.
    */
    using std::chrono::duration;
    using std::chrono::duration_cast;
    using std::chrono::high_resolution_clock;
    using std::chrono::milliseconds;
    auto t1 = high_resolution_clock::now();

    std::vector<std::string> i_pairs;// = get_vector_list_SLLFW(path_pairs.str());

    std::ifstream file;
    file.open("../inputs/pairs_input.txt");
    std::string linein;
    while (getline(file, linein))
    {
        i_pairs.push_back(linein);
    }
    file.close();

    /**
    * Variables that store the results of the tests.
    */
    bool result;
    int no_face = 0;        /* number of pairs of which one of the two images contains no face*/
    int more_face = 0;      /* number of pairs of which one of the two images contains more than face*/
    int true_positive = 0;  /* Matching pair that is classified as match */
    int true_negative = 0;  /* Mismatching pair that is classified as mismatch */
    int false_positive = 0; /* Mismatching pair that is classifed as match  */
    int false_negative = 0; /* Matching pair that is classified as mismatch*/
    std::vector<std::string> v_no_face;
    std::vector<std::string> v_more_face;
    std::vector<std::string> v_FP;
    std::vector<std::string> v_FN;

    /**
    * Creat the face embedder and verifier that will be tested.
    */
    face_embedder _embedder;
    face_verifier _verifier;
    /* Set the threshold on which the verifier will be tested. */
    _verifier.set_threshold(std::stof(argv[1]));

    /**
    * Perform the test and store the results.
    * The test is performed by looping through the pairs in the pairs.txt file which specifies all face image combination to test (perform face verification on).
    */
    int index = 0;
    while (index < (i_pairs.size() - 1))
    {
        try
        {
            std::cout << " Image one: " << index << " " << i_pairs[index] << std::endl;
            _embedder.embed(i_pairs[index]);
            _verifier.set_embedding1(_embedder.get_face_embedding());
            index += 1;
        }
        catch (fv_test_excp &e)
        {

            if (e.get_error_code() == err_no_face)
            {
                v_no_face.push_back(i_pairs[index]);
                no_face += 1;
                index += 3;
                // go to new iteration while loop
                continue;
            }
            if (e.get_error_code() == err_more_face)
            {
                v_more_face.push_back(i_pairs[index]);
                more_face += 1;
                index += 3;
                // go to new iteration while loop
                continue;
            }
            else
            {
                std::cout << "Caught an error!" << std::endl;
                index += 3;
                // go to new iteration while loop
                continue;
            }
        }
        try
        {
            std::cout << " Image two: " << index << " " << i_pairs[index] << std::endl;
            _embedder.embed(i_pairs[index]);
            _verifier.set_embedding2(_embedder.get_face_embedding());
            index += 1;
        }
        catch (fv_test_excp &e)
        {

            if (e.get_error_code() == err_no_face)
            {
                std::cout << "err_no_face" << std::endl;
                v_no_face.push_back(i_pairs[index]);
                no_face += 1;
                index += 2;
                // go to new iteration while loop
                continue;
            }
            if (e.get_error_code() == err_more_face)
            {
                std::cout << "err_more_face" << std::endl;
                v_more_face.push_back(i_pairs[index]);
                more_face += 1;
                index += 2;
                // go to new iteration while loop
                continue;
            }
            else
            {
                std::cout << "Caught an error!" << std::endl;
                index += 2;
                // go to new iteration while loop
                continue;
            }
        }
        std::cout << " Answer: " << i_pairs[index] << std::endl;
        result = _verifier.verify();
        std::cout << " Result: " << result << std::endl;
        std::cout << std::endl;

        /* TP: Matching pair ("1") that is classified as match (true) */
        if (i_pairs[index] == "1" && result == true)
        {
            true_positive += 1;
        }
        /* TN: Mismatching pair that is classified as mismatch */
        if (i_pairs[index] == "0" && result == false)
        {
            true_negative += 1;
        }
        /* FP: Mismatching pair that is classifed as match  */
        if (i_pairs[index] == "0" && result == true)
        {
            false_positive += 1;
            v_FP.push_back(i_pairs[index - 2]);
            v_FP.push_back(i_pairs[index - 1]);
            v_FP.push_back("---");
        }
        /* FN: Matching pair that is classified as mismatch */
        if (i_pairs[index] == "1" && result == false)
        {
            false_negative += 1;
            v_FN.push_back(i_pairs[index - 2]);
            v_FN.push_back(i_pairs[index - 1]);
            v_FN.push_back("---");
        }
        index += 1;
    }

    /**
    * Compute accuracy, TPR, FPR and store the results in a file. 
    */
    /* Create path and open file */
    std::stringstream path_result;
    path_result << "../test_results/" << argv[2] << "_threshold_" << argv[1] << ".txt";
    std::ofstream out(path_result.str());
    /* Redirect std::cout to the file */
    std::streambuf *coutbuf = std::cout.rdbuf(); //save old buf
    std::cout.rdbuf(out.rdbuf());                //redirect std::cout to out.txt!

    /* Compute and write the results to the file */
    std::cout << "Threshold used by face_verifier = " << _verifier.get_threshold() << std::endl;
    std::cout << "Threshold set by user = " << argv[1] << "\n"
              << "Input file = " << argv[2] << std::endl;
    std::cout << "Total input pairs = " << i_pairs.size() / 3 << std::endl;
    std::cout << "Total tested pairs = " << true_positive + true_negative + false_positive + false_negative << std::endl;
    std::cout << std::endl;
    std::cout << "Number of pairs in which no face is detected in one or both of the images = " << no_face << std::endl;
    std::cout << "Number of pairs in which more than one face is detected in one or both of the images = " << more_face << std::endl;
    std::cout << std::endl;
    std::cout << "Number of pairs with result TP = " << true_positive << std::endl;
    std::cout << "Number of pairs with result TN = " << true_negative << std::endl;
    std::cout << "Number of pairs with result FP = " << false_positive << std::endl;
    std::cout << "Number of pairs with result FN = " << false_negative << std::endl;
    float Accuracy = ((float)true_positive + (float)true_negative) / ((float)true_positive + (float)true_negative + (float)false_positive + (float)false_negative);
    std::cout << "Accuracy: " << Accuracy << std::endl;
    float TPR = ((float)true_positive / ((float)true_positive + (float)false_negative));
    std::cout << "True Positive Rate/Recall: " << TPR << std::endl;
    float FPR = ((float)false_positive / ((float)false_positive + (float)true_negative));
    std::cout << "False Positive Rate: " << FPR << std::endl;
    float Precision = ((float)true_positive / ((float)true_positive + (float)false_positive));
    std::cout << "Precision: " << Precision << std::endl;

    /**
    * Store the image pairs in which no face is detected or more than one face to the file, for debugging purpose and to check what images are not usefull. 
    * And store the FN and FP image pairs, to check what kind of images are not verified correctly. 
    */
    std::cout << std::endl;
    std::cout << "Images in which no face is detected: " << std::endl;
    for (std::string &f_no_face : v_no_face)
    {
        std::cout << f_no_face << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Images in which more than one face is detected: " << std::endl;
    for (std::string &f_more_face : v_more_face)
    {
        std::cout << f_more_face << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Images pairs with result False Negative (FN): " << std::endl;
    for (std::string &f_FN : v_FN)
    {
        std::cout << f_FN << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Images pairs with result False Positive (FP): " << std::endl;
    for (std::string &f_FP : v_FP)
    {
        std::cout << f_FP << std::endl;
    }
    std::cout << std::endl;

    /* Stop execution time */
    auto t2 = high_resolution_clock::now();
    /* Get the execution time in milliseconds */
    auto ms_int = duration_cast<milliseconds>(t2 - t1);
    std::cout << "Execution time:" << std::endl;
    std::cout << ms_int.count() << "ms" << std::endl;

    return 0;
}
