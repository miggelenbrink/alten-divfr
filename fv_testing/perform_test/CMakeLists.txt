#root cmakelists

cmake_minimum_required(VERSION 3.5)
project(DIVFR_fv_test VERSION 1.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory(dlib)
add_subdirectory(src)
add_subdirectory(app)










