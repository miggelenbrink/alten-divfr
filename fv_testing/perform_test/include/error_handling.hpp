#ifndef ERROR_HANDLING_NCE_HPP
#define ERROR_HANDLING_NCE_HPP

#include <iostream>
#include <exception>

    //--ERROR-CODES----------------------------------------------------------------------
    // exception errors
    enum error_code
    {
        /* connection error codes */
        err_no_face,
        err_more_face,
        /* Default */
        err_default,
        err_code_not_initialized
    };

    /**
     * Custom exception class for the DNN_ResNet class.
     * 
     * The derived class allows to throw exceptions with an message and error code.
     * The possible error codes can be found in the enum error_code which is in the namespace nce.
     * The catch block can handle the exception and determine what to do depending on the error code.
     */
    class fv_test_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        error_code _ec;

    public:
        fv_test_excp(const std::string msg = "Error message not initialized", const error_code ec = error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        fv_test_excp(const error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~fv_test_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        error_code get_error_code() const
        {
            return _ec;
        }
    };

//----------------------------------------------------------------------------------

#endif //ERROR_HANDLING_NCE_HPP
