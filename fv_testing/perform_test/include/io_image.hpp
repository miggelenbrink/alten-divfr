#ifndef IO_IMAGE_HPP
#define IO_IMAGE_HPP

#include <iostream>

#include "../dlib/image_io.h"
#include "../dlib/opencv.h"
/* interface */
#include "i_io_imagery.hpp"

class io_image : public i_io_imagery
{
public:
    io_image();
    virtual ~io_image() override;

    /**
     * Take a filename and the function loads the file into the dlib::matrix<rgb_pixel> object.
     * The function loads and saves the image to the member variable _image and returns it. 
     * Supported filetypes are PNG, JPEG, BMP and DNG.
     * @param input IN path to image file. 
     * @return return the image. 
     */
    dlib::matrix<dlib::rgb_pixel> load(const std::string input) override;

    /**
     * Get image if an image has been opened.
     * @return return image as dlib::matrix<rgb_pixel> object. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_image() const override;

    /**
     * Same functionality as the get_image() function, but implemented as >> operator.  
     * The operator returns the _image member variable inside the object the operator is called on.
     * The _image type is dlib::matrix<dlib::rgb_pixel>.
     * @return _image
     */
    void operator>>(dlib::matrix<dlib::rgb_pixel> &get_img) override;

    /**
     * Return size of image in bytes. 
     * @return return size in bytes of the dlib::matrix<T> image object. 
     */
    size_t size() const override;

    /**
     * Set image.
     * sets the _image member variable inside the object the operator is called on.
     * The _image type is dlib::matrix<dlib::rgb_pixel>.
     * @param img IN image.
     */
    void set_image(const dlib::matrix<dlib::rgb_pixel> img);

    /**
     * Same functionality as the set_image() function, but implemented as >> operator.  
     * The operator sets the _image member variable inside the object the operator is called on.
     * The _image type is dlib::matrix<dlib::rgb_pixel>.
     */
    void operator<<(const dlib::matrix<dlib::rgb_pixel> set_img);

    virtual void clear() override;

private:
    dlib::matrix<dlib::rgb_pixel> _image;
};

#endif //IO_IMAGE_HPP