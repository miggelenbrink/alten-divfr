#ifndef DNN_RESNET_HPP
#define DNN_RESNET_HPP

#include <iostream>
#include "../dlib/dnn.h"
/* interface */
#include "i_face_to_embedding.hpp"


#define PATH_RESNET_MODEL "../inputs/models/dlib_face_recognition_resnet_model_v1.dat"

// The following block of code defines the ResNet network.
// Lateron the trained parameters/weights are loaded into the ResNet network.
// The parameters can be found in the dlib_face_recognition_resnet_model_v1.dat file.
// ----------------------------------------------------------------------------------------
using namespace dlib;

template <template <int, template <typename> class, int, typename> class block, int N, template <typename> class BN, typename SUBNET>
using residual = add_prev1<block<N, BN, 1, tag1<SUBNET>>>;

template <template <int, template <typename> class, int, typename> class block, int N, template <typename> class BN, typename SUBNET>
using residual_down = add_prev2<avg_pool<2, 2, 2, 2, skip1<tag2<block<N, BN, 2, tag1<SUBNET>>>>>>;

template <int N, template <typename> class BN, int stride, typename SUBNET>
using block = BN<con<N, 3, 3, 1, 1, relu<BN<con<N, 3, 3, stride, stride, SUBNET>>>>>;

template <int N, typename SUBNET>
using ares = relu<residual<block, N, affine, SUBNET>>;
template <int N, typename SUBNET>
using ares_down = relu<residual_down<block, N, affine, SUBNET>>;

template <typename SUBNET>
using alevel0 = ares_down<256, SUBNET>;
template <typename SUBNET>
using alevel1 = ares<256, ares<256, ares_down<256, SUBNET>>>;
template <typename SUBNET>
using alevel2 = ares<128, ares<128, ares_down<128, SUBNET>>>;
template <typename SUBNET>
using alevel3 = ares<64, ares<64, ares<64, ares_down<64, SUBNET>>>>;
template <typename SUBNET>
using alevel4 = ares<32, ares<32, ares<32, SUBNET>>>;

using anet_type = dlib::loss_metric<dlib::fc_no_bias<128, dlib::avg_pool_everything<
                                                              alevel0<
                                                                  alevel1<
                                                                      alevel2<
                                                                          alevel3<
                                                                              alevel4<
                                                                                  dlib::max_pool<3, 3, 2, 2, dlib::relu<dlib::affine<dlib::con<32, 7, 7, 2, 2, dlib::input_rgb_image_sized<150>>>>>>>>>>>>>;

// ----------------------------------------------------------------------------------------

/**
 * ResNet Network implementation of the i_face_to_embedding interface.
 * 
 * The ResNet Network is a pre-trained dlib Deep Neural Network,
 * the implementation is designed by dlib and can be found in the dlib code dnn_imagenet_ex.cpp. 
 * The dlib ResNet DNN model dlib_face_recognition_resnet_model_v1.dat is trained using the code dnn_metric_learning_on_images_ex.cpp.
 * The ResNet input layer takes only images of size 150x150.
 * The ResNet output layer outputs a 128D vector, aka face embedding. 
 */
class DNN_ResNet : public i_face_to_embedding
{
public:
    DNN_ResNet();
    ~DNN_ResNet();

    /**
     * Compute and return a 128D face embedding from an input image with exactly one face in it with size 150x150.
     * See the class's description for detailed explanation of the internal function logic.  
     * @param face_image IN image as matrix<rgb_pixel> with exactly one face, user has to make sure there is only one face in the image.
     * @param out_face_embedding OUT computed face embedding as vector matrix<float, 0, 1>, the vector is sized at runtime.
     * @return Return true on succes, else return false. 
     * @todo vervang de functie door overlaoden << of >> constructor!! zodat je straks easy alles aan elkaar kunt koppelen
     * //virtual bool embed_face(dlib::matrix<dlib::rgb_pixel> in_face_image, dlib::matrix<float, 0, 1> &out_face_embedding) override;
     */
    virtual dlib::matrix<float, 0, 1> embed_face(const dlib::matrix<dlib::rgb_pixel> in_face_image) override;
    //virtual bool embed_face(dlib::matrix<dlib::rgb_pixel> in_face_image, dlib::matrix<float, 0, 1> &out_face_embedding) override;

    /**
     * Set the face image.
     * @param image 150x150 sized image as matrix<rgb_pixel> with exactly one face in it. 
     */
    void set_face_image(dlib::matrix<dlib::rgb_pixel> image) override;

    /**
     * Same functionality as set_face_image(). 
     */
    virtual void operator<<(dlib::matrix<dlib::rgb_pixel> set_img) override;

    /**
     * Get the face image.
     * @return Image as matrix<rgb_pixel>. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_face_image() override;

    /**
     * Get the 128D face embedding.
     * Get the face embedding of the face image that is set with set_face_image().
     * Before calling this function, call the create_face_embedding() function. 
     * If there is no face set, then the function gets an empty face embedding (empty vector).
     * @return Return face embedding as vector matrix<float, 0, 1>, the vector is sized at runtime.  
     */
    virtual dlib::matrix<float, 0, 1> get_face_embedding() override;

    /**
     * Same functionality as get_face_embedding(). 
     */
    virtual void operator>>(dlib::matrix<float, 0, 1> &get_face_embedding) override;

    virtual void clear() override;

private:
    /**
     * Create a 128D face embedding and save it into the member variable _face_embedding.
     * 
     * The function converts the _face_image member into a 128D vector, aka face embedding,
     * subsequently it saves the embedding into the member _face_embedding. 
     * The member _ResNet_dnn computes the embedding. 
     * @return Return true if a face embedding is succesfully computed, else return false. 
     */
     void create_face_embedding();

    dlib::matrix<dlib::rgb_pixel> _face_image;
    dlib::matrix<float, 0, 1> _face_embedding; /**< The colm and row are set to 0 and 1 in matrix<float, row, colm >. This means that the size is determined at runtime */
    anet_type _ResNet_dnn;                     /**< network responsible for computing face embeddings. In the class's constructur the weights/parameters/model dlib_face_recognition_resnet_model_v1.dat are/is loaded into the network */
};

#endif //DNN_RESNET_HPP