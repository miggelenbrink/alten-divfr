#ifndef FACE_EMBEDDER_HPP
#define FACE_EMBEDDER_HPP

#include <iostream>
#include "../dlib/image_io.h"
/* interfaces */
#include "i_io_imagery.hpp"
#include "i_face_detector.hpp"
#include "i_face_to_embedding.hpp"
/* implementation of interfaces*/
#include "io_image.hpp"
#include "HOG.hpp"
#include "DNN_ResNet.hpp"

/**
 * The face_embedder class contains functionality to compute a face embedding from imagery. 
 * 
 * The class has functionality to load or open imagery, detect faces in the imagery and compute face embeddings for the detected faces.
 * For each of the above descripted general functionality is an abstract interface class. Therefore the exact functionality depends on the chosen interfaces's implementations. 
 * 
 * The face_embedder works with the i_io_imagery, i_face_detector and i_face_to_embedding abstract interfaces
 * rather than the interfaces's implementations. In this manner the face_embedder can work with any of the interfaces's implementations.
 * It makes the face_embedder independent of the exact implementation of an function (of an interface's implementation). 
 * This is called a factory design pattern. The core of a factory design pattern is used here, but is slighty adapted.
 *
 * The i_io_imagery interface has two implementations, a io_image and io_web_cam implementation. 
 * The face_embedder uses one of them, which one dependents on which implementation is set by the set_*() function. 
 * This design makes it easy to add i_io_imagery implementation without having to change the face_embedder code. 
 * That means if the implementations statifies the interface then the face_embedder can use the implementation without any effort.
 */
class face_embedder
{
public:
    face_embedder();
    face_embedder(const std::string &input);

    ~face_embedder();

    // dummy function
    dlib::matrix<dlib::rgb_pixel> get_cropped_image();

    /**
     * Load imagery and return corresponding embedding.
     * 
     * Load an image or take a frame from the webcam by specifying the webcam's index or image's path.
     * The frame/image must contain exactly one face otherwise an exception is thrown.
     * The function embeds the frame/image and returns the corresponding embedding, which is an 128D vector with features.
     * @param input IN path or index to imagery.
     * @return Return the computed face embedding.
     */
    dlib::matrix<float, 0, 1> embed(const std::string &input);

    /**
     * Load imagery with exactly one face.
     * Depending on chosen implementation: load webcam (and capture frame immediately) or open an image.
     * The function automatically detects faces and stores it in a member variable.
     * @param input IN path or index of imagery, see the description of the chosen (set_imagery_handler) implementation for the _imagery_handler pointer.
     * @return Return the frame or image that is opened.
     */
    dlib::matrix<dlib::rgb_pixel> load_imagery(const std::string &input);

    /**
     * Find faces in the image, set with load_imagery(), and crop them.
     * Before calling the detect_face() function an image must be set, otherwise an exception is thrown. 
     * This can be done using the function set_image() or the operator<<.
     * If there is exactly one face, then save the cropped face 150x150 into the member _image_cropped_face and return it.
     * @return Return the cropped face.
     */
    dlib::matrix<dlib::rgb_pixel> detect_face();

    /**
     * Get face embedding of the cropped face image _cropped_face_image.
     *  
     * Before calling the get_face_embedding() function an image must be cropped, otherwise an exception is thrown. 
     * This can be done using the function load_imagery() or set_image/operator<< followed by embed_face().
     * @return Return the face embedding, which is an 128D vector with features.
     */
    dlib::matrix<float, 0, 1> get_face_embedding();

    /**
     * Same functionality as set_image().
     */
    void operator<<(dlib::matrix<dlib::rgb_pixel> set_img);

    /**
     * Same functionality as get_face_embedding().
     */
    void operator>>(dlib::matrix<float, 0, 1> &get_emb);

    /**
     * Same functionality as embed_face().
     */
    dlib::matrix<float, 0, 1> operator()();

    /**
     * Same functionality as embed(const std::string input).
     */
    dlib::matrix<float, 0, 1> operator()(const std::string &input);

    /**
     * Set an i_io_imagery interface implementation.
     * 
     * By setting an implementation the face_embedder uses that implementation's logic for the object's member: _imagery_handler.
     * The function also allows to change the implementation on runtime, it makes the face_embedder use the most recent set implementation.
     * It means that a call to a member functoin will cause a differnt function to be exectued depending on the type of the object that calls the function, also known as polymorphism. 
     * 
     * @param obj IN an i_io_imagery implementation, one of the available ones or one created from the i_io_imagery interface.
     * @note The function parameter is an unique_ptr, therefore an object must transfer it's ownership to the member function using std::move()
     */
    void set_imagery_handler(std::unique_ptr<i_io_imagery> obj);

    /**
     * Set an i_face_detector interface implementation.
     * 
     * By setting an implementation the face_embedder uses that implementation's logic for the object's member: _face_detector.
     * The function also allows to change the implementation on runtime, it makes the face_embedder use the most recent set implementation.
     * It means that a call to a member functoin will cause a differnt function to be exectued depending on the type of the object that calls the function, also known as polymorphism. 
     * 
     * @param obj IN an i_face_detector implementation, one of the available ones or one created from the i_face_detector interface.
     * @note The function parameter is an unique_ptr, therefore an object must transfer it's ownership to the member function using std::move()
     */
    void set_face_detector(std::unique_ptr<i_face_detector> obj);

    /**
     * Set an i_face_to_embedding interface implementation.
     * 
     * By setting an implementation the face_embedder uses that implementation's logic for the object's member _embedder.
     * The function also allows to change the implementation on runtime, it makes the face_embedder use the most recent set implementation.
     * It means that a call to a member functoin will cause a differnt function to be exectued depending on the type of the object that calls the function, also known as polymorphism. 
     * 
     * @param obj IN an i_face_to_embedding implementation, one of the available ones or one created from the i_face_detector interface.
     * @note The function parameter is an unique_ptr, therefore an object must transfer it's ownership to the member function using std::move()
     */
    void set_embedder(std::unique_ptr<i_face_to_embedding> obj);

    /**
     * Clear all data in the _imagery_handler, _face_detector and _embedder objects.
     */
    void clear();

private:
    /**
     * Embed the cropped face image _cropped_face_image, set with the function detect_face().
     * Before calling the embed_face() function an image must be cropped, otherwise an exception is thrown. 
     * @return Return the face embedding, which is an 128D vector with features.
     */
    dlib::matrix<float, 0, 1> embed_face();

    std::unique_ptr<i_io_imagery> _imagery_handler;
    std::unique_ptr<i_face_detector> _face_detector;
    std::unique_ptr<i_face_to_embedding> _embedder;
};

#endif //FACE_EMBEDDER_HPP