#ifndef I_FACE_TO_EMBEDDING_HPP
#define I_FACE_TO_EMBEDDING_HPP

#include <iostream>
#include "../dlib/image_io.h"

/**
 * Interface for Deep Neural Network (DNN) implementations that compute an face embedding from an image with exactly one face in it.
 * Usage of implemenatations using this Interface.
 * 1) Create an object of an implemenation made from the i_face_to_embedding interface. 
 * 2) Load an image with exactly one face in it, using the set_face_image(). 
 * 3) Create face embedding, using create_embedding().
 * 4) Get/return the face embeddings, using get_face_embedding().
 * 
 * Or use the start() function.
 * After you created an object, call the start() function. 
 * The start() function takes as input an image with exactly one face.
 * Subsequently it executes the above steps 2-3. 
 * Finaly, the function returns the computed face_embedding. 
 *
 * @todo @see virtual dlib::matrix<float, 0, 1> start(dlib::matrix<dlib::rgb_pixel> face_image) = 0;
 */
class i_face_to_embedding
{
private:
    dlib::matrix<dlib::rgb_pixel> _face_image;
    // <float, row, colm >: the colm and row are set to 0 and 1. This means that the size is determined at runtime.
    dlib::matrix<float, 0, 1> _face_embedding;

public:
    /**
     * Compute face features of an image with exactly one face and return the face features as vector.
     * The face features returned as vector is known as a face embedding. 
     * See the class's description for detailed explanation of the internal function's logic/implementation.  
     * @param in_face_image IN cropped image with focus on exactly one face.
     * @param out_face_embedding OUT face embedding of input (face). 
     * @return Return true if the funcion succesfully computed the face embedding, else return false.
     * //virtual bool embed_face(dlib::matrix<dlib::rgb_pixel> in_face_image, dlib::matrix<float, 0, 1> &out_face_embedding) = 0;
     */
    virtual dlib::matrix<float, 0, 1> embed_face(const dlib::matrix<dlib::rgb_pixel> in_face_image) = 0;
    //virtual bool embed_face(dlib::matrix<dlib::rgb_pixel> in_face_image, dlib::matrix<float, 0, 1> &out_face_embedding) = 0;

    /**
     * Set the face image.
     * @param image Image as matrix<rgb_pixel> with exactly one face in it. 
     */
    virtual void set_face_image(const dlib::matrix<dlib::rgb_pixel> image) = 0;

    /**
     * Same functionality as the set_face_image() function. 
     */
    virtual void operator<<(const dlib::matrix<dlib::rgb_pixel> set_img) = 0;

    /**
     * Get the face image.
     * @return Image as matrix<rgb_pixel>. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_face_image() = 0;

    /**
     * Get the face embedding.
     * Get the face embedding of the face image that is set with set_face_image().
     * If there is not face set, then the function gets an empty face embedding (empty vector).
     * @return Return face embedding as vector matrix<float, 0, 1>, the vector is sized at runtime.  
     */
    virtual dlib::matrix<float, 0, 1> get_face_embedding() = 0;

    /**
     * Same functionality as the set_face_image() function. 
     */
    virtual void operator>>(dlib::matrix<float, 0, 1> &get_face_embedding) = 0;

    virtual void clear() = 0;
};

#endif //I_FACE_TO_EMBEDDING_HPP