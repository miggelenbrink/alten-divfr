#ifndef I_FACE_DETECTOR_HPP
#define I_FACE_DETECTOR_HPP

#include <iostream>
#include <vector>
#include "../dlib/image_io.h"

class i_face_detector
{
private:
    dlib::matrix<dlib::rgb_pixel> _image;
    dlib::matrix<dlib::rgb_pixel> _image_cropped_face;

public:
    virtual dlib::matrix<dlib::rgb_pixel> detect_face(const dlib::matrix<dlib::rgb_pixel> in_image) = 0;
    virtual void set_image(const dlib::matrix<dlib::rgb_pixel> image) = 0;

    virtual void clear() = 0;

    /**
     * Same functionality as the set_image() function, but implemented as >> operator.  
     */
    virtual void operator<<(const dlib::matrix<dlib::rgb_pixel> set_img) = 0;
    virtual dlib::matrix<dlib::rgb_pixel> get_image() = 0;
    virtual dlib::matrix<dlib::rgb_pixel> get_image_cropped_face() = 0;

    /**
     * Same functionality as the get_image_cropped_face() function, but implemented as >> operator.  
     */
    virtual void operator>>(dlib::matrix<dlib::rgb_pixel> &get_cropped_img) = 0;
};

#endif //I_FACE_DETECTOR_HPP