#ifndef EUCLIDEAN_DISTANCE_HPP
#define EUCLIDEAN_DISTANCE_HPP

#include <iostream>

/* interface */
#include "i_matcher.hpp"

/**
 * @todo add in furture: 
 * It should also be noted that face recognition accuracy can be improved if jittering
    // is used when creating face descriptors.  In particular, to get 99.38% on the LFW
    // benchmark you need to use the jitter_image() routine to compute the descriptors,
    // like so:
 */
class euclidean_distance : public i_matcher
{
public:
   euclidean_distance();
   ~euclidean_distance();

   /**
     * Check if the euclidean distance of the two inputs (face_embedding) is less than the threshold, @see set_threshold().
     * @param face_emb1 IN face embedding of face 1.
     * @param face_emb2 IN face embedding of face 2.
     * @return Return true if the euclidean distance is less than the threshold, this means the faces are probably of the same person, else return false.
     */
   virtual bool match_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) override;

   /**
     * Same functionality as match_embeddings.
     */
   virtual bool operator()(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) override;

   /**
     * Set the member face embedding variable _face_emb1. 
     * @param face_emb1 face embedding, aka vector with features.
     */
   virtual void set_embedding1(const dlib::matrix<float, 0, 1> face_emb1) override;

   /**
     * Set the member face embedding variable _face_emb2. 
     * @param face_emb2 face embedding, aka vector with features.
     */
   virtual void set_embedding2(const dlib::matrix<float, 0, 1> face_emb2) override;

   /**
     * Set the both the face embedding member variables. 
     * @param face_emb1 face embedding, aka vector with features.
     * @param face_emb2 face embedding, aka vector with features.
     */
   virtual void set_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) override;

   /**
     * Check if the euclidean distance of the member variables _face_emb1 and _face_emb2 (face_embedding) is less than the threshold, @see set_threshold().
     * @return Return true if the euclidean distance is less than the threshold, this means the faces are probably of the same person, else return false.
     */
   virtual bool match_embeddings() const override;

   /**
     * http://dlib.net/face_recognition.py.html LEZEN!
     * http://dlib.net/dnn_face_recognition_ex.cpp.html
     * lles waarom 0.6 en hoe cropping tot 150x150 en padding van 0.25 werkt!
     * en eulicean distance en lenght enzoo
     *  Here we check if
            // the distance between two face descriptors is less than 0.6, which is the
            // decision threshold the network was trained to use.

     * if two face descriptor vectors have a Euclidean
        # distance between them less than 0.6 then they are from the same
        # person, otherwise they are from different people. Here we just print
        # the vector to the screen.
     * Change the threshold
     * @param threshold IN float 
     * By default the threshold is set to 0.6. This value is used as threshold to train the DNN_ResNet model.
     */
   virtual void set_threshold(const float threshold) override;
   virtual float get_threshold() const override;

   virtual void clear() override;

private:
   dlib::matrix<float, 0, 1> _face_emb1;
   dlib::matrix<float, 0, 1> _face_emb2;
   float _threshold;
};

#endif //EUCLIDEAN_DISTANCE_HPP