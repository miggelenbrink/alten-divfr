#ifndef HOG_HPP
#define HOG_HPP

#include <iostream>

#include "../dlib/image_processing/frontal_face_detector.h" /* frontal_face_detector*/
#include "../dlib/image_processing.h"                       /*face_predictor*/
/* interfaces */
#include "i_face_detector.hpp"

#define PATH_PREDICTOR_MODEL "../inputs/models/shape_predictor_5_face_landmarks.dat"

/**
 * Face detector that uses the HOG alogrimte.
 * Usage of object with HOG type(class):
 * 1) set image (input), with exactly one face in it
 * 2) run the face detector (compute output from input)
 * 3) get cropped image (output)
 */
class HOG : public i_face_detector
{
public:
    HOG();
    ~HOG();

    virtual dlib::matrix<dlib::rgb_pixel> detect_face(const dlib::matrix<dlib::rgb_pixel> in_image) override;

    /**
     * Load image for face detection.
     * This function sets an image in the HOG object. The face_detector() detects faces in the image set with this function.
     * Thus before calling face_detector(), this "set_image()" function should be called.
     * @param image image with type dlib::matrix<dlib::rgb_pixel>.
     */
    virtual void set_image(const dlib::matrix<dlib::rgb_pixel> image) override;

    /**
     * Same functionality as set_image().
     */
    virtual void operator<<(const dlib::matrix<dlib::rgb_pixel> set_img) override;

    /**
     * Return the image that is loaded for face detection, this is the image that is set with set_image().
     * On this image  the face_detector performed. Thus before calling the face_detector()
     * an image must be set using set_image().
     * @return return _image as type dlib::matrix<dlib::rgb_pixel>.
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_image() override;

    /**
     * Return 150x150 image cropped around the face.
     * Before calling this function the detect_faces() function should be called.
     * Otherwise the function returns an empty image.
     * @return return 150x150 cropped face image, if detect_faces() is called before, otherwise return empty image. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_image_cropped_face() override;

    /**
     * Same functionality as get_image_image_cropped().
     */
    virtual void operator>>(dlib::matrix<dlib::rgb_pixel> &get_cropped_img) override;

    virtual void clear() override;

private:
    /**
     * Find faces in image and crop them.
     * Before calling the detect_face() function an image must be set, otherwise an exception is thrown. 
     * If there is exactly one face, then save the cropped face 150x150 into the member _image_cropped_face .
     */
    void detect_faces();

    dlib::frontal_face_detector _face_detector;
    dlib::shape_predictor _sp;

    dlib::matrix<dlib::rgb_pixel> _image;
    dlib::matrix<dlib::rgb_pixel> _image_cropped_face;
};

#endif //HOG_HPP