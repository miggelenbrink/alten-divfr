#ifndef I_IO_IMAGERY_HPP
#define I_IO_IMAGERY_HPP

#include <iostream>
#include "../dlib/image_io.h"

class i_io_imagery
{
private:
    dlib::matrix<dlib::rgb_pixel> _image;

public:
    virtual ~i_io_imagery(){};
    virtual dlib::matrix<dlib::rgb_pixel> load(const std::string input) = 0;
    virtual dlib::matrix<dlib::rgb_pixel> get_image() const = 0;
    virtual void clear() = 0;
    /**
     * Same functionality as the get_image() function, but implemented as >> operator.  
     */
    virtual void operator>>(dlib::matrix<dlib::rgb_pixel> &get_img) = 0;

    virtual size_t size() const = 0;

};

#endif //I_IO_IMAGERY_HPP