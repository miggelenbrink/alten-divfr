#ifndef INPUT_HPP
#define INPUT_HPP

#include <fstream>
#include <sstream>
#include <iterator>
#include <vector>
#include <iostream>
#include <iomanip>
#include <ostream>

/**
 * Functions:
 *   - get_vector_list_lfw()
 *   - get_vector_list_sllfw()
 * 
 * 
 * The functions create a list, which is stored as a file, with the format:
 * 
 * <path/image1.jpg>
 * <path/image2.jpg>
 * <1 for matching, 0 for non matching pair> 
 * 
 * example:
 *
 * ../inputs/lfw/Fred_Funk/Fred_Funk_0001.jpg
 * ../inputs/lfw/Hartmut_Mehdorn/Hartmut_Mehdorn_0001.jpg
 * 0
 * ../inputs/lfw/Fred_Funk/Fred_Funk_0001.jpg
 * ../inputs/lfw/Hartmut_Mehdorn/Hartmut_Mehdorn_0001.jpg
 * 1
 * And so on. 
 */

/**
* Create a list (vector) with paths to the image pairs and the actual result for a input file of the lfw database.
* @see header 3b for the test method and header 3c http://vis-www.cs.umass.edu/lfw/README.txt for the format of the input file. 
*/
std::vector<std::string> get_vector_list_lfw(const std::string &inputfile)
{
    // Open pairs file which contains image combinations for the vf test.
    std::ifstream pairs_txt_file;
    pairs_txt_file.open(inputfile);

    /*  
        The top line in the pairs.txt file specifies the number of sets followed by the number of matched pairs per set (equalto the number of mismatched pairs per set). 
    */
    // read top line
    std::string line;
    std::stringstream ss;
    std::getline(pairs_txt_file, line);
    size_t n_sets, n_pairs;
    // parse top line
    ss << line;
    ss >> n_sets >> n_pairs;

    /*  
        The next lines specify the matched pairs and mismatched pairs in the following format for the matched pairs:

        name   n1   n2
        which means the matched pair consists of the n1 and n2 images for the person with the given name.  For instance,
        George_W_Bush   10   24
        The meaing of the line is: the pair consists of images George_W_Bush_0010.jpg and George_W_Bush_0024.jpg.
        Those two matching face images are passed to the face verification test.

        And in the following format for mismachted pairs:

        name1   n1   name2   n2
        which means the mismatched  pair consists of  the n1 image  of person
        name1 and the n2 image of person name2.  For instance, 
        George_W_Bush   12   John_Kerry   8

    */
    // Get the specefied image pairs and actual result and create a vector with paths to the images and the result per image pair.
    std::vector<std::string> line_list;
    std::string path{"../inputs/lfw/"}; /* prefix */
    while (getline(pairs_txt_file, line))
    {
        // parse second line
        std::istringstream ss2(line);
        std::vector<std::string> word_list(std::istream_iterator<std::string>{ss2},
                                           std::istream_iterator<std::string>());

        /* A line with matched pairs contains 3 words, the format is: person's name, number of 1ste image,  number of 2nd image */
        std::stringstream ss_temp;
        if (word_list.size() == 3)
        {
            /* matched pairs */
            // image number q of person x
            ss_temp << path << word_list[0] << "/" << word_list[0] << "_" << std::setw(4) << std::setfill('0') << word_list[1] << ".jpg";
            line_list.push_back(ss_temp.str());
            // image number p of person x
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            ss_temp << path << word_list[0] << "/" << word_list[0] << "_" << std::setw(4) << std::setfill('0') << word_list[2] << ".jpg";
            line_list.push_back(ss_temp.str());
            // boolean value (true) to indicate this is a matched pair.
            line_list.push_back("1");
        }
        /* A line with mismatched pairs contains 4 words, the format is: name first person, name second person, image number first person,  image number second person */
        else if (word_list.size() == 4)
        {
            /* mismatched pairs */
            // image number q of person x
            ss_temp << path << word_list[0] << "/" << word_list[0] << "_" << std::setw(4) << std::setfill('0') << word_list[1] << ".jpg";
            line_list.push_back(ss_temp.str());
            // image number p of person y
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            ss_temp << path << word_list[2] << "/" << word_list[2] << "_" << std::setw(4) << std::setfill('0') << word_list[3] << ".jpg";
            line_list.push_back(ss_temp.str());
            // boolean value (false) to indicate this is a matched pair.
            line_list.push_back("0");
        }
        else
        {
            std::cerr << "Line in the input .txt file contains not 3 or 4 words, skip and continue" << std::endl;
        }
    }

    return line_list;
}

/**
* Create a list (vector) with paths to the image pairs and the actual result for a input file of the sllfw database.
* @see http://whdeng.cn/SLLFW/index.html#download for the format of the input file. 
*/
std::vector<std::string> get_vector_list_sllfw(const std::string &inputfile)
{
    // Open pairs file which contains image combinations for the vf test.
    std::ifstream pairs_txt_file;
    pairs_txt_file.open(inputfile);

    std::string line;
    // Get the specefied images and create a vector with paths to the images
    std::vector<std::string> line_list;
    std::stringstream ss_temp;

    int cnt = 1;
    std::string path{"../inputs/lfw/"};
    // Get the specefied image pairs and actual result and create a vector with paths to the images and the result per image pair.
    for (int i = 1; i <= 10; i++)
    {

        for (int i = 1; i <= 300; i++)
        {
            // path to image 1
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            getline(pairs_txt_file, line);
            ss_temp << path << line;
            std::cout << "path: " << ss_temp.str() << std::endl;
            line_list.push_back(ss_temp.str());
            ss_temp.clear();

            // path to image 1
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            getline(pairs_txt_file, line);
            ss_temp << path << line;
            std::cout << "path: " << ss_temp.str() << std::endl;
            line_list.push_back(ss_temp.str());

            // indicate that the images are a matching pair
            line_list.push_back("1");
        }

        for (int i = 1; i <= 300; i++)
        {
            // path to image 1
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            getline(pairs_txt_file, line);
            ss_temp << path << line;
            std::cout << "path: " << ss_temp.str() << std::endl;
            line_list.push_back(ss_temp.str());
            ss_temp.clear();

            // path to image 1
            ss_temp.str(std::string()); // efficient way for clearing a stringstream, source: https://stackoverflow.com/questions/20731/how-do-you-clear-a-stringstream-variable
            getline(pairs_txt_file, line);
            ss_temp << path << line;
            std::cout << "path: " << ss_temp.str() << std::endl;
            line_list.push_back(ss_temp.str());

            // indicate that the images are a matching pair
            line_list.push_back("0");
        }
    }
    return line_list;
}

#endif //INPUT_HPP