#include <iostream>
#include <fstream>
#include <istream>
#include "input.hpp"
#include <string.h> // string compare

int main(int argc, char **argv)
{
    /**
    * Validate command line options.
    */
    if (argc != 3)
    {
        std::cout << "Wrong input, the format is as following:" << std::endl;
        std::cout << "./main <file_name> <data_base_name>" << std::endl;
        std::cout << "Database names: 'lfw', 'sllfw'" << std::endl;

        exit(0);
    }
    else
    {
        for (int i = 0; i < argc; i++)
            std::cout << argv[i] << " ";
    }
    std::cout << std::endl;

    std::string in_folder = "../input/pair_list/";
    std::string in_file = argv[1];

    // Get vector with input paths and actual results.
    std::cout << "open: " << in_folder + in_file << std::endl;
    std::vector<std::string> line_list;
    if (strcmp(argv[2], "lfw") == 0)
    {
        /* of LFW database */
        line_list = get_vector_list_lfw(in_folder + in_file);
    }
    else if (strcmp(argv[2], "sllfw") == 0)
    {
        /* of SLLFW database */
        line_list = get_vector_list_sllfw(in_folder + in_file);
    }
    else
    {
        std::cout << "Unknow database name!" << std::endl;
        exit(0);
    }

    // Store vector in input_pairs.txt file.
    std::ofstream file;
    file.open("../output/input_pairs.txt");
    for (int i = 0; i < line_list.size(); ++i)
    {
        file << line_list[i] << std::endl;
    }
    file.close();
    std::cout << "Stored the input_pairs.txt file in ../output/ " << std::endl;

    return 0;
}
