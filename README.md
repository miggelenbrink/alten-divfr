# DIVFV - Digital Identity Verification by Face Verification  

Deze map bevat drie sub-mappen: client_app, server_app en fv_testing map.

De client_app map bevat de clientapplicatie, ofwel de digitale identiteit applicatie.
De server_app map bevat de serverapplicatie, ofwel de controleapplicatie.
De fv_testing folder bevat de code waarmee de gezichtsverificatie software is getest.
De testen zijn uitgevoerd met behulp van de Labeled Faces in the Wild (LFW) dataset, zie http://vis-www.cs.umass.edu/lfw/.
Hoe de testen werken en zijn uitgevoerd is uitgelegd in de source code. Het resultaat van de testen de methode is uitgelegd  
in het hoofdrapport.

Zoals is toegelicht in het hoofdverslag, lag de focus van de afstudeeropdracht op de serverapplicatie.
Maar de uitleg hieronder is ook van toepassing op de clientapplicatie omdat de opbouw en gebruikte tools identiek zijn.
Veel componenten uit de serverapplicatie zijn dan ook herkbruikt in de clientapplicatie, zoals: connection.hpp, queue.hpp
msg_struct.hpp, msg1_handler.hpp. Het msg1_handler.hpp bestand van de serverapplicatie is in essentie hetzelfde als het c_message.hpp bestand, de naam is anders.

#### Algemeen
fv, staat voor face Verification.  
nc, staat voor Network communication.

## File structure serverapplication.

```
<app_name>
+--- app
     +--- main.cpp
     +--- CMakeLists.txt
+--- build
+--- include
     +--- fv
          +--- <.hpp files>
     +--- nc
          +--- <.hpp files>
     fsm.hpp
     logger.hpp
     qt_sink.hpp
     error_handling_fve.hpp
     error_handling_nce.hpp
+--- src
     +--- fv
          +--- <.cpp files>
     +--- nc
          +--- <.cpp files>
     +--- qt
          +--- <.cpp files>
     fsm.cpp
+--- documentation
+--- input
     +--- nc
     +--- fv
+--- logs
+--- third_party
     +--- dlib
     +--- spdlog
+--- CMakeLists.txt
+--- README.md
```

- De input/nc folder bevat het keypair en de selfsigned certificaat van de serverapplicatie, die gebruikt worden voor de versleutelde TLS communicatieverbinding.   
- De input/fv folder bevat het voorgetrainde ResNet-model die gebruikt wordt door het Deep Neural Network (DNN) ResNet architectuur, ookwel Convolutional Neural Network (CNN), die staat is in het include/DNN_ResNet.hpp bestand. De serverapplicatie laadt het voorgetrainde model in, in het ResNet CNN.   
- In de /logs folder wordt voor elke dag een log bestand bijgehouden.   
- De /third_party folder bevat de header-only dlib en spdlog libraries.   
- De /documentation folder bevat de gegenereerde Doxygen documentatie.  

## IP-adres en port client en server applicatie
### client applicatie
Het IP-adres waar de clientapplicatie mee moet verbinden is hard-coded in de applicatie, zie de connect_to_server("127.0.0.1", "6000") functie in main.cpp

### server applicatie
In de serverapplicatie wordt de server gestart op port 6000. Dit gebeurt door de constructor van de fsm class die een server_api object initialiseert, zie fsm.hpp en fsm.cpp. 

## Installation
This section describes the installation and build of the DIVFV serverapplicatie and the clientapplication. The libraries were tested on x86_64 architecture with as operating system Ubuntu 20.04. 

**Requirements**

 - Visual Studio 2019
 - OpenSSL 1.1.1f
 - Boost (Boost.Asio) 1.75.0
 - Protocol Buffers 3.15.5
 - CMake 3.16.3
 - GCC 9.3.0 compiler  
 - Qt5 
   
After installing, make sure the library versions are as above specified.   

**Configure Visual Studio and CMake**  
Make sure you set the GCC 9.3.0 compiler and make sure that CMake can find all library directories, also the dev-libraries.  
We installed the libraries in the default location '/usr', which is part of LD_LIBRARY_PATH. LD_LIBRARY_PATH tells the linker where to find shared libraries.

**OpenSSL**  
The easiest way to get OpenSSL for ubuntu is:  
```
sudo apt-get install openssl
sudo apt-get install -y libssl-dev
```
OpenSSL must be installed because Boost.Asio depends on it.  

**Boost**  
The easiest way to get Boost for ubuntu is:  
```
sudo apt install libboost-dev
sudo apt install libboost-all-dev

go to the boost site, download boost_1_76_0 or version 75
go to the usr/local folder
and extract the .tar.bz2 here! 
example
sudo tar --bzip2 -xf /home/guido/Downloads/boost_1_76_0.tar.bz2 
```

**Protocol Buffers**   

This project is fully C++, therefore we install C++ Protocol Buffers.  
The easiest way to get Protocol Buffers for ubuntu is to following the install, see, [c++ install](https://github.com/protocolbuffers/protobuf/blob/master/src/README.md)

If you define a custom data structure with Protobuf in a .proto file, then compile the .proto file with the command:  
`protoc -I=$SRC_DIR --cpp_out=$DST_DIR $SRC_DIR/<fname>.proto`
- SRC_DIR:  folder where the .proto file is.  
- DST_DIR:  folder where you want the generated data structure (.cpp file)  
  
Easiest is to generate the .cpp file in the same location as the .proto file. 
`protoc -I="." --cpp_out="." <file_name>.proto`  

To create a new datastructure to send different data types: 
   Compile the .proto file as explaind. Place the source file in the /src/nc folder and the header file in the /include/nc folder.
   Go to the msg_type_handler.hpp and include the generated protobuf header file `#include <name>.pb.h`.
   Change to msg_type_handler.hpp file such that is indicates which datastructure type is in it.
   Next let the created msg_type_handler.hpp class inherit from the generated protobuf data strucuture.
   This can be done by changing the line: `class c_message : public divfr::data`
   to `class msg_type_handler : public <class_name>`.
   The `class_name` is the name of the generated protobuf C++ class in the `<name>.pb.h` file.  

Now the new datastructure is availble to use! don't forget to assign a type to it, see include/nc/msg_struct.hpp.

**CMake**  
The easiest way to get CMake for ubuntu is:    
```
sudo apt-get update
sudo apt-get install cmake
```

**Qt5**
The easiest wa to get Qt5 for ubuntu is:
[See Qt5 install](https://wiki.qt.io/Install_Qt_5_on_Ubuntu)  
```
sudo apt-get install qt5-default 
```

**DIVFV client- and serverapplicatie**  
1) Pull this git repository.  

**Build target and execute**  
If you setup the environment correclty, then there are two options to build and execute the target.  
1) Go to the 'build' folder which is in the application's root folder, and run the following commands:   
 
```
    > cmake ..   
    > cmake build .   
    > ./../main  
```

2) Or open the application's root folder in Visual Studio and run a CMake Delete Cache and Reconfigure, subsequently run CMake clean build.   

```
    > In Visual Studio do: ctrl + shift + p and search for "CMake Delete  
    > Cache and Reconfigure" and hit enter. Next, in the blue status bar at the bottom set  
    > the active target. Next, also in the blue status bar, hit the play button to build and launch the selected target.   
```

**LINKER FLAGS**  
Linker flags used by CMake, see src/CMakeLists.txt.  
```
-lcrypto    boost depends on crypto library  
-lssl       boost depends on the OpenSSL library  
-lprotobuf  the application depends/uses the protobuf library, thus link it into the executable  
-lpthread   optional, if you don't use -pthread  
```

## Links  
[Boost.Asio](https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio.html)  
[Protobuf](https://developers.google.com/protocol-buffers)  
[CMake](https://cmake.org/)  
[Dlib](http://dlib.net/)

