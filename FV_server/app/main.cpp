#include <QApplication>
#include "../include/logger.hpp"
#include "../src/qt/mainwindow.h"

int main(int argc, char *argv[])
{
    std::cout << "Version 6" << std::endl;
    
    try
    {
        QApplication a(argc, argv);
        MainWindow w;
        
        // Initialize the logger and it's sinks.
        init_face_verification_logger(w);

        // Start the QT GUI and underlaying fsm. 
        w.show();

        return a.exec();
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Error in the main() function: {0}", e.what());
    }

    return 0;
}