# Root CMakelists.txt file

cmake_minimum_required(VERSION 3.5)
project(DIVFR_server VERSION 1.0)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_subdirectory(third_party/dlib)
add_subdirectory(src)
add_subdirectory(app)










