#include "../../include/nc/connection.hpp"

connection::connection(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket,
                       nc::queue<msg_data> &msgInQueue,
                       std::function<void(nce::errorCodes ec)> callback)
    : _socket(std::move(socket)), _msgInQueue(msgInQueue), _callback_func(callback)
{
    spdlog::debug("Construct connection");
}

connection::~connection()
{
    spdlog::debug("Deconstruct connection");
}

bool connection::is_socket_open()
{
    return _socket.lowest_layer().is_open();
}

// Disconnect function
void connection::stop_socket()
{
    if (is_socket_open())
    {
        _socket.lowest_layer().close();
        spdlog::debug("Socket is closed");
    }
}

void connection::start_reading()
{
    if (is_socket_open())
    {
        read_header();
    }
    else
    {
        SPDLOG_WARN("Start reading failed because there is no socket/connection open");
        _callback_func(nce::errorCodes::socket_is_open_ERR);
        //throw nce::network_lib_excp(nce::error_code::err_socket_is_open);
    }
}

void connection::start_write()
{
    if (_socket.lowest_layer().is_open())
    {
        write_header();
    }
    else
    {
        SPDLOG_WARN("Start write failed because there is no socket/connection open");
                                            _callback_func(nce::errorCodes::socket_is_open_ERR);

        //throw nce::network_lib_excp(nce::error_code::err_socket_is_open);
    }
}

void connection::start_handshake()
{
    _socket.async_handshake(boost::asio::ssl::stream_base::server,
                            [this](const boost::system::error_code &error) {
                                if (!error)
                                {
                                    start_reading();
                                }
                                else
                                {
                                    SPDLOG_WARN("Handshake with client failed");
                                    _callback_func(nce::errorCodes::handshake_ERR);
                                }
                            });
}

void connection::push_writeQueue(const msg_data &value)
{
    _msgOutQueue.push_back(value);
}

void connection::pop_front_WriteQueue()
{
    _msgOutQueue.pop_front();
}

bool connection::writeQueue_empty()
{
    return _msgOutQueue.empty();
}

void connection::write_header()
{
    // prepare header
    auto header = _msgOutQueue.front()._data.size();
    std::string header_serialized;
    header_serialized.resize(sizeof(header));
    header_serialized.assign(reinterpret_cast<const char *>(&(header)), sizeof(header));

    // write header
    boost::asio::async_write(_socket, boost::asio::buffer(header_serialized, sizeof(header_serialized)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     write_id();
                                 }
                                 else
                                 {
                                     SPDLOG_WARN("Async_write header failed");
                                     _callback_func(nce::errorCodes::write_ERR);
                                 }
                             });
}

void connection::write_id()
{
    // prepare id
    auto id = _msgOutQueue.front()._id;
    std::string id_serialized;
    id_serialized.resize(sizeof(id));
    id_serialized.assign(reinterpret_cast<const char *>(&(id)), sizeof(id));

    // prepare payload
    auto payload = _msgOutQueue.front()._data;

    // write id
    boost::asio::async_write(_socket, boost::asio::buffer(id_serialized, sizeof(id_serialized)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     spdlog::debug("Write id Succesfully, wrote {0:d} bytes", length);
                                     if (payload.size() > 0)
                                     {
                                         write_payload();
                                     }
                                     else
                                     {
                                         _msgOutQueue.pop_front();

                                         if (!_msgOutQueue.empty())
                                         {
                                             start_write();
                                         }
                                     }
                                 }
                                 else
                                 {
                                     SPDLOG_WARN("Async_write id failed");
                                     _callback_func(nce::errorCodes::write_ERR);
                                 }
                             });
}

void connection::write_payload()
{
    auto payload = _msgOutQueue.front()._data;

    boost::asio::async_write(_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     spdlog::debug("Write payload Succesfully, wrote {0:d} bytes", length);

                                     _msgOutQueue.pop_front();

                                     if (!_msgOutQueue.empty())
                                     {
                                         spdlog::debug("Start new write because there are still messages in the write queue");
                                         start_write();
                                     }
                                 }
                                 else
                                 {
                                     SPDLOG_WARN("Async_write payload failed");
                                     _callback_func(nce::errorCodes::write_ERR);
                                 }
                             });
}

void connection::read_header()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&_header),
                                                sizeof(_header)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    spdlog::debug("Read header Succesfully, read {0:d} bytes and the header contains the value {1:d}", lenght, _header);
                                    read_id();
                                }
                                else
                                {
                                    _header = 0;
                                    SPDLOG_WARN("Async_read header failed");
                                    _callback_func(nce::errorCodes::read_ERR);
                                }
                            });
}

void connection::read_id()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&_msg._id),
                                                sizeof(_msg._id)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    spdlog::debug("Read id Succesfully, read {0:d} bytes and the message id is: {1:d}", lenght, _msg._id);
                                    //check if message has payload
                                    if (_header > 0)
                                    {
                                        try
                                        {
                                            _msg._data.resize(_header);
                                            read_payload();
                                        }
                                        catch (const std::length_error &le)
                                        {
                                            _header = 0;
                                            _msg._data.erase();

                                            SPDLOG_WARN("Length error on received header");
                                            _callback_func(nce::errorCodes::read_ERR);
                                        }
                                    }
                                    else
                                    {
                                        SPDLOG_WARN("Received message without payload, discard message and wait for new message");
                                        start_reading();
                                    }
                                }
                                else
                                {
                                    SPDLOG_WARN("Async_read id failed");
                                    _callback_func(nce::errorCodes::read_ERR);
                                }
                            });
}

void connection::read_payload()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(_msg._data.data(),
                                                _header),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    spdlog::debug("Read payload Succesfully, read {0:d} bytes", lenght);

                                    //tmp_msg complete, add to message IN queue
                                    _msgInQueue.push_back(_msg);

                                    //@todo implemetn reset function in struct, for now reset member variables here, ready for new read
                                    _header = 0;
                                    _msg._data.clear();

                                    //the server should always listen for messages, thus give the io_context the task to listing for a new header
                                    //read_header(); same as start but with is_connected check
                                    start_reading();
                                }
                                else
                                {
                                    SPDLOG_WARN("Async_read payload failed");
                                    _callback_func(nce::errorCodes::read_ERR);
                                }
                            });
}