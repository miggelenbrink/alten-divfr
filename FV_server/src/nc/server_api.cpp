#include "../../include/nc/server_api.hpp"

server_api::server_api(int port) : server_handler(port), new_connection_flag(false)
{
}

server_api::~server_api()
{
}

bool server_api::onClientConnect()
{

    if (!new_connection_flag)
    {
        return false;
    }

    //the server_api must do a disconnect() before it can accept new connections.
    if (client_is_connected())
    {
        return false;
    }
    else
    {
        //chekc if callback is set
        if (_onClientConnect_callback)
            _onClientConnect_callback();

        new_connection_flag = false;
        return true;
    }
}

void server_api::onClientMessage()
{
    if (_onClientMessage_callback)
        _onClientMessage_callback();
}

void server_api::onClientDisconnect()
{
}