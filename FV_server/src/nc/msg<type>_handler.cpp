
/* --> Specify the correct include file */
#include "../../include/nc/msg<type>_handler.hpp"

msg1_handler::msg1_handler(msg_id id) : _msg_data{id}
{
}

msg1_handler::~msg1_handler()
{
}

size_t msg1_handler::sizeof_id()
{

    return sizeof(_msg_data._id);
}

size_t msg1_handler::sizeof_header()
{
    return sizeof(size_t);
}

size_t msg1_handler::sizeof_payload()
{
    return _msg_data._data.size();
}

bool msg1_handler::parse(msg_data data)
{
    _msg_data = std::move(data);
    if (!ParseFromString(_msg_data._data))
    {
        //x SPDLOG_WARN("Parse payload failed")
        return false;
    }
    else
    {
        //x spdlog::debug("Parsed payload succesfully")
        return true;
    }
}

msg_data msg1_handler::serialize()
{
    if (!SerializeToString(&_msg_data._data))
    {
        //x SPDLOG_WARN("Serialize message failed, return an empty msg_data object")
        _msg_data._data.clear();
        _msg_data._id = msgDefault;
        return _msg_data;
    }
    else
    {
        //x spdlog::debug("Serialized message succesfully")
        return _msg_data;
    }
}
