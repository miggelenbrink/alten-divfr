// #include "../../include/nc/queue.hpp"

// template <class T>
// queue<T>::~queue()
// {
//     clear();
// }

// // function cannot be const because: Precisely, the _mutex object will change it's internal state from say "unlocked" to "locked" state
// template <class T>
// //const T& queue<T>::front()
// T &queue<T>::front()
// {
//     std::scoped_lock lock(mutexQueue);
//     return _queue.front();
// }

// template <class T>
// //const T& queue<T>::back() otheriwse works not in connection.hpp with boost::asio::buffer(). @todo remove const char
// T &queue<T>::back()
// {
//     std::scoped_lock lock(mutexQueue);
//     return _queue.back();
// }

// template <class T>
// bool queue<T>::empty()
// {
//     std::scoped_lock lock(mutexQueue);
//     return _queue.empty();
// }

// //number of elements in the queue
// template <class T>
// std::size_t queue<T>::size()
// {
//     std::scoped_lock lock(mutexQueue);
//     return _queue.size();
// }

// template <class T>
// void queue<T>::push_back(const T &value)
// {
//     std::scoped_lock lock(mutexQueue);
//     _queue.push_back(value);
// }

// template <class T>
// T queue<T>::pop_back()
// {
//     std::scoped_lock lock(mutexQueue);
//     //back)() returns reference, the reference (the actual value. not the address) is copied into x
//     auto x = _queue.back();
//     _queue.pop_back();
//     return x;
// }

// // if called and no data in queue then segmenattion fault if no data in the queue! take care of this
// template <class T>
// T queue<T>::pop_front()
// {
//     std::scoped_lock lock(mutexQueue);
//     auto x = std::move(_queue.front());
//     _queue.pop_front();
//     return x;
// }

// template <class T>
// void queue<T>::clear()
// {
//     std::scoped_lock lock(mutexQueue);
//     _queue.clear();
// }