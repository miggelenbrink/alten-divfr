#include "../../include/nc/server_handler.hpp"

server_handler::server_handler(int port)
    : _acceptor(_io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
      _ssl_context(boost::asio::ssl::context::sslv23), _threadOnMessage_flag(false)
{
    spdlog::debug("Construct server_handler and start the server on port {0:d}", port);

    _ssl_context.set_options(
        boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 |
        // /Always create a new key when using tmp_dh parameters. = empahral key pair, always new keypair on new connection = forward secrecy
        boost::asio::ssl::context::single_dh_use);

    // load the certificates! we only use one certificate, it is not really a chain!
    boost::system::error_code ec;

    _ssl_context.use_certificate_chain_file(CERT_FILE, ec);
    if (ec)
    {
        SPDLOG_WARN("Path to .cert file is incorrect {0}", CERT_FILE);
        throw nce::network_lib_excp(ec.message(), nce::error_code::err_load_CERT_FILE);
    }
    _ssl_context.use_private_key_file(KEY_FILE, boost::asio::ssl::context::pem, ec);
    if (ec)
    {
        SPDLOG_WARN("Path to .key file is incorrect {0}", KEY_FILE);
        throw nce::network_lib_excp(ec.message(), nce::error_code::err_load_KEY_FILE);
    }

    // connect the timer to the io_context.run_one() functie! zodat deze elke x ms uitgevoerd wordt zodra de timer afloopt
    QObject::connect( &_qt_timer_thread, &QTimer::timeout, [this](){_io_context.poll();});

    start_server();
}

server_handler::~server_handler()
{
    spdlog::debug("Deconstruct server_handler");
    stop_server();
}

void server_handler::stop_server()
{
    // stop OnMessage Thread
    _threadOnMessage_flag = true;

    if (!_io_context.stopped())
        _io_context.stop();

    if (_threadContext.joinable())
        _threadContext.join();

    if (_threadOnMessage.joinable())
        _threadOnMessage.join();

    // allow new OnMessage Thread
    _threadOnMessage_flag = false;

    if (!(_connection.use_count() == 0))
    {
        _connection->stop_socket();
        _connection.reset();
        spdlog::debug("Disconnected client succesfully");
    }
}

void server_handler::start_server()
{
    try
    {
        // give the io_context work
        do_accept();

        if (_io_context.stopped())
            _io_context.restart();
        
        // run the io_context
        // _threadContext = std::thread([this]() { _io_context.run(); });
        if (!_qt_timer_thread.isActive())
        {
            std::chrono::milliseconds ms_timer (25);   
            _qt_timer_thread.setInterval(ms_timer.count());
            _qt_timer_thread.start();
        }

        // Check for new messages, if new message than call callback function
        _threadOnMessage = std::thread([this]() {
            while (!_threadOnMessage_flag)
            {
                while (!_msgInQueue.empty())
                {
                    onClientMessage();
                }
            }
        });
    }
    catch (std::exception &e)
    {
        SPDLOG_WARN("Failed to start the server");
        throw nce::network_lib_excp(e.what(), nce::error_code::err_start_server);
    }
    spdlog::debug("The server is running");
}

void server_handler::disconnect_client()
{
    // Delete left messages.
    _msgInQueue.clear();

    // If no client is connected, then do nothing
    if (!(_connection.use_count() == 0))
    {
        _connection->stop_socket();
        _connection.reset();
        spdlog::debug("[server_handler: disconnect_client()] Closed connection with client");
    }
}

bool server_handler::client_is_connected()
{
    if (_connection.use_count() == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void server_handler::send(const std::string data, const msg_id id)
{
    if (!(_connection.use_count() == 0))
    {
        if (_connection->is_socket_open())
        {
            _io_context.post([this, data, id]() {
                //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
                bool WriteInprocess = _connection->writeQueue_empty();

                // Write message into the outgoing message queue (buffer).
                msg_data temp;
                temp._data = data;
                temp._id = id;
                _connection->push_writeQueue(temp);

                // Tell the connection object to write all message in the message queue to the socket stream/server.
                if (WriteInprocess)
                    _connection->start_write();
            });
        }
        else
        {
            SPDLOG_WARN("Send failed, message to be send is not stored in the send queue");
            onErrorConnection(nce::errorCodes::socket_is_open_ERR); /*same as _callback_func(nce::errorCodes::socket_is_open_ERR);*/
            //throw nce::network_lib_excp(nce::error_code::err_socket_is_open);
        }
    }
    else
    {
        SPDLOG_WARN("Send failed, because there is no client connected, message to be send is not stored in the send queue");
    }
}

nc::queue<msg_data> *server_handler::get_msgInQueue_ptr()
{
    return &_msgInQueue;
}

// callback function, called on a new client connection/session
bool server_handler::onClientConnect()
{
    //default behavior virtual/abstract function
    //return true to accept the new client and delete the current one
    //return false to deny the client
    _onClientConnect_callback();
    return true;
}

void server_handler::onClientMessage()
{
    //default behavior virtual/abstract function
    _onClientMessage_callback();
}

void server_handler::set_onClientConnect_callback(std::function<void(void)> onClientConnect_callback)
{
    _onClientConnect_callback = onClientConnect_callback;
}

void server_handler::set_onClientMessage_callback(std::function<void(void)> onClientMessage_callback)
{
    _onClientMessage_callback = onClientMessage_callback;
}

void server_handler::set_onClientDisconnect_callback(std::function<void(void)> onClientDisconnect_callback)
{
    _onClientDisconnect_callback = onClientDisconnect_callback;
}

void server_handler::set_onHandshakeFailed_callback(std::function<void(void)> onHandshakeFailed_callback)
{
    _onHandshakeFailed_callback = onHandshakeFailed_callback;
}

void server_handler::do_accept()
{
    _acceptor.async_accept(
        [this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
            if (!ec)
            {
                _client_address = socket.remote_endpoint().address().to_string();

                std::shared_ptr<connection> tmp_conn =
                    std::make_shared<connection>(
                        boost::asio::ssl::stream<boost::asio::ip::tcp::socket>(std::move(socket), _ssl_context),
                        _msgInQueue,
                        [this](nce::errorCodes erc) { this->onErrorConnection(erc); });

                if (onClientConnect())
                {
                    spdlog::debug("Accepted client connection with address: {0}", _client_address);
                    // start session/connection object/ connection object start reading received messages and pushing them into a queue
                    tmp_conn->start_handshake();
                    _connection = tmp_conn;
                }
                else
                {
                    SPDLOG_WARN("Denied client connection with address: {0}", _client_address);
                }
            }
            do_accept();
        });
}

std::string server_handler::get_client_address()
{
    return _client_address;
}

// @todo change to exception, but for now callback errors because nce is async.../
// callback function, called on ERROR
void server_handler::onErrorConnection(nce::errorCodes ec)
{
    switch (ec)
    {
    case nce::errorCodes::handshake_ERR:
        std::cout << "[SERVER] Errorcode: handshake_ERR" << std::endl;
        if (client_is_connected())
        {
            _onHandshakeFailed_callback();
        }

        break;
    case nce::errorCodes::socket_is_open_ERR:
        std::cout << "[SERVER] Errorcode: socket_is_open_ERR" << std::endl;
        if (client_is_connected())
            _onClientDisconnect_callback();
        //disconnect_client();

        break;
    case nce::errorCodes::read_ERR:
        std::cout << "[SERVER] Errorcode: read_ERR" << std::endl;
        if (client_is_connected())
        {
            // callback function is called in fsm.cpp and the reset state does also a disconnect_client, thus overbodig here.
            //disconnect_client();
            _onClientDisconnect_callback();
        }

        break;
    case nce::errorCodes::write_ERR:
        std::cout << "[SERVER] Errorcode: write_ERR" << std::endl;

        // don't try to acces the pointer if it points to the unkown.
        if (!(_connection.use_count() == 0))
        {
            //if you don't do a disconnect_client(), then manualy delete the message that caused the error from the _msgOutQueue! because alsong the msg is in the buffer it will probably gerenate te same error on a next write.

            // Delete the message that failed the write operation.
            if (!_connection->writeQueue_empty())
                _connection->pop_front_WriteQueue();

            // if there are more messages in the buffer start writing those.
            if (!_connection->writeQueue_empty())
                _connection->start_write();
        }
        break;
    default:
        std::cout << "[SERVER] Errorcode: error code does not exists..." << std::endl;
        break;
    }
}