All files the QT mainwindow class depends on have to be in one folder.
Otherwise set(CMAKE_AUTOMOC ON) in the CMakelist.txt cannot take all file together and create a vtable. 

It is possible to separate the files but then you should link all files using cmake MOC in the CMakelist.txt file.