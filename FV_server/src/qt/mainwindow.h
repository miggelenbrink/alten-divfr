#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextBlock>
#include <QTextCursor>

#include "spdlog/spdlog.h" /* add global space spdlog to this file, to use created loggers in this file*/
#include "../../include/fsm.hpp"
#include "../../third_party/dlib/image_io.h" /* dlib image matrix */
#include "../../third_party/dlib/opencv.h"   /* dlib to opencv matrix */

Q_DECLARE_METATYPE(QTextBlock)
Q_DECLARE_METATYPE(QTextCursor)

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

// Declare because cross referencing the fsm and Mainwindow classes.
class fsm;

/**
 * The MainWindow is the framework for the application's Graphical User Interface (GUI).
 * 
 * The functionality of the GUI's widgets are implemented in this class. 
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    fsm *_statemachine;

    void disable_all_buttons();
    void enable_all_buttons();
    void enable_start_button();
    void disable_start_button();
    void enable_capture_button();
    void disable_caputer_button();
    void enable_reset_button();
    void disable_reset_button();
    void enable_apply_path_button();
    void disable_apply_path_button();
    void enable_apply_index_button();
    void disable_apply_index_button();
    void enable_clear_button();
    void disable_clear_button();
    void disable_cancel_button();
    void enable_cancel_button();
    void enable_stop_button();
    void disable_stop_button();

    void set_progressbar(const int percentage);
    /**
     * Display an image on the qlabel. 
     * 
     * The function must copy the image because, dlib::toMat()
     * returns an OpenCV Mat object which represents the same 
     * image as img. This is done by setting up the Mat object 
     * to point to the same memory as img. Therefore, the returned 
     * Mat object is valid only as long as pointers to the pixels 
     * in img remain valid.
     */
    void set_qlabel_image(dlib::matrix<dlib::rgb_pixel> &image); 
    /**
     * Display an image on the qlabel. 
     * 
     * The function must copy the image because, dlib::toMat()
     * returns an OpenCV Mat object which represents the same 
     * image as img. This is done by setting up the Mat object 
     * to point to the same memory as img. Therefore, the returned 
     * Mat object is valid only as long as pointers to the pixels 
     * in img remain valid.
     */    
    void set_qlabel_webcam(dlib::matrix<dlib::rgb_pixel> &image); 
    void reset_qlabel_image();
    void reset_qlabel_webcam();
    /**
     * fv = face verification 
     */
    void set_qlabel_result_fv(const std::string &text);
    /**
     * atbl = allowed to buy liquid 
     */
    void set_qlabel_result_atbl(const std::string &text);
    /**
     * example stylesheet: "color: green;" or "color: red;"
     */
    void set_color_qlabel_result_fv(const std::string &stylesheet);
    /**
     * example stylesheet: "color: green;" or "color: red;"
     */
    void set_color_qlabel_result_atbl(const std::string &stylesheet);
    void set_qlabel_connection(const std::string &address);
    /**
    * Write a message to the plainTextEdit widget in the GUI which acts as an logger.
    * 
    * Features:
    * - The widget is read-only;
    * - The widget scrolls automatically down to display the latest log message;
    * - The widget keeps track of max 100 lines, it automatically deletes the oldest ones. 
    *   This keeps the memory usage limited.
    */ 
    void set_logger(const std::string &text);

private slots:
    void on_pushButton_capture_clicked();
    void on_pushButton_Start_clicked();
    void on_pushButton_Stop_clicked();
    void on_pushButton_Cancel_clicked();
    void on_pushButton_Reset_results_clicked();
    void on_pushButton_Clear_clicked();
    void on_pushButton_apply_path_clicked();
    void on_pushButton_apply_index_clicked();

private:
    Ui::MainWindow *ui;

};
#endif // MAINWINDOW_H
