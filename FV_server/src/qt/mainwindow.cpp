#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      _statemachine(new fsm(this))
{
    ui->setupUi(this);
    ui->progressBar->setRange(0, 100);

    ui->lineEdit_path_face_identity->setText(QString::fromStdString(_statemachine->get_path()));
    ui->lineEdit_path_face_identity_2->setText(QString::fromStdString(_statemachine->get_index()));

    qRegisterMetaType<QTextBlock>();
    qRegisterMetaType<QTextCursor>();
    // create logger from planTextEdit
    ui->plainTextEdit->setReadOnly(true);
    /**
    * keep track of max 100 log lines, delete the oldest ones.
    * https://doc.qt.io/qt-5/qplaintextedit.html#maximumBlockCount-prop
    */
    ui->plainTextEdit->setMaximumBlockCount(100);

    _statemachine->handle_event(E_INIT);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_capture_clicked()
{
    _statemachine->handle_event(E_TAKESELFIE);
}

void MainWindow::on_pushButton_Start_clicked()
{
    _statemachine->handle_event(E_START);
}

void MainWindow::on_pushButton_Stop_clicked()
{
    _statemachine->handle_event(E_STOP);
}

void MainWindow::on_pushButton_Cancel_clicked()
{
    _statemachine->handle_event(E_CANCEL);
}

void MainWindow::on_pushButton_Reset_results_clicked()
{
    _statemachine->handle_event(E_RESETRESULTS);
}

void MainWindow::on_pushButton_Clear_clicked()
{
    ui->plainTextEdit->clear();

}

void MainWindow::on_pushButton_apply_path_clicked()
{
    spdlog::info("Changed path to image {0}", ui->lineEdit_path_face_identity->text().toStdString());
    _statemachine->set_path(ui->lineEdit_path_face_identity->text().toStdString());
}

void MainWindow::on_pushButton_apply_index_clicked()
{
    spdlog::info("Changed index of camera {0}", ui->lineEdit_path_face_identity_2->text().toStdString());
    _statemachine->set_index(ui->lineEdit_path_face_identity_2->text().toStdString());
}

void MainWindow::disable_all_buttons()
{
    ui->pushButton_apply_index->setDisabled(true);
    ui->pushButton_apply_path->setDisabled(true);
    ui->pushButton_Cancel->setDisabled(true);
    ui->pushButton_capture->setDisabled(true);
    ui->pushButton_Clear->setDisabled(true);
    ui->pushButton_Reset_results->setDisabled(true);
    ui->pushButton_Stop->setDisabled(true);
    ui->pushButton_Start->setDisabled(true);
    ui->pushButton_apply_index->setDisabled(true);
    ui->pushButton_apply_path->setDisabled(true);
}
void MainWindow::enable_all_buttons()
{
    ui->pushButton_apply_index->setEnabled(true);
    ui->pushButton_apply_path->setEnabled(true);
    ui->pushButton_Cancel->setEnabled(true);
    ui->pushButton_capture->setEnabled(true);
    ui->pushButton_Clear->setEnabled(true);
    ui->pushButton_Reset_results->setEnabled(true);
    ui->pushButton_Stop->setEnabled(true);
    ui->pushButton_Start->setEnabled(true);
    ui->pushButton_apply_index->setEnabled(true);
    ui->pushButton_apply_path->setEnabled(true);
}
void MainWindow::disable_start_button()
{
    ui->pushButton_Start->setDisabled(true);
}
void MainWindow::enable_start_button()
{
    ui->pushButton_Start->setEnabled(true);
}
void MainWindow::enable_capture_button()
{
    ui->pushButton_capture->setEnabled(true);
}
void MainWindow::disable_caputer_button()
{
    ui->pushButton_capture->setDisabled(true);
}
void MainWindow::enable_reset_button()
{
    ui->pushButton_Reset_results->setEnabled(true);
}
void MainWindow::disable_reset_button()
{
    ui->pushButton_Reset_results->setDisabled(true);
}
void MainWindow::enable_apply_path_button()
{
    ui->pushButton_apply_path->setEnabled(true);
}
void MainWindow::disable_apply_path_button()
{
    ui->pushButton_apply_path->setDisabled(true);
}
void MainWindow::enable_apply_index_button()
{
    ui->pushButton_apply_index->setEnabled(true);
}
void MainWindow::disable_apply_index_button()
{
    ui->pushButton_apply_index->setDisabled(true);
}
void MainWindow::enable_clear_button()
{
    ui->pushButton_Clear->setEnabled(true);
}
void MainWindow::disable_clear_button()
{
    ui->pushButton_Clear->setDisabled(true);
}
void MainWindow::disable_cancel_button()
{
    ui->pushButton_Cancel->setDisabled(true);
}
void MainWindow::enable_cancel_button()
{
    ui->pushButton_Cancel->setEnabled(true);
}
void MainWindow::enable_stop_button()
{
    ui->pushButton_Stop->setEnabled(true);
}
void MainWindow::disable_stop_button()
{
    ui->pushButton_Stop->setDisabled(true);
}

void MainWindow::set_progressbar(const int percentage)
{
    ui->progressBar->setValue(percentage);
}

void MainWindow::set_qlabel_image(dlib::matrix<dlib::rgb_pixel> &image)
{
    cv::Mat img = dlib::toMat(image);                                                                                              // convert to cv image
    ui->label_Face_Identity->setPixmap(QPixmap::fromImage(QImage(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888))); // convert to Qlabel
}

void MainWindow::reset_qlabel_image()
{
    ui->label_Face_Identity->clear();
    ui->label_Face_Identity->setText("Face identity");
    ui->label_Face_Identity->setStyleSheet("background-color: black");
}

void MainWindow::set_qlabel_webcam(dlib::matrix<dlib::rgb_pixel> &image)
{
    cv::Mat img = dlib::toMat(image);                                                                                       // convert to cv image
    ui->label_Camera->setPixmap(QPixmap::fromImage(QImage(img.data, img.cols, img.rows, img.step, QImage::Format_RGB888))); // convert to Qlabel
}

void MainWindow::reset_qlabel_webcam()
{
    ui->label_Camera->clear();
    ui->label_Camera->setText("Camera");
    ui->label_Camera->setStyleSheet("background-color: black");
}

void MainWindow::set_qlabel_result_fv(const std::string &text) // result face verification
{
    ui->label_result_fv->setText(QString::fromStdString(text));
}

void MainWindow::set_qlabel_result_atbl(const std::string &text) // result Allowed to buy liquid
{
    ui->label_result_bl->setText(QString::fromStdString(text));
}

void MainWindow::set_color_qlabel_result_fv(const std::string &stylesheet) // color result face verification
{
    ui->label_result_fv->setStyleSheet(QString::fromStdString(stylesheet));
}

void MainWindow::set_color_qlabel_result_atbl(const std::string &stylesheet) // color result Allowed to buy liquid
{
    ui->label_result_bl->setStyleSheet(QString::fromStdString(stylesheet));
}

void MainWindow::set_qlabel_connection(const std::string &address)
{
    ui->label_connection_result->setText(QString::fromStdString(address));
}

void MainWindow::set_logger(const std::string &text)
{
    ui->plainTextEdit->appendPlainText(QString::fromStdString(text).trimmed());
    // scroll automatically down
    QTextCursor x = ui->plainTextEdit->textCursor();
    x.movePosition(QTextCursor::End);
    ui->plainTextEdit->ensureCursorVisible();
}