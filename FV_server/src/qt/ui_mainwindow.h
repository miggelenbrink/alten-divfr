/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.8
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPlainTextEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout_7;
    QGridLayout *gridLayout_4;
    QHBoxLayout *horizontalLayout_8;
    QSpacerItem *horizontalSpacer_6;
    QPushButton *pushButton_capture;
    QSpacerItem *horizontalSpacer_5;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QLabel *label_Camera;
    QLabel *label_Face_Identity;
    QTabWidget *tabWidget;
    QWidget *Tab1_Log;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_Clear;
    QPlainTextEdit *plainTextEdit;
    QWidget *tab_Settings;
    QGridLayout *gridLayout_6;
    QGridLayout *gridLayout_3;
    QLabel *label_path_face_identity;
    QSpacerItem *horizontalSpacer_3;
    QGridLayout *gridLayout;
    QPushButton *pushButton_apply_path;
    QLineEdit *lineEdit_path_face_identity;
    QGridLayout *gridLayout_5;
    QLabel *label_path_face_identity_2;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_4;
    QLineEdit *lineEdit_path_face_identity_2;
    QPushButton *pushButton_apply_index;
    QSpacerItem *verticalSpacer;
    QProgressBar *progressBar;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pushButton_Start;
    QPushButton *pushButton_Stop;
    QPushButton *pushButton_Cancel;
    QFrame *frame;
    QFormLayout *formLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_face_verification;
    QLabel *label_result_fv;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_allow_buy_liquid;
    QLabel *label_result_bl;
    QPushButton *pushButton_Reset_results;
    QFrame *frame_2;
    QFormLayout *formLayout;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_connection;
    QLabel *label_connection_result;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(328, 594);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout_7 = new QGridLayout(centralwidget);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSpacer_6 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_6);

        pushButton_capture = new QPushButton(centralwidget);
        pushButton_capture->setObjectName(QString::fromUtf8("pushButton_capture"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(pushButton_capture->sizePolicy().hasHeightForWidth());
        pushButton_capture->setSizePolicy(sizePolicy);
        pushButton_capture->setSizeIncrement(QSize(1, 0));

        horizontalLayout_8->addWidget(pushButton_capture);

        horizontalSpacer_5 = new QSpacerItem(13, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_5);


        gridLayout_4->addLayout(horizontalLayout_8, 1, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(138, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer, 1, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_Camera = new QLabel(centralwidget);
        label_Camera->setObjectName(QString::fromUtf8("label_Camera"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_Camera->sizePolicy().hasHeightForWidth());
        label_Camera->setSizePolicy(sizePolicy1);
        label_Camera->setMinimumSize(QSize(150, 150));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        QBrush brush2(QColor(255, 255, 220, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
        QBrush brush3(QColor(255, 255, 255, 128));
        brush3.setStyle(Qt::SolidPattern);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Active, QPalette::PlaceholderText, brush3);
#endif
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush3);
#endif
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush3);
#endif
        label_Camera->setPalette(palette);
        label_Camera->setAutoFillBackground(true);
        label_Camera->setFrameShape(QFrame::NoFrame);
        label_Camera->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(label_Camera);

        label_Face_Identity = new QLabel(centralwidget);
        label_Face_Identity->setObjectName(QString::fromUtf8("label_Face_Identity"));
        label_Face_Identity->setEnabled(true);
        sizePolicy1.setHeightForWidth(label_Face_Identity->sizePolicy().hasHeightForWidth());
        label_Face_Identity->setSizePolicy(sizePolicy1);
        label_Face_Identity->setMinimumSize(QSize(150, 150));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Dark, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Mid, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Active, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Active, QPalette::Shadow, brush1);
        palette1.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipBase, brush2);
        palette1.setBrush(QPalette::Active, QPalette::ToolTipText, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Active, QPalette::PlaceholderText, brush3);
#endif
        palette1.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Dark, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Mid, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::Shadow, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush2);
        palette1.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Inactive, QPalette::PlaceholderText, brush3);
#endif
        palette1.setBrush(QPalette::Disabled, QPalette::WindowText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Dark, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Mid, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::BrightText, brush);
        palette1.setBrush(QPalette::Disabled, QPalette::ButtonText, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Shadow, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush2);
        palette1.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush1);
#if QT_VERSION >= QT_VERSION_CHECK(5, 12, 0)
        palette1.setBrush(QPalette::Disabled, QPalette::PlaceholderText, brush3);
#endif
        label_Face_Identity->setPalette(palette1);
        label_Face_Identity->setContextMenuPolicy(Qt::DefaultContextMenu);
        label_Face_Identity->setLayoutDirection(Qt::LeftToRight);
        label_Face_Identity->setAutoFillBackground(true);
        label_Face_Identity->setFrameShape(QFrame::NoFrame);
        label_Face_Identity->setScaledContents(true);
        label_Face_Identity->setAlignment(Qt::AlignCenter);
        label_Face_Identity->setIndent(-1);

        horizontalLayout->addWidget(label_Face_Identity);


        gridLayout_4->addLayout(horizontalLayout, 0, 0, 1, 2);


        gridLayout_7->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabWidget = new QTabWidget(centralwidget);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setMaximumSize(QSize(16777215, 16777215));
        Tab1_Log = new QWidget();
        Tab1_Log->setObjectName(QString::fromUtf8("Tab1_Log"));
        verticalLayout = new QVBoxLayout(Tab1_Log);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSpacer_2 = new QSpacerItem(398, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);

        pushButton_Clear = new QPushButton(Tab1_Log);
        pushButton_Clear->setObjectName(QString::fromUtf8("pushButton_Clear"));

        horizontalLayout_3->addWidget(pushButton_Clear);


        verticalLayout->addLayout(horizontalLayout_3);

        plainTextEdit = new QPlainTextEdit(Tab1_Log);
        plainTextEdit->setObjectName(QString::fromUtf8("plainTextEdit"));

        verticalLayout->addWidget(plainTextEdit);

        tabWidget->addTab(Tab1_Log, QString());
        tab_Settings = new QWidget();
        tab_Settings->setObjectName(QString::fromUtf8("tab_Settings"));
        gridLayout_6 = new QGridLayout(tab_Settings);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_path_face_identity = new QLabel(tab_Settings);
        label_path_face_identity->setObjectName(QString::fromUtf8("label_path_face_identity"));

        gridLayout_3->addWidget(label_path_face_identity, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(148, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_3, 0, 1, 1, 1);


        gridLayout_6->addLayout(gridLayout_3, 0, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pushButton_apply_path = new QPushButton(tab_Settings);
        pushButton_apply_path->setObjectName(QString::fromUtf8("pushButton_apply_path"));
        sizePolicy1.setHeightForWidth(pushButton_apply_path->sizePolicy().hasHeightForWidth());
        pushButton_apply_path->setSizePolicy(sizePolicy1);

        gridLayout->addWidget(pushButton_apply_path, 0, 1, 1, 1);

        lineEdit_path_face_identity = new QLineEdit(tab_Settings);
        lineEdit_path_face_identity->setObjectName(QString::fromUtf8("lineEdit_path_face_identity"));

        gridLayout->addWidget(lineEdit_path_face_identity, 0, 0, 1, 1);


        gridLayout_6->addLayout(gridLayout, 1, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_path_face_identity_2 = new QLabel(tab_Settings);
        label_path_face_identity_2->setObjectName(QString::fromUtf8("label_path_face_identity_2"));

        gridLayout_5->addWidget(label_path_face_identity_2, 0, 0, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(148, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_4, 0, 1, 1, 1);


        gridLayout_6->addLayout(gridLayout_5, 2, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        lineEdit_path_face_identity_2 = new QLineEdit(tab_Settings);
        lineEdit_path_face_identity_2->setObjectName(QString::fromUtf8("lineEdit_path_face_identity_2"));

        horizontalLayout_4->addWidget(lineEdit_path_face_identity_2);

        pushButton_apply_index = new QPushButton(tab_Settings);
        pushButton_apply_index->setObjectName(QString::fromUtf8("pushButton_apply_index"));
        sizePolicy1.setHeightForWidth(pushButton_apply_index->sizePolicy().hasHeightForWidth());
        pushButton_apply_index->setSizePolicy(sizePolicy1);

        horizontalLayout_4->addWidget(pushButton_apply_index);


        gridLayout_6->addLayout(horizontalLayout_4, 3, 0, 1, 1);

        verticalSpacer = new QSpacerItem(13, 53, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_6->addItem(verticalSpacer, 4, 0, 1, 1);

        tabWidget->addTab(tab_Settings, QString());

        gridLayout_7->addWidget(tabWidget, 5, 0, 1, 1);

        progressBar = new QProgressBar(centralwidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        QPalette palette2;
        QBrush brush4(QColor(32, 74, 135, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Active, QPalette::Highlight, brush4);
        palette2.setBrush(QPalette::Inactive, QPalette::Highlight, brush4);
        QBrush brush5(QColor(145, 145, 145, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette2.setBrush(QPalette::Disabled, QPalette::Highlight, brush5);
        progressBar->setPalette(palette2);
        progressBar->setValue(0);

        gridLayout_7->addWidget(progressBar, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        pushButton_Start = new QPushButton(centralwidget);
        pushButton_Start->setObjectName(QString::fromUtf8("pushButton_Start"));

        horizontalLayout_2->addWidget(pushButton_Start);

        pushButton_Stop = new QPushButton(centralwidget);
        pushButton_Stop->setObjectName(QString::fromUtf8("pushButton_Stop"));

        horizontalLayout_2->addWidget(pushButton_Stop);

        pushButton_Cancel = new QPushButton(centralwidget);
        pushButton_Cancel->setObjectName(QString::fromUtf8("pushButton_Cancel"));

        horizontalLayout_2->addWidget(pushButton_Cancel);


        gridLayout_7->addLayout(horizontalLayout_2, 2, 0, 1, 1);

        frame = new QFrame(centralwidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy2);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        formLayout_2 = new QFormLayout(frame);
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_face_verification = new QLabel(frame);
        label_face_verification->setObjectName(QString::fromUtf8("label_face_verification"));

        horizontalLayout_5->addWidget(label_face_verification);

        label_result_fv = new QLabel(frame);
        label_result_fv->setObjectName(QString::fromUtf8("label_result_fv"));

        horizontalLayout_5->addWidget(label_result_fv);


        formLayout_2->setLayout(0, QFormLayout::LabelRole, horizontalLayout_5);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_allow_buy_liquid = new QLabel(frame);
        label_allow_buy_liquid->setObjectName(QString::fromUtf8("label_allow_buy_liquid"));

        horizontalLayout_6->addWidget(label_allow_buy_liquid);

        label_result_bl = new QLabel(frame);
        label_result_bl->setObjectName(QString::fromUtf8("label_result_bl"));

        horizontalLayout_6->addWidget(label_result_bl);


        formLayout_2->setLayout(1, QFormLayout::LabelRole, horizontalLayout_6);

        pushButton_Reset_results = new QPushButton(frame);
        pushButton_Reset_results->setObjectName(QString::fromUtf8("pushButton_Reset_results"));

        formLayout_2->setWidget(2, QFormLayout::LabelRole, pushButton_Reset_results);


        gridLayout_7->addWidget(frame, 3, 0, 1, 1);

        frame_2 = new QFrame(centralwidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        sizePolicy2.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy2);
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        formLayout = new QFormLayout(frame_2);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_connection = new QLabel(frame_2);
        label_connection->setObjectName(QString::fromUtf8("label_connection"));

        horizontalLayout_7->addWidget(label_connection);

        label_connection_result = new QLabel(frame_2);
        label_connection_result->setObjectName(QString::fromUtf8("label_connection_result"));

        horizontalLayout_7->addWidget(label_connection_result);


        formLayout->setLayout(0, QFormLayout::LabelRole, horizontalLayout_7);


        gridLayout_7->addWidget(frame_2, 4, 0, 1, 1);

        MainWindow->setCentralWidget(centralwidget);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "DIFV", nullptr));
        pushButton_capture->setText(QApplication::translate("MainWindow", "Capture", nullptr));
        label_Camera->setText(QApplication::translate("MainWindow", "Camera", nullptr));
        label_Face_Identity->setText(QApplication::translate("MainWindow", "Face identity", nullptr));
        pushButton_Clear->setText(QApplication::translate("MainWindow", "Clear", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(Tab1_Log), QApplication::translate("MainWindow", "Log", nullptr));
        label_path_face_identity->setText(QApplication::translate("MainWindow", "Path face identity:", nullptr));
        pushButton_apply_path->setText(QApplication::translate("MainWindow", "Apply", nullptr));
        label_path_face_identity_2->setText(QApplication::translate("MainWindow", "Index camera:", nullptr));
        pushButton_apply_index->setText(QApplication::translate("MainWindow", "Apply", nullptr));
        tabWidget->setTabText(tabWidget->indexOf(tab_Settings), QApplication::translate("MainWindow", "Settings", nullptr));
        pushButton_Start->setText(QApplication::translate("MainWindow", "Start", nullptr));
        pushButton_Stop->setText(QApplication::translate("MainWindow", "Stop", nullptr));
        pushButton_Cancel->setText(QApplication::translate("MainWindow", "Cancel", nullptr));
        label_face_verification->setText(QApplication::translate("MainWindow", "Result face verification:", nullptr));
        label_result_fv->setText(QApplication::translate("MainWindow", "x", nullptr));
        label_allow_buy_liquid->setText(QApplication::translate("MainWindow", "Allowed to buy liquid:", nullptr));
        label_result_bl->setText(QApplication::translate("MainWindow", "x", nullptr));
        pushButton_Reset_results->setText(QApplication::translate("MainWindow", "Reset", nullptr));
        label_connection->setText(QApplication::translate("MainWindow", "Connection:", nullptr));
        label_connection_result->setText(QApplication::translate("MainWindow", "x", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
