#include "../include/fsm.hpp"
#include <iostream>

fsm::fsm(MainWindow *mainwindow) : _image_path{PATH_IMAGE}, _camera_index{INDEX_WEBCAM}, _cur_state{S_Initialised}, _server{SERVER_PORT},
                                   _queue_server{_server.get_msgInQueue_ptr()}, _result{false}, _mainwindow{mainwindow}
{
    spdlog::debug("Constructor fsm");
    _server.set_onClientConnect_callback([this]() { handle_event(E_ONCONNECT); });
    _server.set_onClientMessage_callback([this]() { if(get_current_state() == S_WaitOnMessage ){handle_event(E_ONMESSAGE);} });
    _server.set_onClientDisconnect_callback([this]() { if(get_current_state() == S_WaitOnMessage ){handle_event(E_ONDISCONNECT);} });
    _server.set_onHandshakeFailed_callback([this]() { handle_event(E_STOP); });
}

fsm::~fsm()
{
    spdlog::debug("Deconstructor fsm");
}

void fsm::handle_event(event_e event)
{
    while (event != E_NO)
    {
        event = statemachine(event);
    }
}

event_e fsm::statemachine(event_e event)
{
    std::scoped_lock lock(mutex_fsm);

    state_s nextState = S_NO;
    event_e event_out = E_NO;

    try
    {
        switch (_cur_state)
        {
        case S_Initialised:
        {
            nextState = S_Start;
            event_out = E_NO;

            spdlog::info("Initialised the application succesfully");

            _mainwindow->disable_all_buttons();
            _mainwindow->enable_start_button();
            _mainwindow->enable_clear_button();
            _mainwindow->enable_apply_index_button();
            _mainwindow->enable_apply_path_button();

            break;
        }

        case S_Start:
        {
            switch (event)
            {
            case E_START:
                nextState = S_WaitForConnection;
                event_out = E_NO;

                spdlog::info("Waiting on a client to connect");

                _server.new_connection_flag = true;

                _mainwindow->disable_start_button();
                _mainwindow->disable_apply_index_button();
                _mainwindow->disable_apply_path_button();
                _mainwindow->enable_stop_button();

                break;
            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                SPDLOG_ERROR("Unknown event called in state S_Start");

                break;
            }
            break;
        }
        case S_WaitForConnection:
        {
            _mainwindow->set_progressbar(20);

            switch (event)
            {
            case E_ONCONNECT:
                nextState = S_WaitOnMessage;
                event_out = E_NO;

                // A client connected, don't allow other clients to connect until the verification proces is finished or cancled.
                _server.new_connection_flag = false;

                spdlog::info("Client connected, waiting on personal data");

                _mainwindow->enable_cancel_button();
                _mainwindow->set_qlabel_connection(_server.get_client_address());

                break;
            case E_STOP:
                nextState = S_Reset;
                event_out = E_STOP;

                break;
            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                SPDLOG_ERROR("Unknown event called in state S_WaitForConnection");

                break;
            }
            break;
        }
        case S_WaitOnMessage:
        {
            switch (event)
            {
            case E_CANCEL:
                nextState = S_Reset;
                event_out = E_CANCEL;

                break;
            case E_STOP:
                nextState = S_Reset;
                event_out = E_STOP;

                break;
            case E_ONMESSAGE:
                nextState = S_Read;
                event_out = E_CONTINUE;
                // Do no call any qt widget or spdlog:: here! This line/function is called from the callback function onMessage in the async_read handler.

                break;
            case E_ONDISCONNECT:
                nextState = S_Reset;
                event_out = E_DISCONNECT;

                spdlog::info("The client disconnected premature");

                break;
            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                SPDLOG_ERROR("Unknown event called in state S_WaitOnMessage");

                break;
            }
            break;
        }
        case S_Read:
        {
            _mainwindow->set_progressbar(40);

            // Check if the server has received the data and put it in the receive queue.
            if (!_queue_server->empty())
            {
                // Retrieve the data from the receive queue and store it locally.
                _data = _queue_server->pop_front();

                // Determine what kind of message/data is received based on the id.
                switch (_data._id)
                {
                case msg_id::msg1: /* The message contains a face image and age. */
                {
                    nextState = S_TakeSelfie;
                    event_out = E_NO;

                    if (!_msg1.parse(_data))
                    {
                        // The data is corrupted therefore the parse failed.
                        nextState = S_Reset;
                        event_out = E_STOP;
                        spdlog::info("Parsing failed, discard data and restart the verification proces");
                    }

                    // Save the image to disk
                    std::vector<char> ve_data(_msg1.image_data().begin(), _msg1.image_data().end());
                    std::ofstream output(_image_path, std::ios::binary);
                    std::copy(
                        ve_data.begin(),
                        (ve_data.end() - 6), /* Workaround, last 6 bytes corrupt the jpg image, becuase the vector is appended with suffix at closing the ostream */
                        std::ostreambuf_iterator<char>(output));

                    _mainwindow->enable_capture_button();
                    spdlog::info("Data received succesfully, press the 'Capture' button to continue");

                    break;
                }
                case msg_id::msg2: /* This id has not yet been defined. */
                {
                    nextState = S_ERROR;
                    event_out = E_CONTINUE;

                    /*
                    * This id + message data has not yet been defined.
                    */

                    break;
                }
                default:
                {
                    nextState = S_Reset;
                    event_out = E_CANCEL;
                    spdlog::info("A unknow message is received. \nTry the verification proces again!");

                    break;
                }
                }
            }
            else
            {
                nextState = S_ERROR;
                event_out = E_CONTINUE;
            }
            break;
        }
        case S_TakeSelfie:
        {
            _mainwindow->set_progressbar(60);

            switch (event)
            {
            case E_TAKESELFIE:
                nextState = S_Verify;
                event_out = E_CONTINUE;

                // Capture webcam frame, compute face embedding and pass it to the _verifier.
                _embedder.set_imagery_handler(std::move(std::move(std::make_unique<io_web_cam>())));
                _embedder.embed(_camera_index);
                _verifier.set_embedding1(_embedder.get_face_embedding());
                _face1 = _embedder.get_cropped_image(); /* Store the cropped face image to display it in the QT GUI */

                _mainwindow->disable_cancel_button();
                _mainwindow->disable_stop_button();
                _mainwindow->disable_caputer_button();
                _mainwindow->set_progressbar(80);

                break;

            case E_CANCEL:
                nextState = S_Reset;
                event_out = E_CANCEL;

                // No need to keep the client's face image stored on the harddrive anymore.
                std::remove(_image_path.c_str());

                break;

            case E_STOP:
                nextState = S_Reset;
                event_out = E_STOP;

                // No need to keep the client's face image stored on the harddrive anymore.
                std::remove(_image_path.c_str());

                break;

            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                // No need to keep the client's face image stored on the harddrive anymore.
                std::remove(_image_path.c_str());

                break;
            }
            break;
        }
        case S_Verify:
        {
            nextState = S_Result;
            event_out = E_NO;

            // Load client's face image, compute embedding and pass it to the _verifier.
            _embedder.set_imagery_handler(std::move(std::move(std::make_unique<io_image>())));
            _embedder.embed(_image_path);
            _verifier.set_embedding2(_embedder.get_face_embedding());
            _face2 = _embedder.get_cropped_image(); /* Store the cropped face image to display it in the QT GUI later*/

            // No need to keep the client's face image stored on the harddrive anymore, because the face embedding is computed and in memory.
            std::remove(_image_path.c_str());

            // Display in the GUI which two faces will be verified.
            _mainwindow->set_qlabel_image(_face2);
            _mainwindow->set_qlabel_webcam(_face1);

            spdlog::info("Compare the captured image with the image in the client's data");

            // Verify the face embeddings and get the result.
            _result = _verifier.verify();

            // Display the result.
            if (_result)
            {
                _mainwindow->set_qlabel_result_fv("match");
                _mainwindow->set_color_qlabel_result_fv("color: green;");
            }
            else
            {
                _mainwindow->set_qlabel_result_fv("no match");
                _mainwindow->set_color_qlabel_result_fv("color: red;");
            }

            spdlog::info("Face verification succesfully performed.\nAre the faces the same? {0}", _result);
            spdlog::info("Client's age = {0:d}", _msg1.age());

            // Display if the person is old enough to buy spirits.
            if (_result == true && _msg1.age() >= 18)
            {
                spdlog::info("The client is allowed to buy spirits");
                _mainwindow->set_qlabel_result_atbl("yes");
                _mainwindow->set_color_qlabel_result_atbl("color: green;");
            }
            else
            {
                spdlog::info("The client is not allowed to buy spirits");
                _mainwindow->set_qlabel_result_atbl("no");
                _mainwindow->set_color_qlabel_result_atbl("color: red;");
            }

            spdlog::info("Push the 'Reset' button to continue");

            _mainwindow->disable_caputer_button();
            _mainwindow->enable_reset_button();
            _mainwindow->set_progressbar(100);

            break;
        }
        case S_Result:
        {
            switch (event)
            {
            case E_RESETRESULTS:
                nextState = S_Reset;
                event_out = E_FINISHED;

                break;

            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                break;
            }
            break;
        }
        case S_Reset:
        {
            _result = false;
            _face1.set_size(0, 0);
            _face2.set_size(0, 0);
            _embedder.clear();
            _verifier.clear();
            _data._data.clear();
            _data._id = msg_id::msgDefault;
            _msg1.Clear();

            _mainwindow->set_qlabel_connection("no connection");
            _mainwindow->set_qlabel_result_fv("x");
            _mainwindow->set_color_qlabel_result_fv("color: black;");
            _mainwindow->set_qlabel_result_atbl("x");
            _mainwindow->set_color_qlabel_result_atbl("color: black;");
            _mainwindow->set_progressbar(0);
            _mainwindow->reset_qlabel_image();
            _mainwindow->reset_qlabel_webcam();

            if (_server.client_is_connected())
            {
                _server.disconnect_client();
            }

            switch (event)
            {
            case E_STOP:
                nextState = S_Start;
                event_out = E_NO;

                _server.new_connection_flag = false;

                _mainwindow->disable_all_buttons();
                _mainwindow->enable_start_button();
                _mainwindow->enable_apply_index_button();
                _mainwindow->enable_apply_path_button();
                _mainwindow->enable_clear_button();
                spdlog::info("Stop button pushed");

                break;
            case E_CANCEL:
                nextState = S_WaitForConnection;
                event_out = E_NO;

                _server.new_connection_flag = true;

                _mainwindow->disable_all_buttons();
                _mainwindow->enable_stop_button();
                _mainwindow->enable_clear_button();
                spdlog::info("Cancel button pushed");
                spdlog::info("Waiting on a client to connect");

                break;
            case E_DISCONNECT:
                nextState = S_Start;
                event_out = E_NO;

                _server.new_connection_flag = false;

                _mainwindow->disable_all_buttons();
                _mainwindow->enable_start_button();
                _mainwindow->enable_apply_index_button();
                _mainwindow->enable_apply_path_button();
                _mainwindow->enable_clear_button();
                spdlog::info("Client disconnected from the server");

                break;
            case E_FINISHED:
                nextState = S_WaitForConnection;
                event_out = E_NO;

                _server.new_connection_flag = true;

                _mainwindow->disable_all_buttons();
                _mainwindow->enable_stop_button();
                _mainwindow->enable_clear_button();
                spdlog::info("Face verification finished");
                spdlog::info("Waiting on a client to connect");

                break;
            default:
                nextState = S_ERROR;
                event_out = E_CONTINUE;

                break;
            }
            break;
        }
        case S_ERROR:
        {
            /* 
            This are errors generated by the statemachine's handle_event() or statemachine() functie.
            Errors like unknow states and event are caught here.

            Errors which are generated by object the statemachine uses are handled by the try{}Catch{} mechanishm.
            */

            nextState = S_Reset;
            event_out = E_NO;

            spdlog::info("An error occured, stop the application");
            _mainwindow->disable_all_buttons();
            _mainwindow->enable_start_button();

            break;
        }
        default:
        {
            // A non existing event happend, reset the application.
            nextState = S_Reset;
            event_out = E_NO;

            _mainwindow->disable_all_buttons();
            _mainwindow->enable_start_button();
            spdlog::info("The statemachine is in an unknow state, stop the application");
            
            break;
        }
        }
    }
    /* The next part handles all errors generated by objects that the statemachine uses */
    catch (const nce::network_lib_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case nce::error_code::err_load_KEY_FILE:
            spdlog::error("ERROR = err_load_KEY_FILE");
            spdlog::info("Loading key pair failed (for TCP/IP over TLS communication).");
            spdlog::info("Make sure to use the correct key file, the paths to the .key files are correct and the keys are not expired.");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case nce::error_code::err_load_CERT_FILE:
            spdlog::error("ERROR = err_load_CERT_FILE");
            spdlog::info("Loading certificate failed (for TCP/IP over TLS communication).");
            spdlog::info("Make sure to use the correct certificate file, the paths to the .crt file is correct and the certificate is not expired.");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case nce::error_code::err_start_server:
            spdlog::error("ERROR = err_start_server");
            spdlog::info("Failed to setup and start the server.");
            spdlog::info("Close the application and try again, or contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::DNN_ResNet_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_deserialize_net:
            spdlog::error("ERROR = err_deserialize_net");
            spdlog::info("Failed to setup the net module (which computes the face embedding).");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case fve::error_code::err_embed_face:
            spdlog::error("ERROR = err_embed_face");
            spdlog::info("Failed to embed a cropped image, make sure a cropped image is set before calling an embed() function.");
            spdlog::info("Try again or close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::euclidean_distance_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_no_face_embeddings_available:
            spdlog::error("ERROR = err_no_face_embeddings_available");
            spdlog::info("Failed to use the computed face embeddings, make sure there is an face embedding set.");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::face_embedder_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_construct_embedder:
            spdlog::error("ERROR = err_construct_embedder");
            spdlog::info("Failed to construct the _face_embedder object");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::face_verifier_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_construct_verifier:
            spdlog::error("ERROR = err_construct_verifier");
            spdlog::info("Failed to construct the _face_verifier object");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::HOG_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_deserialize_hog:
            spdlog::error("ERROR = err_deserialize_hog");
            spdlog::info("Failed to setup the face detector.");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case fve::error_code::err_detect_face:
            spdlog::error("ERROR = err_detect_face");
            spdlog::info("Failed detect a face and crop the face.");
            spdlog::info("Make sure the image has a clear frontal face without any accesoires and try again.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case fve::error_code::err_no_cropped_image_available:
            spdlog::error("ERROR = err_no_cropped_image_available");
            spdlog::info("There is no image cropped yet, therefore a cropped image is not available.");
            spdlog::info("Make sure an image is detected and cropped before calling the get_cropped_image() function.");
            spdlog::info("Close the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::io_web_cam_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_load_webcam:
            spdlog::error("ERROR = err_load_webcam");
            spdlog::info("Camera not found, specify the correct camera index and try again.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        case fve::error_code::err_capture_frame:
            spdlog::error("ERROR = err_capture_frame");
            spdlog::info("Failed to capture a frame from the webcam, the webcam stream is not open.");
            spdlog::info("Make sure the camera is connected and specify the camera index.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const fve::io_image_excp &e)
    {
        SPDLOG_ERROR(e.what());

        switch (e.error_code())
        {
        case fve::error_code::err_load_img:
            spdlog::error("ERROR = err_load_img");
            spdlog::info("Failed to load the image specified in the path to image identity form.");
            spdlog::info("Make sure the path is right, otherwise contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        default:
            spdlog::error("ERROR = default");
            spdlog::info("Unknown error occured, stop the application and contact the developer.");
            nextState = S_Reset;
            event_out = E_STOP;
            break;
        }
    }
    catch (const std::exception &e)
    {
        spdlog::error("ERROR = std::exception");
        spdlog::info("Unknown error occured, stop the application and contact the developer.");
        nextState = S_Reset;
        event_out = E_STOP;
    }

    _cur_state = nextState;
    return event_out;
}

state_s fsm::get_current_state() const
{
    return _cur_state;
}

void fsm::set_path(const std::string path)
{
    _image_path = path;
}

void fsm::set_index(const std::string index)
{
    _camera_index = index;
}

std::string fsm::get_path()
{
    return _image_path;
}

std::string fsm::get_index()
{
    return _camera_index;
}
