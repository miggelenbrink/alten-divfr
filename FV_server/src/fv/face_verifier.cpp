#include "../../include/fv/face_verifier.hpp"

face_verifier::face_verifier()
{
    try
    {
        /* set default implementations for the interfaces */
        set_matcher(std::move(std::make_unique<euclidean_distance>()));
        spdlog::debug("Construct face_verifier");
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Failed to construct face embedder");
        throw fve::face_verifier_excp(e.what(), fve::error_code::err_construct_verifier);
    }
}

face_verifier::~face_verifier()
{
    spdlog::debug("Deconstruct face_verifier");
}

face_verifier::face_verifier(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2)
{
    set_embeddings(std::move(emb1), std::move(emb2));
}

bool face_verifier::verify(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2)
{
    return _matcher->match_embeddings(std::move(emb1), std::move(emb2));
    //return (*_matcher)(emb1, emb2);
    //set_embeddings(emb1, emb2);
    //return _matcher->match_embeddings();
}

bool face_verifier::verify()
{
    return _matcher->match_embeddings();
}

void face_verifier::set_embedding1(const dlib::matrix<float, 0, 1> emb1)
{
    _matcher->set_embedding1(std::move(emb1));
}
void face_verifier::set_embedding2(const dlib::matrix<float, 0, 1> emb2)
{
    _matcher->set_embedding2(std::move(emb2));
}
void face_verifier::set_embeddings(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2)
{
    _matcher->set_embeddings(std::move(emb1), std::move(emb2));
}

void face_verifier::set_matcher(std::unique_ptr<i_matcher> obj)
{
    _matcher = std::move(obj);
}

void face_verifier::clear()
{
    _matcher->clear();
}

void face_verifier::set_threshold(const float threshold)
{
    _matcher->set_threshold(threshold);
}

float face_verifier::get_threshold() const
{
    return _matcher->get_threshold();
}
