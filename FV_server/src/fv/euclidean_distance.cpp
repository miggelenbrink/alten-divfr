#include "../../include/fv/euclidean_distance.hpp"

euclidean_distance::euclidean_distance() : _threshold(0.6)
{
    spdlog::debug("Construct euclidean_distance");
}

euclidean_distance::~euclidean_distance()
{
    spdlog::debug("Deconstruct euclidean_distance");
}

bool euclidean_distance::match_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
    return match_embeddings();
}

bool euclidean_distance::operator()(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
    return match_embeddings();
}

void euclidean_distance::set_embedding1(const dlib::matrix<float, 0, 1> face_emb1)
{
    _face_emb1 = std::move(face_emb1);
    spdlog::debug("Set face embedding 1");
}

void euclidean_distance::set_embedding2(const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb2 = std::move(face_emb2);
    spdlog::debug("Set face embedding 2");
}

void euclidean_distance::set_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2)
{
    _face_emb1 = std::move(face_emb1);
    _face_emb2 = std::move(face_emb2);
    spdlog::debug("Set face embeddings 1 and 2");
}

bool euclidean_distance::match_embeddings() const
{
    if (_face_emb1.size() == 0 | _face_emb2.size() == 0)
    {
        SPDLOG_ERROR("The two face embeddings to compare are not both set");
        throw fve::euclidean_distance_excp(fve::error_code::err_no_face_embeddings_available);
    }
    try
    {
        if (dlib::length((_face_emb1 - _face_emb2)) < _threshold)
        {
            spdlog::debug("Face embbedings 1 and 2 match");
            return true;
        }
        else
        {
            spdlog::debug("Face embbedings 1 and 2 don't match");
            return false;
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Computing euclidean_distance failed");
        throw fve::euclidean_distance_excp(e.what(), fve::error_code::err_compute_euclidean_distance);
    }
}

void euclidean_distance::set_threshold(const float threshold)
{
    // @todo uitzoeken of threshold tussen bepaald limiet moet liggen? bv tussen 0 en 20?
    // testen in unit test.
    _threshold = threshold;
    spdlog::debug("Threshold is set to", _threshold);
}

float euclidean_distance::get_threshold() const
{
    return _threshold;
}

void euclidean_distance::clear()
{
    _face_emb1 = 0;
    _face_emb2 = 0;
}