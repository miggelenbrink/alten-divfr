#include "../../include/fv/io_web_cam.hpp"

io_web_cam::io_web_cam()
{
    spdlog::debug("Construct io_web_cam");
}

io_web_cam::~io_web_cam()
{
    spdlog::debug("Deconstruct io_web_cam");
}

dlib::matrix<dlib::rgb_pixel> io_web_cam::get_image() const
{
    spdlog::debug("Get image");
    return _image;
}

dlib::matrix<dlib::rgb_pixel> io_web_cam::load(const std::string input)
{
    try
    {
        if (open(std::stoi(input)))
        {
            return capture_frame();
        }
        else
        {
            throw fve::io_web_cam_excp(fve::error_code::err_load_webcam);
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_WARN("Failed to open the webcam with index: {0}", input);
        throw fve::io_web_cam_excp(e.what(), fve::error_code::err_load_webcam);
    }
}

void io_web_cam::operator>>(dlib::matrix<dlib::rgb_pixel> &get_img)
{
    capture_frame();
    get_img = _image;
    spdlog::debug("Capture image");
}

bool io_web_cam::open(const int index)
{
    try
    {
        if (_web_cam.open(index, cv::CAP_V4L2))
        {
            spdlog::debug("Opened webcam with index {0:d} succesfully", index);
            return true;
        }
        else
        {
            SPDLOG_WARN("Failed to open the webcam with index: {0:d}", index);
            return false;
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Opening webcam failed");
        throw fve::io_web_cam_excp(e.what(), fve::error_code::err_load_webcam);
    }
}

dlib::matrix<dlib::rgb_pixel> io_web_cam::capture_frame()
{
    try
    {
        if (_web_cam.isOpened())
        {
            if (!_web_cam.read(_image_openCV))
            {
                SPDLOG_WARN("Failed to capture a frame, webcam stream is not open");
                throw fve::io_web_cam_excp(fve::error_code::err_isopen_webcam);
            }
            else
            {
                /**
                 * Convert OpenCV image object into Dlib image object.
                 * Convert OpenCV cv::Mat object into dlib::matrix<T>.
                 * @param cvMat_image IN OpenCV image object
                 * @return opened image as dlib::matrix<rgb_pixel> object. 
                 */
                dlib::cv_image<dlib::bgr_pixel> img(_image_openCV);
                dlib::assign_image(_image, img);

                // dlib::cv_image<dlib::rgb_pixel> img(_image_openCV);
                // dlib::assign_image(_image, dlib::cv_image<dlib::rgb_pixel>(img));
                spdlog::debug("Succesfully captured a frame from the webcam");
                return _image;
            }
        }
        else
        {
            SPDLOG_WARN("Webcam stream is not open");
            throw fve::io_web_cam_excp(fve::error_code::err_isopen_webcam);
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_WARN("Capturing frame from webcam failed");
        throw fve::io_web_cam_excp(e.what(), fve::error_code::err_capture_frame);
    }
}

size_t io_web_cam::size() const
{
    spdlog::debug("Return size the webcam's captured frame");
    return _image.size();
}

void io_web_cam::clear()
{
    _image.set_size(0, 0);
}