#include "../../include/fv/DNN_ResNet.hpp"

DNN_ResNet::DNN_ResNet()
{
    spdlog::debug("Construct DNN_ResNet");
    try
    {
        dlib::deserialize(PATH_RESNET_MODEL) >> _ResNet_dnn; /**< the trained network weights/parameters/model dlib_face_recognition_resnet_model_v1.dat is/are loaded into the network */
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Loading DNN_ResNet dlib_face_recognition_resnet_model_v1.dat model failed");
        throw fve::DNN_ResNet_excp(e.what(), fve::error_code::err_deserialize_net);
    }
}

DNN_ResNet::~DNN_ResNet()
{
    spdlog::debug("Deconstructor DNN_ResNet");
}

void DNN_ResNet::set_face_image(dlib::matrix<dlib::rgb_pixel> image)
{
    spdlog::debug("Image cropped around face set");
    _face_image = std::move(image);
    create_face_embedding();
}

void DNN_ResNet::operator<<(dlib::matrix<dlib::rgb_pixel> set_img)
{
    spdlog::debug("Image cropped around face set");
    _face_image = std::move(set_img);
    create_face_embedding();
}

dlib::matrix<dlib::rgb_pixel> DNN_ResNet::get_face_image()
{
    spdlog::debug("Get cropped image around face set");
    return _face_image;
}

dlib::matrix<float, 0, 1> DNN_ResNet::get_face_embedding()
{
    spdlog::debug("Get face embedding");
    return _face_embedding;
}

void DNN_ResNet::operator>>(dlib::matrix<float, 0, 1> &get_face_embedding)
{
    spdlog::debug("Get cropped image around face set");
    get_face_embedding = _face_embedding;
}

dlib::matrix<float, 0, 1> DNN_ResNet::embed_face(const dlib::matrix<dlib::rgb_pixel> in_face_image)
{
    // Convert face image into a 128D vector, aka face embedding.
    // The _ResNet_dnn Deep Neural Network implementation computes the embedding.
    _face_image = std::move(in_face_image);
    create_face_embedding();
    spdlog::debug("Embedded image");

    return _face_embedding;
}

void DNN_ResNet::create_face_embedding()
{
    try
    {
        if (_face_image.size() == 0)
        {
            SPDLOG_WARN("No face image is set");
            throw fve::DNN_ResNet_excp(fve::error_code::err_no_cropped_image_set);
        }
        else
        {
            // Convert face image into a 128D vector, aka face embedding.
            // The _ResNet_dnn Deep Neural Network implementation computes the embedding.
            _face_embedding = _ResNet_dnn(_face_image);
            spdlog::debug("Created a face embedding");
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Computing face embedding failed");
        throw fve::DNN_ResNet_excp(e.what(), fve::error_code::err_embed_face);
    }
}

void DNN_ResNet::clear()
{
    _face_embedding = 0;
    _face_image.set_size(0,0);
}
