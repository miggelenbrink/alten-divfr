#include "../../include/fv/io_image.hpp"

io_image::io_image()
{
    spdlog::debug("Construct io_image");
}
io_image::~io_image()
{
    spdlog::debug("Deconstruct io_image");
}

dlib::matrix<dlib::rgb_pixel> io_image::load(const std::string input)
{
    try
    {
        dlib::load_image(_image, input);
        spdlog::debug("Loaded image {0} succesfully", input);
    }
    catch (const std::exception &e)
    {
        SPDLOG_WARN("Loading image failed");
        throw fve::io_image_excp(e.what(), fve::error_code::err_load_img);
    }

    return _image;
}

dlib::matrix<dlib::rgb_pixel> io_image::get_image() const
{
    spdlog::debug("Get image");
    return _image;
}

void io_image::operator>>(dlib::matrix<dlib::rgb_pixel> &get_img)
{
    spdlog::debug("Get image");
    get_img = this->_image;
}

size_t io_image::size() const
{
    spdlog::debug("Get size of image");
    return _image.size();
}

void io_image::set_image(const dlib::matrix<dlib::rgb_pixel> img)
{
    spdlog::debug("Set image");
    _image = img;
}

void io_image::operator<<(const dlib::matrix<dlib::rgb_pixel> set_img)
{
    spdlog::debug("Set image");
    this->_image = set_img;
}

void io_image::clear()
{
    _image.set_size(0, 0);
}