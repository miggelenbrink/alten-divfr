#include "../../include/fv/HOG.hpp"

HOG::HOG() : _face_detector(dlib::get_frontal_face_detector())
{
    spdlog::debug("Construct HOG");
    try
    {
        dlib::deserialize(PATH_PREDICTOR_MODEL) >> _sp;
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Loading face predictor shape_predictor_5_face_landmarks.dat model failed");
        throw fve::HOG_excp(e.what(), fve::error_code::err_deserialize_hog);
    }
}

HOG::~HOG()
{
    spdlog::debug("Deconstruct HOG");
}

dlib::matrix<dlib::rgb_pixel> HOG::detect_face(const dlib::matrix<dlib::rgb_pixel> in_image)
{
    set_image(std::move(in_image));
    detect_faces();
    spdlog::debug("Detected one face");
    return get_image_cropped_face();
}

void HOG::set_image(const dlib::matrix<dlib::rgb_pixel> image)
{
    _image = std::move(image);
    detect_faces();
    spdlog::debug("Image is set");
}

void HOG::operator<<(const dlib::matrix<dlib::rgb_pixel> set_img)
{
    _image = std::move(set_img);
    detect_faces();
    spdlog::debug("Image is set");
}

dlib::matrix<dlib::rgb_pixel> HOG::get_image()
{
    spdlog::debug("Get image");
    return _image;
}

dlib::matrix<dlib::rgb_pixel> HOG::get_image_cropped_face()
{
    spdlog::debug("Get image cropped around the face");
    if (_image_cropped_face.size() == 0)
    {
        SPDLOG_WARN("No image cropped around the face available");
        throw fve::HOG_excp(fve::error_code::err_no_cropped_image_available);
    }
    return _image_cropped_face;
}

void HOG::operator>>(dlib::matrix<dlib::rgb_pixel> &get_cropped_img)
{
    spdlog::debug("Get image cropped around the face");
    get_cropped_img = _image_cropped_face;
}

void HOG::detect_faces()
{
    try
    {
        std::vector<dlib::matrix<dlib::rgb_pixel>> faces;
        for (auto face : _face_detector(_image))
        {
            auto shape = _sp(_image, face);
            // We can also extract copies of each face that are cropped, rotated upright,
            // and scaled to a standard size as shown here:
            // http: //dlib.net/face_landmark_detection_ex.cpp.html
            dlib::matrix<dlib::rgb_pixel> face_chip;
            dlib::extract_image_chip(_image, dlib::get_face_chip_details(shape, 150, 0.25), face_chip);
            faces.push_back(std::move(face_chip));
        }
        /*exactly one face*/
        if (faces.size() == 1)
        {
            _image_cropped_face = faces.back();
            faces.pop_back();
            spdlog::debug("Detected one face");
        }
        /*no face*/
        else if (faces.size() == 0)
        {
            SPDLOG_WARN("Detected zero faces, the image must contain exactly one face");
            throw fve::HOG_excp(fve::error_code::err_no_cropped_image_computed);
        }
        else
        /*more than one face*/
        {
            //int temp = faces.size();
            //faces.clear();
            //SPDLOG_WARN("Detected {0:d} faces, the image must contain exactly one face", temp);
            throw fve::HOG_excp(fve::error_code::err_no_cropped_image_computed);
        }
    }
    catch (const std::exception &e)
    {
        SPDLOG_ERROR("Detecting face failed");
        throw fve::HOG_excp(e.what(), fve::error_code::err_detect_face);
    }
}

void HOG::clear()
{
    _image.set_size(0, 0);
    _image_cropped_face.set_size(0, 0);
}