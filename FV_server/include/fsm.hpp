#ifndef FSM_H
#define FSM_H

#include <iostream>
#include <fstream> // open file
#include <cstdio>  // remove file from disk
#include <mutex>   // handle event should not be called paralel/async, therefore use mutexes

#include "spdlog/spdlog.h" /* add global space spdlog to this file, to use created loggers in this file*/

#include "../src/qt/mainwindow.h"

#include "nc/server_api.hpp"
#include "nc/msg_struct.hpp"
#include "nc/queue.hpp"
#include "nc/msg1_handler.hpp"

#include "fv/face_embedder.hpp"
#include "fv/face_verifier.hpp"

/* Default inputs */
#define PATH_IMAGE "../input/fv/images/user.jpg"
#define INDEX_WEBCAM "0"
#define SERVER_PORT 6000

enum state_s
{
    S_NO, S_Initialised, S_Start, S_WaitForConnection,
    S_WaitOnMessage, S_Read, S_TakeSelfie, S_Verify,
    S_Result, S_Reset, S_ERROR
};

/**
* E_INIT: Initialize the statemachine, this event should only be called once.
* E_NO: Wait for user input (event) or external event (callback) in the next state. 
* E_CONTINUE: Go on to the next state and handle the state, do not wait for external event like user input.
*/
enum event_e
{
    E_NO, E_CONTINUE, E_INIT, E_START, E_STOP,
    E_RESETRESULTS, E_CANCEL, E_FINISHED, E_TAKESELFIE,
    E_ONCONNECT, E_DISCONNECT, E_ONMESSAGE, E_ONDISCONNECT
};

// Declare because cross referencing between the fsm and Mainwindow classes.
class MainWindow;

/**
* The fsm machine models the face verification's specified behaviour. 
* 
* See the design documentation for the behaviour.
*/ 
class fsm
{
public:
    fsm(MainWindow *mainwindow);
    ~fsm();

    void handle_event(event_e event);
    state_s get_current_state() const;

    /**
     * Set the path for temporary storing a client's face image.
     * 
     * @param path IN Path relative to the directory this .hpp file is in. 
     */ 
    void set_path(const std::string path);
    /**
     * Set the index of the camera that the application should use. 
     * @see https://docs.opencv.org/master/d8/dfe/classcv_1_1VideoCapture.html for more information on camera indexes.
     * @param index IN Camera's index. 
     */ 
    void set_index(const std::string index);

    /**
     * Get the path of where the client's will be temporary stored during a face verification proces. 
     * @return Path.
     */
    std::string get_path();
    /**
     * Get the camera index which the app uses to access a camera. 
     * @return index.
     */
    std::string get_index();

private:
    event_e statemachine(event_e event);
    state_s _cur_state;
    event_e _cur_event;

    MainWindow *_mainwindow;            /**< QT GUI */

    server_api _server;                 /**< Boost Asio network server */

    nc::queue<msg_data> *_queue_server; /**< Ptr to the _server's messageInQueue. To process received data */
    msg1_handler _msg1;                 /**< Protobuf class to parse and serialize data */
    msg_data _data;                     /**< Store serialized data */

    face_embedder _embedder;
    face_verifier _verifier;

    std::string _image_path;            /**< The user's face identity image is temporay stored at this location, @todo Convert binary data to OpenCV cv::mat object and converte that to dlib::matrix<> for the face_embedder */  
    std::string _camera_index;          /**< The webcam's/camera's index to use to capture a frame*/

    dlib::matrix<dlib::rgb_pixel> _face1; /**< store the cropped face extracted from the webcam, to show it in the QT GUI */
    dlib::matrix<dlib::rgb_pixel> _face2; /**< store the cropped face extracted from the user's data, to show it in the QT GUI */

    std::mutex mutex_fsm;               /**< lock the fsm, such that the server's callback function cannot start two fsm.handle()/statemachine() parralel */

    bool _result;                       /**< Result face _verifier as bool, true = faces match, false = faces mismatch */
};

#endif // FSM_H
