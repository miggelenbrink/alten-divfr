#include <iostream>
#include "spdlog/spdlog.h"                   /* add global space spdlog to this file, to use created loggers in this file*/
#include "spdlog/sinks/basic_file_sink.h"    /* stdout_color_sink_mt */
#include "spdlog/sinks/stdout_color_sinks.h" /* basic_file_sink_mt */
#include <iomanip>                           /*get current date*/
/* mutex on custom qt logger function */
#include "spdlog/details/null_mutex.h"
#include <mutex>
/* Custom sink/logger that displays log messages to a QT widget */
#include "qt_sink.hpp"
#include "../src/qt/mainwindow.h"

/**
 * Initialize logger that writes stdout and a file which is created daily.
 * 
 * Include "#include <spdlog/spdlog.h>" in every file where you want to use the logger.
 * Because the logger works in the global space of spdlog.
 * More info about spdlog: @see https://github.com/gabime/spdlog/wiki 
 * @param file IN path to log file, a .txt file, if no file exists, one is created with the filename set to the current date.
 * @return Return true if the logger is succesfully initialized, else return false.   
 */
bool init_face_verification_logger(std::string file = "default")
{
    /**
     *   Log level usage:
     * - Trace    Trace flow of code and functions.
     * - Debug    Diagnostic info.
     * - Info     General info (start/stop/open/close service/file).
     * - Warn     Info about error that occured but is recoverable by the app.
     * - Error    Error that stops the application, force shutdown.
     * 
     *  usage logger:
     *   use spdlog::trace("<log message>"), spdlog::debug("<log message>"), etc to write a log message without file location and line number to a sink (e.g. file). 
     *   use SPDLOG_TRACE("<log message>"), SPDLOG_DEBUG("<log message>"), etc to write a log message including file location and line number to a sink (e.g. file). 
     * 
     *  *The logger is set to never write file locations and line numbers to the console, see the set_pattern() function below.
     *   In this way the console is and keeps clear and readable. 
     */
    try
    {
        /* Create stdout logger */
        auto log_console = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        // Log all messages with level trace and higher.
        log_console->set_level(spdlog::level::trace);
        // Format log message:
        // [%^%l%$]: Display log level with color range
        // [%T]:     Display current time
        // %v:       Dispaly log message (textual string)
        log_console->set_pattern("[FV][%^%l%$][%T] %v"); /*FV = Face Verification*/

        /* Create daily file logger */
        if (file == "default")
        {
            auto t = std::time(nullptr);
            auto tm = *std::localtime(&t);

            std::ostringstream oss;
            oss << std::put_time(&tm, "../logs/log_fv_%d_%m_%Y.txt");
            file = oss.str();
        }
        auto log_file = std::make_shared<spdlog::sinks::basic_file_sink_mt>(file, true);
        // Log all messages with level trace and higher.
        log_file->set_level(spdlog::level::trace);
        // custom format which formats the messages
        // [%^%l%$]: Display log level with color range
        // [%D]:     Date
        // [%T]:     Time
        // [%@]:     file and line number
        // %v:       message
        log_file->set_pattern("[FV][%^%l%$][%D][%T][%!][%@] %v"); /*FV = Face Verification*/

        // Create wrapper around the two above mentioned loggers, Now we can write to both sinks with one call.
        std::vector<spdlog::sink_ptr> ptr_to_logs({log_console, log_file});
        auto logger = std::make_shared<spdlog::logger>("FV_logger",
                                                       ptr_to_logs.begin(),
                                                       ptr_to_logs.end());

        // Make sure log messages are written to the sink on an log message with level warn.
        logger->flush_on(spdlog::level::warn);
        // Make the logger availble for the global space of spdlog.h.
        spdlog::set_default_logger(logger);
        // Write all log messages stashed in the log buffer to the sinks every 5 seconds.
        spdlog::flush_every(std::chrono::seconds(5));

        spdlog::info("Logger succesfully initialized");
    }
    catch (const std::exception &e)
    {
        std::cerr << "Failed to initialize spdlog logger: " << e.what() << std::endl;
        return false;
    }
    return true;
}

/**
* Initialize logger that writes stdout, a file which is created daily and a QT plainTextEdit widget.
*
* For more details see the function above and the qt_sink.hpp file. 
*/
bool init_face_verification_logger(MainWindow &qt_app)
{
    try
    {
        auto log_console = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
        log_console->set_level(spdlog::level::trace);
        log_console->set_pattern("[FV][%^%l%$][%T] %v");

        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);

        std::string file;
        std::ostringstream oss;
        oss << std::put_time(&tm, "../logs/log_fv_%d_%m_%Y.txt");
        file = oss.str();

        auto log_file = std::make_shared<spdlog::sinks::basic_file_sink_mt>(file, true);
        log_file->set_level(spdlog::level::trace);
        log_file->set_pattern("[FV][%^%l%$][%D][%T][%!][%@] %v"); 

        auto log_QTwidget = std::make_shared<qt_sink<std::mutex>>(qt_app);
        log_QTwidget->set_level(spdlog::level::info);
        log_QTwidget->set_pattern("[%^%l%$][%T] %v"); 

        std::vector<spdlog::sink_ptr> ptr_to_logs({log_console, log_file, log_QTwidget});
        auto logger = std::make_shared<spdlog::logger>("FV_logger",
                                                       ptr_to_logs.begin(),
                                                       ptr_to_logs.end());

        logger->flush_on(spdlog::level::warn);
        spdlog::set_default_logger(logger);
        spdlog::flush_every(std::chrono::seconds(5));

        spdlog::info("Logger succesfully initialized");
    }
    catch (const std::exception &e)
    {
        std::cerr << "Failed to initialize spdlog logger: " << e.what() << std::endl;
        return false;
    }
    return true;
}