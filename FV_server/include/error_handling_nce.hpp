#ifndef ERROR_HANDLING_NCE_HPP
#define ERROR_HANDLING_NCE_HPP

#include <iostream>
#include <exception>

// nw = network communication error handling.
namespace nce
{
    // Error codes used in the exception's try()catch() mechanism.
    enum error_code
    {
        /* !!! Errors thrown and caught inside the class's member functions, developer that uses an instance/object of the class does not have to catch and handle these erros. */
        /* Start server error codes */
        err_load_KEY_FILE,
        err_load_CERT_FILE,
        err_start_server,

        /* !!! Errors thrown that must be handled outside the class's member function. */
        /* Default */
        err_default,
        err_code_not_initialized
    };

    // Error codes used in the callback function OnErrorFromConnection(), Boost.Asio's async function call the OnErrorFromConnection() with appropiate error code on an error. By using a callback the develop must implement what to do on an error.
    enum errorCodes
    {
        handshake_ERR,
        socket_is_open_ERR,
        write_ERR,
        read_ERR,
        default_ERR,
    };

    /**
     * Custom exception class for the network communication code. 
     * 
     * The derived class allows to throw exceptions with a message and error code.
     * The possible error codes can be found in the enum error_code which is in the namespace nce.
     * The custom exception class has the benefit that a catch block can determine what to do depending on the error code.
     */
    class network_lib_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        nce::error_code _ec;

    public:
        /**
        * Throw an exception with a textual string and nce::error_code.
        * 
        * The function has for all parameters a default value thus only the parameter that is useful should be used. 
        *  
        * @param msg IN Message explaining the error. 
        * @param ec IN Error code beloning to the type of error that occurs.
        */
        network_lib_excp(const std::string msg = "Error message not initialized", const nce::error_code ec = nce::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        network_lib_excp(const nce::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~network_lib_excp() {}

        /**
        * Get the message explaning the error.
        * @return error message
        */
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        /**
        * Get the error code belong to the error that occured. 
        * @return One of the error codes in nce::error_code enum. 
        */
        nce::error_code error_code() const
        {
            return _ec;
        }
    };
}

#endif //ERROR_HANDLING_NCE_HPP
