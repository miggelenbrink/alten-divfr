#ifndef QT_SINK_HPP
#define QT_SINK_HPP

#include <iostream>
#include "spdlog/sinks/base_sink.h"
/* reference to qt mainwindow ui*/
#include "../src/qt/mainwindow.h"

/**
* Custom sink that writes to the QT plainTextEdit widget in the mainwindow class, see mainwindow.h. 
* 
* This class is a custom sink which specifies a planTextEdit QT widget as sink/destination for log messages. 
* The custom sink inherits from the base class base_sink. 
*/
template <typename Mutex>
class qt_sink : public spdlog::sinks::base_sink<Mutex>
{
public:
    qt_sink(MainWindow &qt_app);
    ~qt_sink();

protected:
    void sink_it_(const spdlog::details::log_msg &msg) override;
    void flush_() override;

private:
    MainWindow &_qt_app; /**< Reference to the QT GUI framework MainWindow class */
};

template <typename Mutex>
qt_sink<Mutex>::qt_sink(MainWindow &qt_app): _qt_app(qt_app)
{
}

template <typename Mutex>
qt_sink<Mutex>::~qt_sink()
{
}

template <typename Mutex>
void qt_sink<Mutex>::sink_it_(const spdlog::details::log_msg &msg)
{

    // log_msg is a struct containing the log entry info like level, timestamp, thread id etc.
    // msg.raw contains pre formatted log

    // If needed (very likely but not mandatory), the sink formats the message before sending it to its final destination:
    spdlog::memory_buf_t formatted;
    spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);

    // write the formated msg to the qt widget. 
    _qt_app.set_logger(fmt::to_string(formatted));
}

template <typename Mutex>
void qt_sink<Mutex>::flush_()
{
    /* optional */
}
#endif //QT_SINK_HPP
