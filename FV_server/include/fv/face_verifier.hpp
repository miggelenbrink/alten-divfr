#ifndef FACE_VERIFIER_HPP
#define FACE_VERIFIER_HPP

#include <iostream>
#include "spdlog/spdlog.h"           /* add global space spdlog to this file, to use created loggers in this file*/
#include "../error_handling_fve.hpp" /* custom exceptions for error handling */
#include "../error_handling_nce.hpp" /* custom exceptions for error handling */
/* interfaces */
#include "i_matcher.hpp"
/* implementation of the above interfaces*/
#include "euclidean_distance.hpp"

class face_verifier
{
public:
    face_verifier();
    ~face_verifier();

    /**
     * Construct verifier object and set two face embeddings to compare.
     * The face embeddings are 128D vectors whith features.
     * @param emb1 face embedding 1.
     * @param emb2 face embedding 2. 
     */
    face_verifier(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2);

    /**
     * Set two face embeddings and compare them.
     * The face embeddings are 128D vectors whith features.
     * @param emb1 face embedding 1.
     * @param emb2 face embedding 2. 
     * @return Return true if face embeddings match, else return false.
     */
    bool verify(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2);

    /**
     * Compare the two face embedding member variables _emb1 and _emb2 and return the result.
     * Before calling this this function two face embedding must be set, otherwise an exception is thrown.
     * The face embeddings are 128D vectors whith features.
     * @param emb1 face embedding 1.
     * @param emb2 face embedding 2. 
     * @return Return true if face embeddings match, else return false.
     */
    bool verify();

    /**
     * Set face embedding one. 
     * @param face_emb1 face embedding, aka vector with features.
     */
    void set_embedding1(const dlib::matrix<float, 0, 1> emb1);

    /**
     * Set face embedding two. 
     * @param face_emb2 face embedding, aka vector with features.
     */

    void set_embedding2(const dlib::matrix<float, 0, 1> emb2);

    /**
     * Set the both the face embedding member variables. 
     * @param face_emb1 face embedding, aka vector with features.
     * @param face_emb2 face embedding, aka vector with features.
     */
    void set_embeddings(const dlib::matrix<float, 0, 1> emb1, const dlib::matrix<float, 0, 1> emb2);

    /**
     * Set an i_matcher interface implementation.
     * 
     * By setting an implementation the face_verifier uses that implementation's logic for the object's member: _matcher.
     * The function also allows to change the implementation on runtime, it makes the face_verifier use the most recent set implementation.
     * It means that a call to a member functoin will cause a differnt function to be exectued depending on the type of the object that calls the function, also known as polymorphism. 
     * 
     * @param obj IN an i_matcher implementation, one of the available ones or one created from the i_matcher interface.
     * @note The function parameter is an unique_ptr, therefore an object must transfer it's ownership to the member function using std::move()
     */
    void set_matcher(std::unique_ptr<i_matcher> obj);

    /**
     * Clear all data in the _i_matcher objects.
     */
    void clear();
    
    void set_threshold(const float threshold);

    float get_threshold() const;

private:
    std::unique_ptr<i_matcher> _matcher;
};

#endif //FACE_VERIFIER_HPP