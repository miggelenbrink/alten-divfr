#ifndef I_MATCHER_HPP
#define I_MATCHER_HPP

#include <iostream>
#include "../../third_party/dlib/image_io.h"

class i_matcher
{
private:
    dlib::matrix<float, 0, 1> _face_emb1;
    dlib::matrix<float, 0, 1> _face_emb2;

public:
    virtual bool match_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) = 0;
    virtual bool match_embeddings() const = 0;
    virtual bool operator()(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) = 0;
    virtual void clear() = 0;

    virtual void set_embedding1(const dlib::matrix<float, 0, 1> face_emb1) = 0;
    virtual void set_embedding2(const dlib::matrix<float, 0, 1> face_emb2) = 0;
    virtual void set_embeddings(const dlib::matrix<float, 0, 1> face_emb1, const dlib::matrix<float, 0, 1> face_emb2) = 0;
    virtual float get_threshold() const = 0;
    virtual void set_threshold(const float threshold) = 0;
};

#endif //I_MATCHER_HPP