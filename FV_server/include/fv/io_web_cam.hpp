#ifndef CAPTURE_VIDEO_FRAME_HPP
#define CAPTURE_VIDEO_FRAME_HPP

#include <iostream>
#include "spdlog/spdlog.h"                   /* add global space spdlog to this file, to use created loggers in this file*/
#include "../error_handling_fve.hpp"         /* custom exceptions for error handling */
#include "../error_handling_nce.hpp"         /* custom exceptions for error handling */
#include <opencv2/core.hpp>                  /*cv::VideoCapture capturing frames from the webcam*/
#include <opencv2/videoio.hpp>               /*cv::VideoCapture capturing frames from the webcam*/
#include "../../third_party/dlib/image_io.h" /*dlib::matrix<dlib::rgb_pixel>*/
#include "../../third_party/dlib/opencv.h"   /*cv::VideoCapture capturing frames from the webcam*/
/* interface */
#include "i_io_imagery.hpp"

class io_web_cam : public i_io_imagery
{
public:
    io_web_cam();
    virtual ~io_web_cam() override;

    /**
     * Open videostream on webcam and grab frame from videostream and store it in member variable _image.
     * @param index is the id of the video capturing device to open, to open default camera pass 0.
     * @return return true on succes, false on failure. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> load(const std::string input) override;

    /**
     * Get image.
     * @return return image as dlib::matrix<rgb_pixel> object. 
     */
    virtual dlib::matrix<dlib::rgb_pixel> get_image() const override;

    /**
     * Same functionality as get_image(). 
     */
    virtual void operator>>(dlib::matrix<dlib::rgb_pixel> &get_img) override;

    /**
     * Return size of image in bytes. 
     * @return return size in bytes of the dlib::matrix<T> image object. 
     */
    size_t size() const override;

    /**
     * Open videostream on webcam and return true on succes.
     * @param index is the id of the video capturing device to open, to open default camera pass 0.
     * @return return true on succes, false on failure. 
     */
    bool open(const int index);

    /**
     * Grab frame from videostream, store and return it in member variable _image.
     * Every time the function is called, current captured frame frame are overridden.
     * @return return the captured image.
     */
    dlib::matrix<dlib::rgb_pixel> capture_frame();

    virtual void clear() override;

private:
    dlib::matrix<dlib::rgb_pixel> _image;

    // see "cv_image<bgr_pixel> cimg(temp);" in http://dlib.net/webcam_face_pose_ex.cpp.html for why we need to keep a copy of cv::mat img.
    cv::Mat _image_openCV;

    cv::VideoCapture _web_cam;
};

#endif //CAPTURE_VIDEO_FRAME_HPP