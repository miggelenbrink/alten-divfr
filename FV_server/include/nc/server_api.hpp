#ifndef SERVER_API_HPP
#define SERVER_API_HPP

#include <iostream>
#include "server_handler.hpp"
#include "spdlog/spdlog.h"              /* add global space spdlog to this file, to use created loggers in this file*/
#include "../error_handling_nce.hpp"    /* custom exceptions for error handling */

class server_api : public server_handler
{
public:
    server_api(int port);
    ~server_api();

    bool new_connection_flag;

private:
    // customize the amout of clients the server can accept. currently only one!
    virtual bool onClientConnect();
    virtual void onClientMessage();
    virtual void onClientDisconnect();
};

#endif //SERVER_API_HPP