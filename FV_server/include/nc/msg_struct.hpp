/** @file msg_struct.hpp 
* @author Guido
* @date   18-04-21
*
* @todo
*/

#ifndef MSG_STRUCT_HPP
#define MSG_STRUCT_HPP

#include <iostream>

/*
*   Enum msg_id: message types used for the message exchange between a client and server.
*
*   The msg_id enum contains all possible message types(classes) which the client and server can use to communicate.
*   A user can create a new message type by generating a protobuf c++ class and wrap it into 
*   the msg<type>_handler.hpp class. Subsequently the user must manualy add the created message type here 
*   in the enum. The message type is used as an ID number and send along with the data. Because 
*   of the ID(messageType) the server and client know how to parse and serialize the data. Therefore it is 
*   IMPORTANT that the message types are equal to and have the same order as the message in both the client and server application. 
*   
*   - id/type msg1 stands for message of the type(class) msg1_handler which wraps the protobuf message.pb.h class. 
*   - id/type <ID_name> stands for messages of the type(class) <class_name> which wrap the protobuf <generate protobuf class> class. 
*/
enum msg_id
{
    msgDefault,
    msg1,
    msg2,
};

struct msg_data
{
    msg_id _id;
    std::string _data;
};

#endif //MSG_STRUCT_HPP