/** @file msg1_handler.hpp
* Wrapper class around protobuf generated class.
* See the documentation and msg_struct.hpp file. 
* 
* @author Guido
* @date   25-04-21
*
* @note To use a generated protobuf class as base class(inheritance), delete the 'final'  keyword in the protobuf classes.
* @todo Follow the instruction indicated by '-->'.
*/

#ifndef S_MESSAGE_HPP
#define S_MESSAGE_HPP

#include <iostream>
#include "msg_struct.hpp"
//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/

/* --> Include the generated protobuf class. */
// #include "<name>.pb.h"

class msg1_handler : public /* --> specify the generated protobuf class name here (inheritance) */
{
public:
    msg1_handler(msg_id id = msg_id::msg1);
    ~msg1_handler();

    // size of the header variable in bytes

    size_t sizeof_header();
    // size of the id variable in bytes
    size_t sizeof_id();
    // size of the payload variable in bytes
    size_t sizeof_payload();

    bool parse(msg_data data);
    msg_data serialize();

private:
    msg_data _msg_data;
};

#endif //S_MESSAGE_HPP