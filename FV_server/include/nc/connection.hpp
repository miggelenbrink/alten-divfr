/** @file connection.hpp 
* @author Guido
* @date   22-03-21
*
* @todo
*/

#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <functional> // callback function

#include "spdlog/spdlog.h"           /* add global space spdlog to this file, to use created loggers in this file*/
#include "../error_handling_nce.hpp" /* custom exceptions for error handling */

#include "queue.hpp"
#include "msg_struct.hpp"

/**
* The connection object handles the socket's handshake, read and write operations.
*
* A connection object is created when a client connects to the server.
* The first task of the connection object is to perform a TLS handshake with the client.
* If the handshake fails the connection object must be destroyed, else the connection object is ready to handle read and write requests. 
*
* The object is contructed by passing a socket to it, from then on it handles the socket.
* Futher the connection object has msgOutQeue and a reference to a msgInQueue.
* Data that is received from a client on the connection object's socket is async read by the connection object and stored in the msgInQueue.
* Data that must be written to a client is queued in the msgOutQueue and when the socket is ready, the socket writes async the data to the client. 
*
* If a client disconnects, the connection object should be deconstructed, this closes the socket. 
*/
class connection
{
public:
    /**
    * Constructor. 
    * Initialize a boost::asio::ssl::stream<boost::asio::ip::tcp::socket> by moving it into socket object
    *.Futher is creates a reference to a msgInQueue where the connection object will store received messages.
    * Because of the referene the msgInQueue is accesable outside of this class and so the messages can be processed outside the connection object.
    * Last, the constructor initializes a callback function for errorhandling. 
    * On all errors that occur the connection member function call the callback function with an appropriate error code.
    * The error codes are in the @see error_handling_nce.hpp file. 
    * @param socket A ssl stream socket object.
    * @param msgInqueue A queue of the type nc::queue.
    * @param callback A callback function having one parameter with the type nce::errorCodes. 
    */
    connection(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket,
               nc::queue<msg_data> &msgInQueue,
               std::function<void(nce::errorCodes ec)> callback);

    ~connection();

    /**
     * Determine if the socket is open.
     * @return Return true if the socket is open, else return false. 
     */
    bool is_socket_open();
    /**
     * Close the socket if it is open, othwer do nothing.
     */
    void stop_socket();
    /**
     * Perform a SSL handshake with the peer connection.  
     * If the handshake is succesful then the connection object's socket start waiting to read received data. 
     * If the handshake fails the callback function is called with nce::errorCodes::handshake_ERR.
     * In this way the class that controls the connection object can determine what to do, probably destory the connection object.
     */
    void start_handshake();
    /**
     * Start checking for received data,
     * if data is received in the socket's stream/buffer then call read_header().
     */
    void start_reading();
    /**
     * This function should be called after pushing data into the _msgOutQueue, using  push_writeQueue(). 
     * Otherwise it has no effect. 
     * This function calls the private read_header function. 
     */
    void start_write();
    /**
     * Write messages/data to _msgOutQueue that are ready to be tranmitted to the client.
     * Messages are only transmitted after calling the start_write() function. 
     * @param value Serialized data ready to be send to the client. 
     */
    void push_writeQueue(const msg_data &value);
    /**
     * Check if the _msgOutQueue has data in it.
     * @return Return true if there is data in the queue, else return false. 
     */
    bool writeQueue_empty();
    /**
     * Delete the oldest pushed message from the queue. 
     */
    void pop_front_WriteQueue();

private:
    /**
    * Read a header from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of bytes of the socket_stream.
    * When a valid header is received it calls the read_id function, else it calls the callback function with an appropriate error code.
    * A header is valid if the function is be able to read the fixed size of bytes from the socket stream. 
    */
    void read_header();
    /**
    * Read a id from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of the socket_stream. 
    * When a valid id is received it calls the read_payload function.
    */
    void read_id();
    /**
    * Read the payload from the socket-stream.
    * This function reads number of bytes specefied in the header of the socket_stream.
    * When the payload is succesfully read the header, id and payload are pushed to the msgInQueue as an msg_data structure. 
    * 
    * The read functions read the number of bytes that they are assigned to read from the socket stream. They do not check if the actual data has a meaning,
    * The functions are asynchronously, therefore the functions return immediatly and in another thread the functions wait until all bytes are received and read.
    * 
    * When all bytes are received the the async read function calls the next read function. the order is read_header() --> read_id() --> read_payload().
    * After reading a payload the read_header() function is called again, and waits for a new header to receive. 
    */
    void read_payload();

    /**
    * Write a header on the socket-stream.
    * The function points to the oldest message in the _msgOutQueue and sends the message's (msg_data structure) header to the socket stream. 
    * The write is performed asynchronously, on succes the write_id() function is called.
    * On failure the callback function is called with an appropriate error code.
    */
    void write_header();
    /**
    * Write a id on the socket-stream.
    * The function points to the oldest message in the _msgOutQueue and sends the message's (msg_data structure) id to the socket stream. 
    * The write is performed asynchronously, on succes the write_payload() function is called.
    * On failure the callback function is called with an appropriate error code.
    */
    void write_id();
    /**
    * Write a header on the socket-stream.
    * The function points to the oldest message in the _msgOutQueue and sends the message's (msg_data structure) payload to the socket stream. 
    * The write is performed asynchronously, on succes the msg_data structure is deleted from the _msgOutQueue and the async function finishes. 
    * On failure the callback function is called with an appropriate error code.
    */
    void write_payload();

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
    nc::queue<msg_data> &_msgInQueue;
    nc::queue<msg_data> _msgOutQueue;

    size_t _header; //*< Temp variables to reconstruct a msg_data structure from the received data. */
    msg_data _msg;  //*< Temp variables to reconstruct a msg_data structure from the received data. */

    std::function<void(nce::errorCodes ec)> _callback_func;
};

#endif //CONNECTION_HPP