/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo create function that can open a socket again and connecxt to server, in progress. // void client_interface::reopen_socket_and_connect_to_server(const std::string _ip, const short _port)
*/

#ifndef SERVER_INTERFACE_HPP
#define SERVER_INTERFACE_HPP

#include <iostream>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <chrono>
#include <QTimer>
#include <QObject>

#include "queue.hpp"
#include "msg_struct.hpp"
#include "connection.hpp"

#include "spdlog/spdlog.h"           /* add global space spdlog to this file, to use created loggers in this file*/
#include "../error_handling_nce.hpp" /* custom exceptions for error handling */

#define CERT_FILE "../input/nc/certificate/server.crt"
#define KEY_FILE "../input/nc/certificate/server.key"

class server_handler
{
    //Q_OBJECT
public:


    server_handler(int port);
    ~server_handler();

    //void dowork();

    void start_server();
    void stop_server();

    bool client_is_connected();
    void disconnect_client();

    void send(const std::string data, const msg_id id);
    nc::queue<msg_data> *get_msgInQueue_ptr();

    std::string get_client_address();

    void set_onClientConnect_callback(std::function<void(void)> onClientConnect_callback);
    void set_onClientMessage_callback(std::function<void(void)> onClientMessage_callback);
    void set_onClientDisconnect_callback(std::function<void(void)> onClientDisconnect_callback);
    void set_onHandshakeFailed_callback(std::function<void(void)> onHandshakeFailed_callback);

protected:
    // callback function, called on a new client connection/session
    virtual bool onClientConnect();
    virtual void onClientMessage();
    // callback function, called on ERROR
    virtual void onErrorConnection(nce::errorCodes ec);

    std::function<void(void)> _onClientConnect_callback;
    std::function<void(void)> _onClientMessage_callback;
    std::function<void(void)> _onClientDisconnect_callback;
    std::function<void(void)> _onHandshakeFailed_callback;

private:
    void do_accept();

    nc::queue<msg_data> _msgInQueue;

    boost::asio::io_context _io_context;
    boost::asio::ssl::context _ssl_context;
    boost::asio::ip::tcp::acceptor _acceptor;
    std::shared_ptr<connection> _connection;
    std::string _client_address;

    std::thread _threadContext;
    // threadC0ntext roept qt functies aan via de callbakc, gebruik qtimer want die exctue in de qt mainthread.
    QTimer _qt_timer_thread;
    
    std::thread _threadOnMessage;
    bool _threadOnMessage_flag;


};

#endif //SERVER_INTERFACE_HPP