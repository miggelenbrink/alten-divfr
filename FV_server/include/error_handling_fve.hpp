#ifndef ERROR_HANDLING_FVE_HPP
#define ERROR_HANDLING_FVE_HPP

#include <iostream>
#include <exception>

/**
 * Custom exception class for the face verification code.
 *  
 * The classes allow catch blocks to determine what type of error occured.
 * In this way the application can determine what action to undertake depending on the error code.
 * 
 * Each class has a dedicated derived exception implementation, despite their implementations are nearly the same. 
 * This is just for convience and keeping error codes specefic to a class.
 * 
 * Derived exception classes in this file:
 * - DNN_ResNet_excp
 * - euclidean_distance_excp
 * - face_embedder_excp
 * - face_verifier_excp
 * - HOG_excp
 * - io_web_cam_excp
 * - io_image_excp
 */

// fve = face verification error handling
namespace fve
{
    enum error_code
    {
        /* !!! Errors thrown that must be handled outside the class's member function. */
        /* DNN_ResNet_excp error codes */
        err_deserialize_net,
        err_embed_face,
        /* euclidean_distance_excp error codes */
        err_no_face_embeddings_available,
        /* face_embedder_excp error codes */
        err_construct_embedder,
        /* face_verifier_excp error codes */
        err_construct_verifier,
        /* HOG_excp error codes */
        err_deserialize_hog,
        err_detect_face,
        err_no_cropped_image_available,
        /* io_web_cam_excp error codes */
        err_load_webcam,
        err_capture_frame,
        /* io_image_excp error codes */
        err_load_img,

        /* !!! Errors thrown and caught inside the class's member functions, developer that uses an instance/object of the class does not have to catch and handle these erros. */
        /* DNN_ResNet_excp error codes */
        err_no_cropped_image_set,
        /* euclidean_distance_excp error codes */
        err_compute_euclidean_distance,
        /* HOG_excp error codes */
        err_no_cropped_image_computed,
        /* io_web_cam_excp error codes */
        err_isopen_webcam,
        /* Default */
        err_default,
        err_code_not_initialized
    };

    //----------------------------------------------------------------------------------

    /**
     * Custom exception class for the DNN_ResNet class.
     * 
     * The derived class allows to throw exceptions with a message and error code.
     * The possible error codes can be found in the enum error_code which is in the namespace fve.
     * The custom exception class has the benefit that a catch block can determine what to do depending on the error code.
     */
    class DNN_ResNet_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        /**
        * Throw an exception with a textual string and nce::error_code.
        * 
        * The function has for all parameters a default value thus only the parameter that is useful should be used. 
        *  
        * @param msg IN Message explaining the error. 
        * @param ec IN Error code beloning to the type of error that occurs.
        */
        DNN_ResNet_excp(const std::string msg = "Error message not initialized", const fve::error_code ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        DNN_ResNet_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~DNN_ResNet_excp() {}

        /**
        * Get the message explaning the error.
        * @return error message
        */
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        /**
        * Get the error code belong to the error that occured. 
        * @return One of the error codes in nce::error_code enum. 
        */
        fve::error_code error_code() const
        {
            return _ec;
        }
    };

    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class euclidean_distance_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        euclidean_distance_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        euclidean_distance_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~euclidean_distance_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };

    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class face_embedder_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        face_embedder_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        face_embedder_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~face_embedder_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };

    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class face_verifier_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        face_verifier_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        face_verifier_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~face_verifier_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };
    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class HOG_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        HOG_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        HOG_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~HOG_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };
    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class io_web_cam_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        io_web_cam_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        io_web_cam_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~io_web_cam_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };

    //----------------------------------------------------------------------------------

    /**
     * See the explanation above the DNN_ResNet_excp class.
     */
    class io_image_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        fve::error_code _ec;

    public:
        io_image_excp(const std::string &msg = "Error message not initialized", const fve::error_code &ec = fve::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        io_image_excp(const fve::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~io_image_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        fve::error_code error_code() const
        {
            return _ec;
        }
    };
}

#endif //ERROR_HANDLING_FVE_HPP
