#include "../include/session.hpp"

session::session(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket,
                 queue<msg_struct> &msgInQueue,
                 std::function<void(errorCodes ec)> callback)
    : _socket(std::move(socket)), _msgInQueue(msgInQueue), _callback_func(callback)
{
    std::cout << "[SERVER session:constructor] " << _socket.lowest_layer().remote_endpoint() << std::endl;
}

session::~session()
{
    std::cout << "[SERVER session:deconstructor] " << _socket.lowest_layer().remote_endpoint() << std::endl;
}

bool session::is_socket_open()
{
    return _socket.lowest_layer().is_open();
}

// Disconnect function
void session::stop_socket()
{
    if (_socket.lowest_layer().is_open())
    {
        _socket.lowest_layer().close();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void session::start_reading()
{
    if (_socket.lowest_layer().is_open())
    {
        read_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void session::start_write()
{
    if (_socket.lowest_layer().is_open())
    {
        write_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void session::write_header()
{
    auto header = msgOutBuf.front().header_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(header, sizeof(header)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //std::cout << "[SERVER connection.hpp write_header()] async_write succesfully, wrote " << length << " bytes" << std::endl;
                                     write_id();
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_header()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void session::write_id()
{
    auto id = msgOutBuf.front().id_serialized;
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(id, sizeof(id)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     if (payload.size() > 0)
                                     {
                                         //std::cerr << "[SERVER connection.hpp write_id()] write id succesfully, wrote " << length << " bytes" << std::endl;
                                         write_payload();
                                     }
                                     else
                                     {
                                         msgOutBuf.pop_front();

                                         if (!msgOutBuf.empty())
                                         {
                                             //write_header();
                                             start_write();
                                         }
                                         std::cout << "[SERVER] write succesfull, Warning: message has no body" << std::endl;
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_id()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void session::write_payload()
{
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     std::cerr << "[SERVER] write succesful! Payload is " << length << " bytes." << std::endl;

                                     msgOutBuf.pop_front();

                                     if (!msgOutBuf.empty())
                                     {
                                         std::cerr << "[SERVER] Message out queue not empty, start new write." << std::endl;

                                         start_write();
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_payload()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void session::read_header()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.header),
                                                sizeof(tmp_msg.header)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[SERVER] Read a header, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         header = " << tmp_msg.header << std::endl;
                                    read_id();
                                }
                                else
                                {
                                    std::cout << "[SERVER connection.hpp read_header()] read_header failed: " << ec.message() << std::endl;
                                    tmp_msg.header = 0;
                                    _callback_func(read_ERR);
                                }
                            });
}

void session::read_id()
{

    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.id),
                                                sizeof(tmp_msg.id)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[SERVER] Read a id, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         id = " << tmp_msg.id << std::endl;

                                    //check if message has payload
                                    if (tmp_msg.header > 0)
                                    {
                                        try
                                        {
                                            tmp_msg.payload_serialized.resize(tmp_msg.header);
                                            read_payload();
                                        }
                                        catch (const std::length_error &le)
                                        {
                                            std::cout << "[SERVER connection.hpp read_id()] received header to big or corrupted, therefore <<tmp_msg.payload_serialized.resize(tmp_msg.header)>> failed, deny message" << std::endl;
                                            std::cerr << "                                       Length error: " << le.what() << std::endl;
                                            tmp_msg.header = 0;
                                            tmp_msg.payload_serialized.erase();
                                            _callback_func(read_ERR);
                                        }
                                    }
                                    else
                                    {
                                        // no payload, just finished and go on with next task
                                        std::cout << "[SERVER] Warning, received message without payload " << std::endl;
                                        start_reading();
                                    }
                                }
                                else
                                {
                                    std::cout << "[SERVER connection.hpp read_id()] read_id failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

void session::read_payload()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(tmp_msg.payload_serialized.data(),
                                                tmp_msg.header),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[SERVER] Read the payload, read " << lenght << " bytes" << std::endl;

                                    //tmp_msg complete, add to message IN queue
                                    _msgInQueue.push_back(tmp_msg);

                                    //@todo implemetn reset function in struct, for now reset member variables here, ready for new read
                                    tmp_msg.header = 0;
                                    tmp_msg.id = messageTypes::defaultMsg;
                                    tmp_msg.payload_serialized.clear();
                                    tmp_msg.header_serialized.clear();
                                    tmp_msg.id_serialized.clear();

                                    //the server should always listen for messages, thus give the io_context the task to listing for a new header
                                    //read_header(); same as start but with is_connected check
                                    start_reading();
                                }
                                else
                                {
                                    std::cout << "[SERVER connection.hpp read_payload()] read_payload failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

void session::start_handshake()
{
    _socket.async_handshake(boost::asio::ssl::stream_base::server,
                            [this](const boost::system::error_code &error) {
                                if (!error)
                                {
                                    start_reading();
                                }
                                else
                                {
                                    std::cout << "[SERVER} handshake failed: " << error.message() << std::endl;
                                    _callback_func(handshake_ERR);
                                }
                            });
}
