/** @file s_message.hpp
* Wrapper class around protobuf generated class.
* 
* @author <name>
* @date   22-03-21
*
* @note To use a generated protobuf class as base class(inheritance), delete the 'final'  keyword in the protobuf classes.
* @todo 
*/

#ifndef S_MESSAGE_HPP
#define S_MESSAGE_HPP

#include <iostream>
#include "message.pb.h"
#include "common.hpp"

class s_message : public divfr::data
{
public:
    s_message(messageTypes id = messageTypes::protoMsg1);
    ~s_message();

    //total size of data in the msg_struct
    size_t size_header_id_payload();
    // size of the header variable in bytes
    size_t sizeof_header();
    // size of the id variable in bytes
    size_t sizeof_id();
    // size of the payload variable in bytes
    size_t size_payload_serialized();

    // input: a s_message struct
    // reconstruct protobuf s_message
    // return true on sucesfull, fail on error
    bool parseprotoMsg1Struct(msg_struct data);

    // place protobuf data in the msg_stuct object
    void setprotoMsg1Struct();
    // get msg_struct object, always perform a set msg_struct before a getprotoMsg1Struct()
    msg_struct getprotoMsg1Struct();

    // get serialized id. for transmission over a socket stram
    // get serialized header. for transmission over a socket stram
    // get serialized payload. for transmission over a socket stram
    std::string get_serialized_id();
    std::string get_serialized_header();
    std::string get_serialized_payload();

private:
    msg_struct protoMsg1Struct;
};



#endif //S_MESSAGE_HPP