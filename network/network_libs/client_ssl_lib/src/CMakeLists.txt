# set library name!
set(TARGET_NAME _network_client)

#see below!! 
#find_package(Boost 1.71.0 REQUIRED)
find_package(Protobuf REQUIRED)
find_package(OpenSSL REQUIRED)



set(HEADERS

../include/queue.hpp
../include/client_interface.hpp
../include/c_message.hpp
../include/connection.hpp
../include/message.pb.h
)

set(SOURCES
    client_interface.cpp
    c_message.cpp
    connection.cpp
    message.pb.cc
)

set(LINK_LIBRARIES
    pthread
    ${Protobuf_LIBRARIES}
    ${Boost_LIBRARIES}
    ${OPENSSL_LIBRARIES}
)

set(COMPILE_OPTIONS)

add_library(${TARGET_NAME} ${SOURCES} ${HEADERS})
target_link_libraries(${TARGET_NAME} ${LINK_LIBRARIES})

# Must use the newest boost version! otherwise SSL stream sockets/ acceptor function does not work, volgorde is belangrijk, kan dit niet naar boven verplaatsen
target_include_directories(${TARGET_NAME} PUBLIC /usr/local/boost_1_75_0)

target_include_directories(${TARGET_NAME} PUBLIC .)
target_compile_options(${TARGET_NAME} INTERFACE ${COMPILE_OPTIONS})

# set output folder CMAKE_SOURCE_DIR is the path where the root CMakeLists.txt is.
set_target_properties(${TARGET_NAME}
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
    #RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
)

# g++ -I include -I /usr/local/boost_1_75_0 -o main main.cpp ../src/message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread -g -lcrypto -lssl

#  sudo apt-get install libboost-all-dev



