#include "../include/connection.hpp"
#include <boost/asio/ssl.hpp> //boost::asio::ssl


using std::placeholders::_1;
using std::placeholders::_2;

connection::connection(boost::asio::io_context &io_context,
                       boost::asio::ssl::context &context,
                       queue<msg_struct> &msgInQueue,
                       std::function<void(errorCodes ec)> callback)
    : _socket(io_context, context), _msgInQueue(msgInQueue), _callback_func(callback)
{
    _socket.set_verify_mode(boost::asio::ssl::verify_peer);
    _socket.set_verify_callback(std::bind(&connection::verify_certificate, this, _1, _2));
    std::cout << "[CLIENT session:constructor] " << std::endl;
}

connection::~connection()
{
    std::cout << "[CLIENT session:deconstructor] " << std::endl;
}

//It is cleanest to make shutdown() calls on both the ssl::stream and its lowest_layer().
//The first ends the SSL connection and the second ends the TCP connection.
void connection::disconnect()
{
    if (_socket.lowest_layer().is_open())
    {   
        _socket.lowest_layer().close();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

bool connection::is_socket_open()
{
    return _socket.lowest_layer().is_open();
}

void connection::connect_to_server(const boost::asio::ip::tcp::resolver::results_type _endp)
{
    // async_connect opens the socket
    _endp_backup = _endp;

    boost::asio::async_connect(_socket.lowest_layer(), _endp,
                               [this](const boost::system::error_code &error,
                                      const boost::asio::ip::tcp::endpoint &endpoint) {
                                   if (!error)
                                   {
                                       std::cout << "[CLIENT] async_conntect succesfully " << endpoint.address() << ":" << endpoint.port() << std::endl;
                                       handshake();
                                   }
                                   else
                                   {
                                       std::cout << "[CLIENT] async_connect failed: " << error.message() << "\n";
                                       _callback_func(connection_ERR);
                                   }
                               });
}

void connection::handshake()
{
    _socket.async_handshake(boost::asio::ssl::stream_base::client,
                            [this](const boost::system::error_code &error) {
                                if (!error)
                                {
                                    std::cout << "[CLIENT] handshake succesfully" << std::endl;

                                    // Give context work to do, otherwise it closes immedialty
                                    if (_socket.lowest_layer().is_open())
                                    {
                                        read_header();
                                    }
                                    else
                                    {
                                        _callback_func(connection_ERR);
                                    }
                                }
                                else
                                {
                                    std::cout << "Handshake failed: " << error.message() << "\n";
                                    _callback_func(connection_ERR);
                                }
                            });
}

void connection::start_reading()
{
    if (_socket.lowest_layer().is_open())
    {
        read_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void connection::start_write()
{
    if (_socket.lowest_layer().is_open())
    {
        write_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void connection::write_header()
{
    auto header = msgOutBuf.front().header_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(header, sizeof(header)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //std::cout << "[CLIENT connection.hpp write_header()] async_write succesfully, wrote " << length << " bytes" << std::endl;
                                     write_id();
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_header()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::write_id()
{
    auto id = msgOutBuf.front().id_serialized;
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(id, sizeof(id)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 // asio has now sent the bytes - if there was a problem
                                 // an error would be available...
                                 if (!ec)
                                 {
                                     if (payload.size() > 0)
                                     {
                                         //std::cerr << "[CLIENT connection.hpp write_id()] write id succesfully, wrote " << length << " bytes" << std::endl;
                                         write_payload();
                                     }
                                     else
                                     {
                                         msgOutBuf.pop_front();

                                         if (!msgOutBuf.empty())
                                         {
                                             start_write();
                                         }
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_id()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::write_payload()
{
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     std::cerr << "[CLIENT] write succesful! Payload is " << length << " bytes." << std::endl;

                                     msgOutBuf.pop_front();

                                     if (!msgOutBuf.empty())
                                     {
                                         std::cerr << "[CLIENT] Message out queue not empty, start new write." << std::endl;

                                         start_write();
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_payload()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::read_header()
{
    //read incoming data until we have enough bytes to construct the header, and store the body/payload size in the header
    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.header),
                                                sizeof(tmp_msg.header)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read a header, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         header = " << tmp_msg.header << std::endl;
                                    read_id();
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_header()] read_header failed: " << ec.message() << std::endl;
                                    tmp_msg.header = 0;
                                    _callback_func(read_ERR);
                                }
                            });
}

void connection::read_id()
{

    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.id),
                                                sizeof(tmp_msg.id)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read a id, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         id = " << tmp_msg.id << std::endl;

                                    //check if message has payload
                                    if (tmp_msg.header > 0)
                                    {
                                        try
                                        {
                                            tmp_msg.payload_serialized.resize(tmp_msg.header);
                                            read_payload();
                                        }
                                        catch (const std::length_error &le)
                                        {
                                            std::cout << "[CLIENT connection.hpp read_id()] received header to big or corrupted, therefore <<tmp_msg.payload_serialized.resize(tmp_msg.header)>> failed, deny message" << std::endl;
                                            std::cerr << "                                       Length error: " << le.what() << std::endl;
                                            tmp_msg.header = 0;
                                            tmp_msg.payload_serialized.erase();
                                            _callback_func(read_ERR);
                                        }
                                    }
                                    else
                                    {
                                        std::cout << "[Client] Warning, received message without payload " << std::endl;
                                        start_reading();
                                    }
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_id()] read_id failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

void connection::read_payload()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(tmp_msg.payload_serialized.data(),
                                                tmp_msg.header),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read the payload, read " << lenght << " bytes" << std::endl;

                                    //tmp_msg complete, add to message IN queue
                                    _msgInQueue.push_back(tmp_msg);

                                    //@todo implemetn reset function in struct, for now reset member variables here, ready for new read
                                    tmp_msg.header = 0;
                                    tmp_msg.id = messageTypes::defaultMsg;
                                    tmp_msg.payload_serialized.clear();
                                    tmp_msg.header_serialized.clear();
                                    tmp_msg.id_serialized.clear();

                                    //the server should always listen for messages, thus give the io_context the task to listing for a new header
                                    start_reading();
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_payload()] read_payload failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

bool connection::verify_certificate(bool preverified,
                                          boost::asio::ssl::verify_context &ctx)
{
    // The verify callback can be used to check whether the certificate that is
    // being presented is valid for the peer. For example, RFC 2818 describes
    // the steps involved in doing this for HTTPS. Consult the OpenSSL
    // documentation for more details. Note that the callback is called once
    // for each certificate in the certificate chain, starting from the root
    // certificate authority.

    // In this example we will simply print the certificate's subject name.
    char subject_name[256];
    // retrieve information from the received certificate
    // x509 is a standard.
    X509 *cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());
    X509_NAME_oneline(X509_get_subject_name(cert), subject_name, 256);
    std::cout << "Verifying " << subject_name << "\n";

    return preverified;
}