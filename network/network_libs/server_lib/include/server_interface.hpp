/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo create function that can open a socket again and connecxt to server, in progress. // void client_interface::reopen_socket_and_connect_to_server(const std::string _ip, const short _port)
*/

#ifndef SERVER_INTERFACE_HPP
#define SERVER_INTERFACE_HPP

#include <iostream>
#include <boost/asio.hpp>
#include "session.hpp"
#include "queue.hpp"
#include "s_message.hpp"
#include "common.hpp"

class server_interface
{
public:
    server_interface(short port);
    ~server_interface();

    void stop_server();
    bool start_server();

    bool client_is_connected();
    void disconnect_client();

    void send(msg_struct msg);
    void update(size_t maxMessagesCount = 100);

protected:
    // callback function, called on a new client connection/session
    virtual bool onClientConnect();

    // callback function, called on new received message
    virtual void onMessage(msg_struct msg);

    // callback function, called on ERROR
    virtual void onErrorFromSession(errorCodes ec);

private:
    void do_accept();

    boost::asio::io_context _context;
    boost::asio::ip::tcp::acceptor _acceptor;
    std::thread _threadContext;
    queue<msg_struct> _msgInQueue;
    std::shared_ptr<session> _connectionSession;
};

#endif //SERVER_INTERFACE_HPP