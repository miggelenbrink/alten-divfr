/** @file session.hpp
* The session object handles the socket and read/write buffers of a connected socket.
*
* @author <name>
* @date   22-03-21
*
* @todo
*/

#ifndef SESSION_HPP
#define SESSION_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <functional>       //callback functions

#include "queue.hpp"
#include "common.hpp"
#include "s_message.hpp"

class session
{
public:
    session(boost::asio::ip::tcp::socket socket, queue<msg_struct> &msgInQueue, std::function<void(errorCodes ec)> callback);
    ~session();

    bool is_socket_open();
    void stop_socket();
    void start_reading();
    void start_write();

private:
    void write_header();
    void write_id();
    void write_payload();

    void read_header();;
    void read_id();;
    void read_payload();;
 
    boost::asio::ip::tcp::socket _socket;
    msg_struct tmp_msg;
    std::function<void(errorCodes ec)> _callback_func;
    queue<msg_struct> &_msgInQueue;

public:
    queue<msg_struct> msgOutBuf;
};

#endif //SESSION_HPP