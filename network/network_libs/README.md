# The client_ssl and server_ssl libary use keypairs and certificates

Specify the locations of the keypairs and certificates in the common.hpp file.


# Create Certificate authority (CA), selfsigned certificate and keypairs

Generate a keypair for the CA.  
```
openssl genrsa -out ca.key 2048
```
  
Generate selfsigned certificate: sign the public key by the private key.  
```
openssl req -new -x509 -key ca.key -out ca.crt  
```
  
Generate a keypair for the server and a sign request file.  
```
openssl genrsa -out server.key 2048
openssl req -new -key server.key -out server_sign_request.csr  
```
  
Sign the signrequest by the CA.  
```
openssl -x509 -req -in server_sign_request.csr -CA ca.crt -CAKey ca.key -CAcreateserial -out server.crt
```
  
Next distribute the root CA to the clients. The root certificate is the CA's public key signed by its own private key.    

pass the server.crt and server.key to server application.
and pass ca.crt to the clients.   
