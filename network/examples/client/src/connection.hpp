/** @file connection.hpp
* The connection object handles the socket and read/write buffers of a connected socket.
*
* @author <name>
* @date   22-03-21
*
* @todo onnodige includes verwijderen
*/

#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <string>     // verwijderen?
#include <functional> //callback functions
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp> //boost::asio::ssl
#include <thread>
#include "queue.hpp"
#include "c_message.hpp"
#include "common.hpp"

/* 
* The connection class.
* The connection object handles the socket 
* and read/write buffers of a connected client.
*/
class connection
{
public:
    /**
    * Constructor. 
    * Receives a tcp::ip::socket and moves it into an owned socket. 
    */
    connection(boost::asio::ip::tcp::socket socket, queue<msg_struct> &msgInQueue, std::function<void(errorCodes ec)> callback);
    /**
    * Deconstructor. 
    * Destroy socket and clean up all read/write buffers.
    */
    ~connection();

    /**
    * Connect asynchronous to a boost::asio::endpoint. 
    * The functions instructs the socket to async connect to the endpoint. 
    * @param _endp endpoint of type boost::asio::endpoint.  
    */
    void connect_to_server(boost::asio::ip::tcp::endpoint _endp);

    bool is_socket_open();

    void reconnect();
    /**
    * Disconnect from server.
    * Disconnect is called by the main application.
    */
    void disconnect();

    void start_reading();
    void start_write();

    /**
    * Disconnect from server.
    * Disconnect is called whenever an async function fails, triggers callback disconnectOnError.
    */
    void disconnectOnError();
    /**
    * Write header on socket-stream.
    * Looks up the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized header variable async on the socket-stream.
    */
    void write_header();
    /**
    * Write id on socket-stream.
    * Looks up the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized id variable async on the socket-stream.
    */
    void write_id();
    /**
    * Write payload on socket-stream.
    * Looks up and pops(deletes) the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized header variable async on the socket-stream.
    */
    void write_payload();
    /**
    * Reads a header from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of the socket_stream. The 2 bytes are the header and stored into a temporary msg_struct, which is used to construct a message while receiving the header, id and payload.
    * As soon the msg_struct is complete the msg_struct is pushed into a receive queue. The client application can acces message via the receive queue. 
    * When a valid header is received it calls the read_id function.
    */
    void read_header();
    /**
    * Reads a id from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of the socket_stream. The 2 bytes are the id and stored into a temporary msg_struct, which is used to construct a message while receiving the header, id and payload.
    * When a valid id is received it calls the read_payload function.
    */
    void read_id();
    /**
    * Reads the payload from the socket-stream.
    * This function reads number of bytes specefied in the header of the socket_stream. The payload is also temporary stored into the msg_struct.
    * When a the payload is received the msg_struct is pushed onto the message receive queue and the temporary msg_struct is reset. 
    */
    void read_payload();

    // msgOutbuf must be public because the client_interface push_back(s) messages(msg_struct) into the queue.
    queue<msg_struct> msgOutBuf;

private:
    // tmp message to construct message from receiving bytes
    msg_struct tmp_msg;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::ip::tcp::endpoint _endp_backup;
    queue<msg_struct> &_msgInQueue;
    std::function<void(errorCodes ec)> _callback_func;
};

connection::connection(boost::asio::ip::tcp::socket socket, queue<msg_struct> &msgInQueue, std::function<void(errorCodes ec)> callback)
    : _socket(std::move(socket)), _msgInQueue(msgInQueue), _callback_func(callback)
{
    std::cout << "[CLIENT session:constructor] " << std::endl;
}

connection::~connection()
{
    std::cout << "[CLIENT session:deconstructor] " << std::endl;
}

void connection::disconnect()
{
    if (_socket.is_open())
    {
        _socket.close();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

bool connection::is_socket_open()
{
    return _socket.is_open();
}

void connection::connect_to_server(boost::asio::ip::tcp::endpoint _endp)
{
    // async_connect opens the socket
    _endp_backup = _endp;
    _socket.async_connect(_endp, [this](std::error_code ec) { //pass the this pointer otherwise the lambda cannot acces the connection's member funcs/vars.
        if (!ec)
        {
            std::cout << "[CLIENT connection.hpp connect_to_server()] async_conntect succesfully " << _socket.remote_endpoint() << std::endl;

            // Give context work to do, otherwise it closes immedialty
            if (_socket.is_open())
            {
                read_header();
            }
            else
            {
                _callback_func(connection_ERR);
            }
        }
        else
        {
            std::cout << "[CLIENT connection.hpp connect_to_server()] async_connect failed: " << ec.message() << std::endl;
            _callback_func(connection_ERR);
        }

    });
}

void connection::start_reading()
{
    if (_socket.is_open())
    {
        read_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void connection::start_write()
{
    if (_socket.is_open())
    {
        write_header();
    }
    else
    {
        _callback_func(socket_is_open_ERR);
    }
}

void connection::write_header()
{
    auto header = msgOutBuf.front().header_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(header, sizeof(header)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //std::cout << "[CLIENT connection.hpp write_header()] async_write succesfully, wrote " << length << " bytes" << std::endl;
                                     write_id();
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_header()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::write_id()
{
    auto id = msgOutBuf.front().id_serialized;
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(id, sizeof(id)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 // asio has now sent the bytes - if there was a problem
                                 // an error would be available...
                                 if (!ec)
                                 {
                                     if (payload.size() > 0)
                                     {
                                         //std::cerr << "[CLIENT connection.hpp write_id()] write id succesfully, wrote " << length << " bytes" << std::endl;
                                         write_payload();
                                     }
                                     else
                                     {
                                         msgOutBuf.pop_front();

                                         if (!msgOutBuf.empty())
                                         {
                                             start_write();
                                         }
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_id()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::write_payload()
{
    auto payload = msgOutBuf.front().payload_serialized;

    boost::asio::async_write(_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     std::cerr << "[CLIENT] write succesful! Payload is " << length << " bytes." << std::endl;

                                     msgOutBuf.pop_front();

                                     if (!msgOutBuf.empty())
                                     {
                                         std::cerr << "[CLIENT] Message out queue not empty, start new write." << std::endl;

                                         start_write();
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[CLIENT connection.hpp write_payload()] async_write failed  " << ec.message() << std::endl;
                                     _callback_func(write_ERR);
                                 }
                             });
}

void connection::read_header()
{
    //read incoming data until we have enough bytes to construct the header, and store the body/payload size in the header
    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.header),
                                                sizeof(tmp_msg.header)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read a header, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         header = " << tmp_msg.header << std::endl;
                                    read_id();
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_header()] read_header failed: " << ec.message() << std::endl;
                                    tmp_msg.header = 0;
                                    _callback_func(read_ERR);
                                }
                            });
}

void connection::read_id()
{

    boost::asio::async_read(_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&tmp_msg.id),
                                                sizeof(tmp_msg.id)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read a id, " << lenght << " bytes read." << std::endl;
                                    std::cout << "         id = " << tmp_msg.id << std::endl;

                                    //check if message has payload
                                    if (tmp_msg.header > 0)
                                    {
                                        try
                                        {
                                            tmp_msg.payload_serialized.resize(tmp_msg.header);
                                            read_payload();
                                        }
                                        catch (const std::length_error &le)
                                        {
                                            std::cout << "[CLIENT connection.hpp read_id()] received header to big or corrupted, therefore <<tmp_msg.payload_serialized.resize(tmp_msg.header)>> failed, deny message" << std::endl;
                                            std::cerr << "                                       Length error: " << le.what() << std::endl;
                                            tmp_msg.header = 0;
                                            tmp_msg.payload_serialized.erase();
                                            _callback_func(read_ERR);
                                        }
                                    }
                                    else
                                    {
                                        std::cout << "[Client] Warning, received message without payload " << std::endl;
                                        start_reading();
                                    }
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_id()] read_id failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

void connection::read_payload()
{
    boost::asio::async_read(_socket,
                            boost::asio::buffer(tmp_msg.payload_serialized.data(),
                                                tmp_msg.header),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    std::cout << "[CLIENT] Read the payload, read " << lenght << " bytes" << std::endl;

                                    //tmp_msg complete, add to message IN queue
                                    _msgInQueue.push_back(tmp_msg);

                                    //@todo implemetn reset function in struct, for now reset member variables here, ready for new read
                                    tmp_msg.header = 0;
                                    tmp_msg.id = messageTypes::defaultMsg;
                                    tmp_msg.payload_serialized.clear();
                                    tmp_msg.header_serialized.clear();
                                    tmp_msg.id_serialized.clear();

                                    //the server should always listen for messages, thus give the io_context the task to listing for a new header
                                    start_reading();
                                }
                                else
                                {
                                    std::cout << "[CLIENT connection.hpp read_payload()] read_payload failed: " << ec.message() << std::endl;
                                    _callback_func(read_ERR);
                                }
                            });
}

#endif //CONNECTION_HPP