/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo create function that can open a socket again and connecxt to server, in progress. // void client_interface::reopen_socket_and_connect_to_server(const std::string _ip, const short _port)
*/

#ifndef CLIENT_INT_HPP
#define CLIENT_INT_HPP

#include <iostream>
#include "common.hpp"
#include "c_message.hpp"
#include "queue.hpp"
#include "connection.hpp"

/* 
* The client_interface class.
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*/
class client_interface
{
public:
    /**
    * Constructor. 
    * Creates and initializes an io_contet, tcp::ip::socket and
    * a thread that runs the io_context to handle async boost::asio functions.
    */
    client_interface();
    /**
    * Deconstructor. 
    * Stops the running io_context, joins the io_context thread and destory the client_interface.
    */
    ~client_interface();

    /**
    * Disconnect from server. 
    * Stops the running io_context, joins the io_context thread
    * and destory the connection object.
    */
    void disconnect();

    /**
    * Connect to a server. 
    * @param _ip The server's IP address. 
    * @param _port The port # the server listens on for new connections.
    * @return true on succesfully connected, false on failed to connect.
    */
    bool connect(const std::string _ip, const short _port);

    /**
    * A
    */
    bool is_connected();

    /**
    * send msg_struct message to the server.
    * The send function places the message into a msgOut queue and the connection object writes messages in the msgOut queue on the socket stream.
    * @param msg A msg_struct instance.
    */
    void send(msg_struct msg);

    /**
    * A
    */
    queue<msg_struct> &get_msgInQueu();

protected:
    /**
    * A
    */
    virtual void onErrorFromSession(errorCodes ec)
    {
        std::cout << "[CLIENT] Default onErrorFromSession()" << std::endl;
        switch (ec)
        {
        case errorCodes::connection_ERR:
            std::cout << "[CLIENT] Errorcode: connection_ERR, do a reconnect (not active in release) " << std::endl;
            //must do a disconnect to clean up the failed connect try
            disconnect();

            ///@todo add a deadline, that after 3 attempts this function quits! and control goes back to the console
            // otherwise reconnect will spam
            // using namespace std::chrono_literals;
            // std::this_thread::sleep_for(4000ms);
            // connect(_ip_, _port_);

            break;
        case errorCodes::socket_is_open_ERR:
            std::cout << "[CLIENT] Errorcode: socket_is_open_ERR, probably/assume the server is disconnected" << std::endl;
            disconnect();

            break;
        case errorCodes::read_ERR:
            std::cout << "[CLIENT] Errorcode: read_ERR, probably/assume the server is disconnected" << std::endl;
            disconnect();

            break;
        case errorCodes::write_ERR:
            std::cout << "[CLIENT] Errorcode: write_ERR" << std::endl;

            // don't try to acces the pointer if it points to the unkown.
            if (_connectionSession)
            {
                //if you don't do a disconnect_client(), then manualy delete the message that caused the error from the msgOutBuf! because alsong the msg is in the buffer it will probably gerenate te same error on a next write.

                if (!_connectionSession->msgOutBuf.empty())
                    _connectionSession->msgOutBuf.pop_front();

                if (!_connectionSession->msgOutBuf.empty())
                    _connectionSession->start_write();
            }

            break;
        default:
            std::cout << "[CLIENT] Errorcode: error code does not exists..." << std::endl;

            break;
        }
    }

private:
    // Buffer for messages read by the connection object which are ready to be processed by the client application.
    queue<msg_struct> _msgInQueue;
    boost::asio::io_context _context;
    boost::asio::ip::tcp::socket _socket;
    std::thread _threadContext;
    std::unique_ptr<connection> _connectionSession;
    short _port_;
    std::string _ip_;
};

client_interface::client_interface() : _socket(_context)
{
}

client_interface::~client_interface()
{
    if (_threadContext.joinable())
        _threadContext.join();

    disconnect();

    std::cout << "[CLIENT] Client closed, deconstructor client_interface" << std::endl;
}

void client_interface::disconnect()
{
    // destroy connection/session object

    if (_connectionSession)
    {
        _connectionSession->disconnect();
        _connectionSession.reset();
    }

    if (_msgInQueue.empty())
        _msgInQueue.clear();

    if (_socket.is_open())
        _socket.close();

    if (!_context.stopped())
        _context.stop();

    std::cerr << "[CLIENT] disconnected from server" << std::endl;
}

bool client_interface::connect(const std::string _ip, const short _port)
{
    //cache ip and port, for reconnect, if connect failes
    _ip_ = _ip;
    _port_ = _port;
    try
    {
        // Create a connection and socket object and pass the socket to the connectoin object
        if (!_connectionSession)
        {
            _connectionSession = std::make_unique<connection>(std::move(_socket), _msgInQueue, [this](errorCodes erc) { this->onErrorFromSession(erc); });

            // Create boost::asio::endpoint
            auto _endpoint = boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(_ip), _port);

            // Make connection object and start reading
            _connectionSession->connect_to_server(_endpoint);

            if (_context.stopped())
                _context.restart();

            // Run the io_context in a thread, the io_context handles all function called on the _socket instance.
            if(_threadContext.joinable())
                _threadContext.join();
                
            _threadContext = std::thread([this]() { _context.run(); });
        }
        else
        {
            std::cerr << "[CLIENT client_interface.hpp] Do a disconnect before a new connect" << std::endl;
            return false;
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "[CLIENT client_interface.hpp] failed to connect to the server: " << e.what() << std::endl;
        onErrorFromSession(connection_ERR);

        return false;
    }
    return true;
}

bool client_interface::is_connected()
{
    if (_connectionSession)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void client_interface::send(msg_struct msg)
{
    if (_connectionSession)
    {
        if (_connectionSession->is_socket_open())
        {
            // async send
            _context.post([this, msg]() {
                //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
                bool WriteInprocess = _connectionSession->msgOutBuf.empty();

                // Write message into the outgoing message queue (buffer).
                _connectionSession->msgOutBuf.push_back(msg);

                // Tell the connection object to write all message in the message queue to the socket stream/server.
                if (WriteInprocess)
                    _connectionSession->start_write();
            });
        }
        else
        {
            std::cout << "[CLIENT] client_interface: send failed because socket to server is not open " << std::endl;
            onErrorFromSession(errorCodes::socket_is_open_ERR);
        }
    }
    else
    {
        std::cout << "[CLIENT] client_interface: send failed because there is no client connected " << std::endl;
    }
}

queue<msg_struct> &client_interface::get_msgInQueu()
{
    return _msgInQueue;
}

#endif //CLIENT_INT_HPP