/** @file queue.hpp
* A thread save deque (double ended queue).
* A Thread save queue, because the queue can be accesed by more than one object simultatoulsy.
* The connection and client object work both on the queue
*
* @author <name>
* @date   22-03-21
*
* @todo 
*/

#ifndef QUEUE_HPP
#define QUEUE_HPP

#include <iostream>
#include <mutex>
#include <deque>

/**
* A template queue. 
*    
* The queue class provides functionality to control the queue.
* It is a double ended queue which can be accesed on the front and end.
* Futher it is a template class.
*/
template <class T>
class queue
{
public:
    queue() = default;
    queue(const queue<T> &) = delete;
    ~queue();
    /**
    * Return a reference to the front message in the queue. 
    * 
    * @return return message
    * @note Function cannot be const because: because, the _mutex object will change it's internal state from say "unlocked" to "locked" state. and in a const function no changes can be made
    */
    T &front();
    T &back();
    bool empty();
    /**
    * Return the number of elements in the queue.
    * 
    * @return Number of elements in the queue. 
    */
    std::size_t size();
    void push_back(const T &value);
    T pop_back();
    /**
    * Pops the front message of the queue and returns it, subsequently it deletes the message from the queue.
    * 
    * @note Always check if the queue is not empty, otherwise a segmentation fault happens.
    * @return Return the message in the front of the queue.
    */
    T pop_front();
    void resize(const std::size_t size);
    void clear();

private:
    std::mutex mutexQueue;
    std::deque<T> _queue;
};

template <class T>
queue<T>::~queue()
{
    clear();
}

template <class T>
T &queue<T>::front()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.front();
}

template <class T>
T &queue<T>::back()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.back();
}

template <class T>
bool queue<T>::empty()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.empty();
}

template <class T>
std::size_t queue<T>::size()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.size();
}

template <class T>
void queue<T>::push_back(const T &value)
{
    std::scoped_lock lock(mutexQueue);
    _queue.push_back(value);
}

template <class T>
T queue<T>::pop_back()
{
    std::scoped_lock lock(mutexQueue);
    //back)() returns reference, the reference (the actual value. not the address) is copied into x
    auto x = _queue.back();
    _queue.pop_back();
    return x;
}

// if called and no data in queue then segmenattion fault if no data in the queue! take care of this
template <class T>
T queue<T>::pop_front()
{
    std::scoped_lock lock(mutexQueue);
    auto x = std::move(_queue.front());
    _queue.pop_front();
    return x;
}

template <class T>
void queue<T>::clear()
{
    std::scoped_lock lock(mutexQueue);
    _queue.clear();
}

#endif //QUEUE_HPP