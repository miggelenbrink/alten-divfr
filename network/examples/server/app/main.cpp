#include <iostream>
#include <signal.h> //signal handler ctrl + c

#include "../src/server_interface.hpp"
#include "../src/s_message.hpp"
#include <ostream>
#include <fstream>

#define OUT_PATH "/home/guido/Desktop/DIVFR/network/examples/server/app/apple_out.jpg"

class server : public server_interface
{

private:
    s_message msg1;

public:
    bool Exit_FLAG; // true, close the application

public:
    server(short port) : server_interface(port), Exit_FLAG(false)
    {
    }

    ~server()
    {
    }

    void set_a_msg()
    {
        msg1.set_age(22);
        msg1.set_img_fname("dankje");
        msg1.set_img_fformat("dankje");
        msg1.set_image_data("dankje");
    }

    void console()
    {
        char c_input;
        while (c_input != 'q')
        {
            std::cout << "[console] press 'q' to exit the application" << std::endl;
            std::cout << "[console] press 's' to send the test message" << std::endl;
            std::cout << "[console] press 'e' to send empty message" << std::endl;
            std::cout << "[console] press 'd' to disconnect the client" << std::endl;

            std::cin >> c_input;
            switch (c_input)
            {
            case 'q':
            {
                Exit_FLAG = true;
                break;
            }
            case 'd':
            {
                disconnect_client();
                break;
            }
            case 's':
            {
                set_a_msg();
                msg1.setprotoMsg1Struct();
                send(msg1.getprotoMsg1Struct());
                break;
            }
            case 'e':
            {
                s_message msg1;
                msg1.setprotoMsg1Struct();
                send(msg1.getprotoMsg1Struct());
                break;
            }
            default:
            {
                std::cout << "[console] key not recognised, use the keys 's' or 'q' " << std::endl;
                break;
            }
            }
        }
    }

protected:
    // handle only one connection at a time and only accept new connection after client disconnected, or the server disconnected the client.
    virtual bool onClientConnect()
    {
        //the server must do a disconnect() before it can accept new connections.
        if (client_is_connected())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    virtual void onMessage(msg_struct msg)
    {

        switch (msg.id)
        {
        case messageTypes::protoMsg1:
        {
            s_message msg2;
            msg2.parseprotoMsg1Struct(msg);

            std::cout << msg2.age() << std::endl;
            std::cout << msg2.img_fname() << std::endl;
            std::cout << msg2.img_fformat() << std::endl;
            std::cout << msg2.image_data() << std::endl;

            std::ofstream output(OUT_PATH, std::ios::binary);

            std::copy(
                msg2.image_data().begin(),
                msg2.image_data().end(),
                std::ostreambuf_iterator<char>(output));

            break;
        }
        case messageTypes::protoMsg2:
        {
            //bestaat nog niet
            break;
        }
        default:
        {
            std::cout << "[onMessage] received message with unknown id.\nDo nohting with it." << std::endl;

            break;
        }
        }
    }
};

int main(int argc, char *argv[])
{
    server server_test(6003);

    // create thread to processes the console/user interaction
    auto thread_console = std::thread([&]() {
        while (!server_test.Exit_FLAG)
        {
            server_test.console();
            std::cout << "[main] close console_thread" << std::endl;
        }
    });

    // get received message and call onMessage callback to handle the msg
    while (!server_test.Exit_FLAG)
    {
        server_test.update(5);
    }

    if (thread_console.joinable())
    {
        thread_console.join();
    }

    std::cout << "[main] exit application, bye..." << std::endl;
    return 0;
}