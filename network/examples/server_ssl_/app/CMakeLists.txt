set(SOURCEF
    main.cpp
)

add_executable(main ${SOURCEF})

# take the lib name specefied in src/CMakeLists.txt
target_link_libraries(main _network_server) 

set_target_properties(main
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
)