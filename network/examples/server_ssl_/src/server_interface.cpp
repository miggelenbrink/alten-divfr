#include "../include/server_interface.hpp"

server_interface::server_interface(short port)
    : _acceptor(_io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
      _ssl_context(boost::asio::ssl::context::sslv23)
{

    _ssl_context.set_options(
        boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 |
        // /Always create a new key when using tmp_dh parameters. = empahral key pair, always new keypair on new connection = forward secrecy
        boost::asio::ssl::context::single_dh_use);

    // load the certificates! we only use one certificate, it is not really a chain!
    boost::system::error_code ec;

    _ssl_context.use_certificate_chain_file(CERT_FILE, ec);
    if (ec)
    {
        std::cout << ec.message() << std::endl;
        std::cout << "check path to .cert file in common.hpp file CERT_FILE" << std::endl;
    }
    _ssl_context.use_private_key_file(KEY_FILE, boost::asio::ssl::context::pem, ec);
    if (ec)
    {
        std::cout << ec.message() << std::endl;
        std::cout << "check path to .cert file in common.hpp file CERT_FILE" << std::endl;
    }

    start_server();
}

server_interface::~server_interface()
{
    stop_server();
    std::cout << "[SERVER] server closed, deconstructor server_interface" << std::endl;
}

void server_interface::stop_server()
{
    if (!_io_context.stopped())
        _io_context.stop();

    if (_threadContext.joinable())
        _threadContext.join();

    if (!(_connectionSession.use_count() == 0))
    {
        _connectionSession->stop_socket();
        _connectionSession.reset();
        std::cout << "[SERVER] disconnected client" << std::endl;
    }
}

bool server_interface::start_server()
{
    try
    {
        // give the context work
        do_accept();
        std::cerr << "[SERVER] Waiting for incoming connection.." << std::endl;

        if (_io_context.stopped())
            _io_context.restart();

        // run context
        _threadContext = std::thread([this]() { _io_context.run(); });
    }
    catch (std::exception &e)
    {
        std::cerr << "[SERVER] Failed to start the server, server_interface, start_server(): " << e.what() << std::endl;
        return false;
    }

    return true;
}

void server_interface::disconnect_client()
{
    // delete left messages.
    _msgInQueue.clear();

    // if no client is connected, then do nothing
    if (!(_connectionSession.use_count() == 0))
    {
        _connectionSession.reset();
        std::cout << "[SERVER] Client disconnected" << std::endl;
        std::cerr << "[SERVER] Waiting for new incoming connection.." << std::endl;
    }
    else
    {
        std::cout << "[SERVER] No client to disconnect, before calling disconnect_client() wait for a client to connect" << std::endl;
    }
}

bool server_interface::client_is_connected()
{
    if (_connectionSession.use_count() == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void server_interface::update(size_t maxMessagesCount)
{
    size_t messageCount = 0;

    // pass the specified number of message by the user to onMessage function! and if there are no message do nothing
    while (messageCount < maxMessagesCount && !_msgInQueue.empty())
    {
        auto msg = _msgInQueue.pop_front();
        onMessage(msg);
        messageCount++;
    }

    ///@todo create return value that tells the user if all message have been read or that the messageQueue was empty
}

void server_interface::send(msg_struct msg)
{
    if (!(_connectionSession.use_count() == 0))
    {
        if (_connectionSession->is_socket_open())
        {
            _io_context.post([this, msg]() {
                //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
                bool WriteInprocess = _connectionSession->msgOutBuf.empty();

                // Write message into the outgoing message queue (buffer).
                _connectionSession->msgOutBuf.push_back(msg);

                // Tell the connection object to write all message in the message queue to the socket stream/server.
                if (WriteInprocess)
                    _connectionSession->start_write();
            });
        }
        else
        {
            std::cout << "[SERVER] server_interface: send failed because socket to client is not open, msg is NOT stored in msgOutbuf " << std::endl;
            onErrorFromSession(errorCodes::socket_is_open_ERR);
        }
    }
    else
    {
        std::cout << "[SERVER] server_interface: send failed because there is no client connected, msg is NOT stored in msgOutbuf " << std::endl;
    }
}

// callback function, called on a new client connection/session
bool server_interface::onClientConnect()
{
    //return true to accept the new client and delete the current one
    //return false to deny the client
    return true;
}

// callback function, called on new received message
void server_interface::onMessage(msg_struct msg)
{
    std::cout << "[SERVER] Default onMessage() function called, message is not handled" << std::endl;
}

// callback function, called on ERROR
void server_interface::onErrorFromSession(errorCodes ec)
{
    std::cout << "[SERVER] Default onErrorFromSession()" << std::endl;
    switch (ec)
    {
    case errorCodes::handshake_ERR:
        std::cout << "[SERVER] Errorcode: handshake failed!" << std::endl;
        disconnect_client();

        break;
    case errorCodes::socket_is_open_ERR:
        //std::cout << "[SERVER] Errorcode: socket_is_open_ERR, probably/assume the client is disconnected" << std::endl;
        disconnect_client();

        break;
    case errorCodes::read_ERR:
        //std::cout << "[SERVER] Errorcode: read_ERR, probably/assume the client is disconnected" << std::endl;
        disconnect_client();

        break;
    case errorCodes::write_ERR:
        //std::cout << "[SERVER] Errorcode: write_ERR" << std::endl;

        // don't try to acces the pointer if it points to the unkown.
        if (!(_connectionSession.use_count() == 0))
        {
            //if you don't do a disconnect_client(), then manualy delete the message that caused the error from the msgOutBuf! because alsong the msg is in the buffer it will probably gerenate te same error on a next write.

            if (!_connectionSession->msgOutBuf.empty())
                _connectionSession->msgOutBuf.pop_front();

            if (!_connectionSession->msgOutBuf.empty())
                _connectionSession->start_write();
        }

        break;
    default:
        std::cout << "[SERVER] Errorcode: error code does not exists..." << std::endl;

        break;
    }
}

void server_interface::do_accept()
{

    _acceptor.async_accept(
        [this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
            if (!ec)
            {
                std::cerr << "[SERVER] client " << socket.remote_endpoint() << " attempting to connect" << std::endl;

                std::shared_ptr<session> tmp_conn =
                    std::make_shared<session>(
                        boost::asio::ssl::stream<boost::asio::ip::tcp::socket>(
                            std::move(socket), _ssl_context),
                        _msgInQueue,
                        [this](errorCodes erc) {
                            this->onErrorFromSession(erc);
                        });

                if (onClientConnect())
                {
                    std::cerr << "[SERVER] client accepeted" << std::endl;
                    // start session/connection object/ connection object start reading received messages and pushing them into a queue
                    tmp_conn->start_handshake();
                    _connectionSession = tmp_conn;
                }
                else
                {
                    std::cerr << "[SERVER] client denied" << std::endl;
                }
            }
            do_accept();
        });
}