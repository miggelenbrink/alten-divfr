#include "../include/s_message.hpp"

s_message::s_message(messageTypes id) : protoMsg1Struct{0, id}
{
}

s_message::~s_message()
{
}

size_t s_message::size_header_id_payload()
{
    return ByteSizeLong() + sizeof(protoMsg1Struct.header) + sizeof(protoMsg1Struct.id);
}

size_t s_message::sizeof_id()
{
    return sizeof(protoMsg1Struct.id);
}

size_t s_message::sizeof_header()
{
    return sizeof(protoMsg1Struct.header);
}

size_t s_message::size_payload_serialized()
{
    return protoMsg1Struct.payload_serialized.size();
}

bool s_message::parseprotoMsg1Struct(msg_struct data)
{
    if (!ParseFromString(data.payload_serialized))
    {
        std::cout << "[SERVER] Parse payload failed - ParseFromString()" << std::endl;
        return false;
    }
    else
    {
        std::cout << "[SERVER] Payload succesfully parsed - ParseFromString() " << std::endl;
        return true;
    }
}

void s_message::setprotoMsg1Struct()
{
    //make const if everything works fine
    size_t sizeofHeader = sizeof_header();
    size_t sizeofId = sizeof_id();

    protoMsg1Struct.header = ByteSizeLong();
    // allocate space for serialized s_message
    protoMsg1Struct.payload_serialized.resize(protoMsg1Struct.header);

    protoMsg1Struct.header_serialized.resize(sizeofHeader);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.header_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.header)), sizeof(protoMsg1Struct.header));

    protoMsg1Struct.id_serialized.resize(sizeofId);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.id_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.id)), sizeof(protoMsg1Struct.id));

    if (!SerializeToString(&protoMsg1Struct.payload_serialized))
    {
        std::cout << "[CLIENT s_message.hpp setprotoMsg1Struct()] SerializeToString() failed " << std::endl;
    }
    else
    {
        std::cout << "[CLIENT s_message.hpp setprotoMsg1Struct()] SerializeToString() succesfully " << std::endl;
    }
}

msg_struct s_message::getprotoMsg1Struct()
{
    //optional, but easy to forget to do a setprotoMsg1Struct before calling getprotoMsg1Struct
    return protoMsg1Struct;
}

std::string s_message::get_serialized_id()
{
    return protoMsg1Struct.id_serialized;
}

std::string s_message::get_serialized_header()
{
    return protoMsg1Struct.header_serialized;
}

std::string s_message::get_serialized_payload()
{
    return protoMsg1Struct.payload_serialized;
}