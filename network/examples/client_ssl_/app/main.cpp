/** @file main.cpp
* Client application that uses the client_ssl_ library and the message.proto data structure.  
*
* The application prints a console and asks the user what to do:
*    press 'q' to exit the application" << std::endl;
*    press 'e' to send empty message" << std::endl;
*    press 's' to send the test message" << std::endl;
*    press 'c' to connect the server" << std::endl;
*    press 'd' to disconnect from the server" << std::endl;
*
* By issuing 'c' the client connects to a server (127.0.0.1:6000). The server's address is hardcoded in the main.cpp example application. 
* By issuing 's' the client assigns values to the custom message.proto data strucutre which is wrapped into the c_message class and sends the data to the server.
*
* @author Guido
* @date   22-03-21
*
* @todo use a C++ style guide, instead of only being consequent with variable and function naming.
*/

/*
* manual compile command: g++ -I include -I /usr/local/boost_1_75_0 -o main main.cpp src/message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread -g -lcrypto -lssl 
*/

#include <iostream>
#include <fstream> // open file, ifstream()
// client_ssl_lib include files
#include "../include/common.hpp"
#include "../include/queue.hpp"
#include "../include/connection.hpp"
#include "../include/client_interface.hpp"
// wrapper around the C++ generated protobuf class, generated of the custom message.proto data structure
#include "../include/c_message.hpp"

// paths to images
#define IN_PATH_JOHN "../input/John.jpg"
#define IN_PATH_HUGO "../input/Hugo.jpg"
#define IN_PATH_GUIO "../input/Guido.jpg"
#define IN_PATH_KOEN "../input/Koen.jpg"


/* 
* The client class is the application and inherits from the client_interface which is the client_ssl_lib's API. 
* Thus the client application can use the client_ssl_lib API.
* 
* The client_interface has callback functions, which can be set:
*   set_connErrCallback:      callback is triggerend when the connect() or reconnect() function fails to connect to a server.
*   set_writeErrCallback:     callback is triggerend when asyn_read() fails
*   set_readErrCallback:      callback is triggerend when async_write() fails
*   set_dissConnectCallback:  callback is triggerend when there is no socket is open anymore! thus the server is closed/offline.
* The default behavior can be changed in the client class. 
*/
class client : public client_interface
{
private:
    // custom protobuf message
    c_message msg_one_;

public:
    bool exit_flag_;

public:
    client() : exit_flag_(false) {}
    ~client() {}

    void connect_to_server(const std::string ip, const std::string port)
    {
        connect(ip, port);
    }

    void set_message(const std::string &path_to_image, const int age)
    {
        // open image as binary
        std::ifstream file(path_to_image, std::ios::binary);

        // store image in std::string as binary serialized data
        const std::string image_data_serialized(std::istreambuf_iterator<char>(file), {});

        // assign data to protobuf data structure
        msg_one_.set_age(age);
        msg_one_.set_img_fname("<optional_file_name>");
        msg_one_.set_img_fformat("optional_file_format>");
        msg_one_.set_image_data(image_data_serialized);
    }

    void console()
    {
        char c_input;
        while (c_input != 'q')
        {
            std::cout << "[console] press 'c' to connect the server" << std::endl;
            std::cout << "[console] press 'd' to disconnect from the server" << std::endl;
            std::cout << "[console] press 'g' to send a message containing an image of Guido and his age" << std::endl;
            std::cout << "[console] press 'j' to send a message containing an image of John and his age" << std::endl;
            std::cout << "[console] press 'h' to send a message containing an image of Hugo and his age" << std::endl;
            std::cout << "[console] press 'k' to send a message containing an image of Koen and his age" << std::endl;
            std::cout << "[console] press 'e' to send a message containing no data" << std::endl;
            std::cout << "[console] press 'q' to exit the application" << std::endl;
            std::cin >> c_input;

            switch (c_input)
            {
            case 'c':
            {
                connect_to_server("127.0.0.1", "6000");
                break;
            }
            case 'd':
            {
                disconnect();
                break;
            }
            case 'g':
            {
                set_message(IN_PATH_GUIO, 22);
                send(msg_one_.get_protoMsg1Struct());
                break;
            }
            case 'j':
            {
                set_message(IN_PATH_JOHN, 54);
                send(msg_one_.get_protoMsg1Struct());
                break;
            }
            case 'h':
            {
                set_message(IN_PATH_HUGO, 54);
                send(msg_one_.get_protoMsg1Struct());
                break;
            }
            case 'k':
            {
                set_message(IN_PATH_KOEN, 30);
                send(msg_one_.get_protoMsg1Struct());
                break;
            }
            case 'e':
            {
                c_message msg_one_;
                msg_one_.set_protoMsg1Struct();
                send(msg_one_.get_protoMsg1Struct());
                break;
            }
            case 'q':
            {
                exit_flag_ = true;
                std::cout << "[console] bye" << std::endl;
                break;
            }
            default:
            {
                std::cout << "[console] Input is not valid, chose between 'c', 'd', 'g', 'j', 'h', 'k', 'e' and 'q'." << std::endl;
                break;
            }
            }
        }
    }

    // Check if there are new messages avaible in the client_ssl library's messageInQueue (message received from the server are stored in the messageInQueue).
    void update()
    {
        while (!get_msgInQueu().empty())
        {
            auto msg = get_msgInQueu().pop_front();

            // if message avaible then print them to the screen.
            // check what the message id so the right data members can called.
            switch (msg.id)
            {
            case messageTypes::protoMsg1:
            {
                c_message msgIn;
                msgIn.parse_protoMsg1Struct(msg);

                std::cout << "age: " << msgIn.age() << std::endl;
                std::cout << "img_fname: " << msgIn.img_fname() << std::endl;
                std::cout << "img_format: " << msgIn.img_fformat() << std::endl;
                std::cout << "img_data: " << msgIn.image_data() << std::endl;

                break;
            }
            case messageTypes::protoMsg2:
            {
                std::cout << "Message with ID protoMsg2 is not defined!" << std::endl;
                break;
            }
            default:
            {
                break;
            }
            }
        }
    }
};

int main()
{
    client client_;

    std::cout << "[CLIENT]" << std::endl;

    // create thread to processes the console/user interaction
    auto thread_console = std::thread([&]() {
        while (!client_.exit_flag_)
        {
            client_.console();
        }
    });

    while (!client_.exit_flag_)
    {
        // get received messages and call onMessage callback to handle the msg
        client_.update();
    }

    if (thread_console.joinable())
    {
        thread_console.join();
    }

    std::cout << "[main] Exit application, bye..." << std::endl;
    return 0;
}
