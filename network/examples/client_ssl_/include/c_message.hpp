/** @file c_message.hpp
* Wrapper around protobuf's generated C++ class.
* 
* @author <name>
* @date   22-03-21
*
* @note To use a generated protobuf class as base class(inheritance), delete the 'final' keyword in the protobuf classes.
* @todo 
*/

#ifndef C_MESSAGE_HPP
#define C_MESSAGE_HPP

#include <iostream>
#include "message.pb.h"
#include "common.hpp"

class c_message : public divfr::data
{
public:
    /**
    * Construct c_message object and chose a message id, do no use an id that is already in use
    */
    c_message(messageTypes id = messageTypes::protoMsg1);
    /**
    * Deconstruct c_message object
    */
    ~c_message();

    /**
    * Return sizeof(header). 
    * @return The number of bytes of the header's type.
    */
    size_t sizeof_header();
    /**
    * Return sizeof(id). 
    * @return The number of bytes of the id's type.
    */
    size_t sizeof_id_type();
    /** 
    * Reconstruct protobuf object. 
    *
    * @param data msg_struct @see common.hpp
    * @return true on succes, else failure.
    */
    bool parse_protoMsg1Struct(msg_struct data);
    /**
    * Set the msg_struct @see common.hpp protoMsg1Struct.
    * The c_message member data variables are serialized and stored in the message_struct.
    */
    void set_protoMsg1Struct();
    /**
    * Get msg_struct instance.
    * 
    * @note always perform a set msg_struct before a get_protoMsg1Struct(): optional, but easy to forget to do a set_protoMsg1Struct() before calling get_protoMsg1Struct() otherwise the data is not updated into the struct
    */
    msg_struct get_protoMsg1Struct();
    /**
    * Get serialized id. 
    * For transmission over a socket stream. because socket streams work with serialized binary data.
    * @return return the serialized data as binary, as varialbe with the type std::string
    */
    std::string get_serialized_id();
    /**
    * Get serialized header. 
    * For transmission over a socket stream. because socket streams work with serialized binary data.
    *     
    * @return return the serialized data as binary, as varialbe with the type std::string
    */
    std::string get_serialized_header();
    /**
    * Get serialized payload. 
    * For transmission over a socket stream. because socket streams work with serialized binary data.
    * @return return the serialized data as binary, as varialbe with the type std::string
    */
    std::string get_serialized_payload();

private:
    msg_struct protoMsg1Struct;
};

#endif //C_MESSAGE_HPP