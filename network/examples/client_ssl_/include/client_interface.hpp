/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* library's functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo 
*/

#ifndef CLIENT_INT_HPP
#define CLIENT_INT_HPP

#include <iostream>
#include "common.hpp"
#include "c_message.hpp"
#include "queue.hpp"
#include "connection.hpp"
#include <cstdlib>
#include <cstring>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

/* 
* The client_interface class.
* The client interface provides a user with the 
* library's functionality.
*/
class client_interface
{
public:
    /**
    * Constructor. 
    * Creates and initializes an io_context, tcp::ip::socket and
    * a thread that runs the io_context to handle async boost::asio functions.
    */
    client_interface();
    /**
    * Deconstructor. 
    * Stops the running io_context, joins the io_context thread and destory the client_interface.
    */
    ~client_interface();

    /**
    * Disconnect from server. 
    * Stops the running io_context, joins the io_context thread
    * and destory the connection object.
    */
    void disconnect();

    /**
    * Connect to a server. 
    * @param _ip The server's IP address. 
    * @param _port The port # the server listens on for new connections.
    * @return true on succesfully connected, false on failed to connect.
    */
    bool connect(const std::string _ip, const std::string _port);

    /**
    * Check if the socket is connected to another socket.
    * @return true if socket is still connected, else false.
    */
    bool is_connected();

    /**
    * send msg_struct to the server. The msg_struct is the custom protobuf message but then serialized.
    * The send function places the message into a msgOut queue and the connection object writes messages in the msgOut queue on the socket stream.
    * @param msg A msg_struct instance.
    */
    void send(msg_struct msg);

    /**
    * msgInQueue is the queue where received data in the form of a message is stored.
    * @return return a pointer to the msgInQueue.
    * @todo correct spelling mistake Queu.
    */
    queue<msg_struct> &get_msgInQueu();



protected:
    /**
    * Abstract callback function.
    * The function handles errors, @see common.hpp for the errors that can occur.
    */
    virtual void onErrorFromSession(errorCodes ec);

private:
public:
    // Buffer for messages read by the connection object which are ready to be processed by the client application.
    queue<msg_struct> _msgInQueue;

    boost::asio::io_context _io_context;
    boost::asio::ssl::context _ssl_context;

    // Thread to run the io_context, the io_context handles async_functions
    std::thread _threadContext;
    std::unique_ptr<connection> _connectionSession;
    std::string _port_;
    std::string _ip_;
};

#endif //CLIENT_INT_HPP