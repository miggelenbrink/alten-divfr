/** @file connection.hpp
* The connection object handles the socket and read/write buffers of a connected socket.
*
* @author <name>
* @date   22-03-21
*
* @todo
*/

#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <string>    
#include <functional> // used for callback functions
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp> //boost::asio::ssl
#include <thread>
#include "queue.hpp"
#include "c_message.hpp"
#include "common.hpp"

/* 
* The connection class.
* The connection object handles the socket 
* and read/write buffers of a connected client.
*/
class connection
{
public:
    /**
    * Constructor. 
    * Receives a tcp::ip::socket and moves it into an owned socket. 
    */
    connection(boost::asio::io_context &io_context,
               boost::asio::ssl::context &context,
               queue<msg_struct> &msgInQueue,
               std::function<void(errorCodes ec)> callback);
    /**
    * Deconstructor. 
    * Destroy socket and clean up all read/write buffers.
    */
    ~connection();

    /**
    * Connect asynchronous to a boost::asio::endpoint. 
    * The functions instructs the socket to async connect to the endpoint. 
    * @param _endp endpoint of type boost::asio::endpoint.  
    */
    void connect_to_server(const boost::asio::ip::tcp::resolver::results_type _endp);

    bool is_socket_open();

    void reconnect();
    /**
    * Disconnect from server.
    * Disconnect is called by the main application.
    */
    void disconnect();

    void start_reading();
    void start_write();

    /**
    * Disconnect from server.
    * Disconnect is called whenever an async function fails, triggers callback disconnectOnError.
    */
    void disconnectOnError();
    /**
    * Write header on socket-stream.
    * Looks up the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized header variable async on the socket-stream.
    */
    void write_header();
    /**
    * Write id on socket-stream.
    * Looks up the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized id variable async on the socket-stream.
    */
    void write_id();
    /**
    * Write payload on socket-stream.
    * Looks up and pops(deletes) the msg_struct in the front of the message write queue (msgOutbuf) and writes it's serialized header variable async on the socket-stream.
    */
    void write_payload();
    /**
    * Reads a header from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of the socket_stream. The 2 bytes are the header and stored into a temporary msg_struct, which is used to construct a message while receiving the header, id and payload.
    * As soon the msg_struct is complete the msg_struct is pushed into a receive queue. The client application can acces message via the receive queue. 
    * When a valid header is received it calls the read_id function.
    */
    void read_header();
    /**
    * Reads a id from the socket-stream.
    * This function reads a fixed size (2bytes/sizeof(size_t)) of the socket_stream. The 2 bytes are the id and stored into a temporary msg_struct, which is used to construct a message while receiving the header, id and payload.
    * When a valid id is received it calls the read_payload function.
    */
    void read_id();
    /**
    * Reads the payload from the socket-stream.
    * This function reads number of bytes specefied in the header of the socket_stream. The payload is also temporary stored into the msg_struct.
    * When a the payload is received the msg_struct is pushed onto the message receive queue and the temporary msg_struct is reset. 
    */
    void read_payload();

    void handshake();

    bool verify_certificate(bool preverified,
                                          boost::asio::ssl::verify_context &ctx);

    // msgOutbuf must be public because the client_interface push_back(s) messages(msg_struct) into the queue.
    queue<msg_struct> msgOutBuf;

private:
public:
    // tmp message to construct message from receiving bytes
    msg_struct tmp_msg;
    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
    queue<msg_struct> &_msgInQueue;
    std::function<void(errorCodes ec)> _callback_func;
};

#endif //CONNECTION_HPP