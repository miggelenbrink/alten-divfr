#ifndef EXCEPTION_TYPES_HPP
#define EXCEPTION_TYPES_HPP

#include <iostream>
#include <exception>

// nw = network error handling
namespace neh
{
    //--ERROR-CODES----------------------------------------------------------------------

    enum error_code
    {
        /* connection error codes */
        err_handshake,
        err_socket_is_open,
        err_read,
        err_write,

        err_load_KEY_FILE,
        err_load_CERT_FILE,
        err_start_server,
        /* msg_framing_stragety_one error codes */
        /* msg1_handler error codes */
        /* queue error codes */
        /* sever_api error codes*/
        /* sever_handler error codes*/
        /* Default */
        err_default,
        err_code_not_initialized
    };

    //----------------------------------------------------------------------------------

    /**
     * Custom exception class for the DNN_ResNet class.
     * 
     * The derived class allows to throw exceptions with an message and error code.
     * The possible error codes can be found in the enum error_code which is in the namespace neh.
     * The catch block can handle the exception and determine what to do depending on the error code.
     */
    class network_lib_excp : public std::exception
    {
    private:
        std::string _msg_exception;
        neh::error_code _ec;

    public:
        network_lib_excp(const std::string msg = "Error message not initialized", const neh::error_code ec = neh::error_code::err_code_not_initialized) : _msg_exception(msg), _ec(ec) {}
        network_lib_excp(const neh::error_code ec) : _msg_exception("Error message not initialized"), _ec(ec) {}

        ~network_lib_excp() {}
        virtual const char *what() const noexcept override
        {
            return _msg_exception.c_str();
        }

        neh::error_code error_code() const
        {
            return _ec;
        }
    };
}
    //----------------------------------------------------------------------------------

#endif //EXCEPTION_TYPES_HPP
