#ifndef SERVER_HPP
#define SERVER_HPP

#include <iostream>
#include "server_handler.hpp"

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */


class server_api : public server_handler
{
public:
    server_api(const short port) : server_handler(port)
    {
    }

    ~server_api()
    {
    }

protected:
    // customize the amout of clients the server can accept. currently only one!
    virtual bool onClientConnect()
    {
        //the server_api must do a disconnect() before it can accept new connections.
        if (client_is_connected())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

private:
};

#endif //SERVER_HPP