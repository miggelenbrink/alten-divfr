#ifndef HEADER_ID_PAYLOAD_HPP
#define HEADER_ID_PAYLOAD_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "i_msg_framing_strategy.hpp"
#include "msg_struct.hpp"
#include "queue.hpp"

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */


class msg_framing_strategy_one : public i_msg_framing_strategy
{
public:
    msg_framing_strategy_one();
    ~msg_framing_strategy_one();

    //created pointer instead of reference, because reference is a delted function.
    virtual void set_queue_and_socket(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *socket, queue<msg_data> *msgOutQueue, queue<msg_data> *msgInQueue) override;
    virtual void start_write() override;
    virtual void start_read() override;

private:
    virtual void write_header() override;
    virtual void write_id() override;
    virtual void write_payload() override;

    virtual void read_header() override;
    virtual void read_id() override;
    virtual void read_payload() override;

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *_socket;
    queue<msg_data> *_msgOutQueue;
    queue<msg_data> *_msgInQueue;

    // temp variables to reconstruct received data.
    size_t _header;
    msg_data _msg;
};

#endif //HEADER_ID_PAYLOAD_HPP