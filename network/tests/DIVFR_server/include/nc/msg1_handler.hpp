/** @file msg1_handler.hpp
* Wrapper class around protobuf generated class.
* 
* @author <name>
* @date   22-03-21
*
* @note To use a generated protobuf class as base class(inheritance), delete the 'final'  keyword in the protobuf classes.
* @todo 
*/

#ifndef S_MESSAGE_HPP
#define S_MESSAGE_HPP

#include <iostream>
#include "message.pb.h"
#include "msg_struct.hpp"

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */


class msg1_handler : public divfr::data
{
public:
    msg1_handler(msg_id id = msg_id::msg1);
    ~msg1_handler();

    // size of the header variable in bytes
    size_t sizeof_header();
    // size of the id variable in bytes
    size_t sizeof_id();
    // size of the payload variable in bytes
    size_t sizeof_payload();

    void set_msg_data(msg_data data);
    msg_data get_msg_data();
    // input: a msg1_handler struct
    // reconstruct protobuf msg1_handler
    // return true on sucesfull, fail on error
    bool parse(msg_data data);
    // place protobuf data in the msg_stuct object
    msg_data serialize();
    
private:
    msg_data _msg_data;
};



#endif //S_MESSAGE_HPP