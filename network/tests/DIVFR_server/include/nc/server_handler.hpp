/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo create function that can open a socket again and connecxt to server, in progress. // void client_interface::reopen_socket_and_connect_to_server(const std::string _ip, const short _port)
*/

#ifndef SERVER_INTERFACE_HPP
#define SERVER_INTERFACE_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include "queue.hpp"
#include "msg_struct.hpp"
#include "connection.hpp"

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */


#define CERT_FILE "/home/guido/Desktop/DIVFR/network/network_libs/server_ssl_lib/input/nc/certificate/server.crt"
#define KEY_FILE "/home/guido/Desktop/DIVFR/network/network_libs/server_ssl_lib/input/nc/certificate/server.key"

// abstract base class;
class server_handler
{
public:
    server_handler(short port);
    ~server_handler();

    bool client_is_connected();
    void disconnect_client();

    void send(const std::string data, const msg_id id);
    
    // all data that is received is stored in htis buffer
    queue<msg_data> _msgInQueue;

protected:
    // callback function, called on a new client connection/session
    virtual bool onClientConnect();

private:
    void do_accept();
    bool start_server();
    void stop_server();

    boost::asio::io_context _io_context;
    boost::asio::ssl::context _ssl_context;

    boost::asio::ip::tcp::acceptor _acceptor;
    std::thread _threadContext;
    std::shared_ptr<connection> _connection;
};

#endif //SERVER_INTERFACE_HPP