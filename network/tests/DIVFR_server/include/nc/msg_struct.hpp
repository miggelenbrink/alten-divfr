#ifndef MSG_STRUCT_HPP
#define MSG_STRUCT_HPP

#include <iostream>

/*
*   Enum messageTypes, message types used in message exchange client and server.
*
*   Contains all possible message types(classes) which the client can use to send to a server.
*   A user can create a new message type by generating a protobuf c++ class and wrap it into 
*   the c_message class. Subsequently the user must manualy add the created message type here 
*   in the Enum. The message type is used as an ID number and send along with the data.Because 
*   of the ID(messageType) the server knows how to parse the received data. Therefore it is 
*   IMPORTANT that the message types here are equal to and have the same order as the message 
*   types in the divfr_TLS_server library Enum.
*   
*   - id/type protoMsg1 stands for messages of the type(class) c_message
*   - id/type <ID_name> stands for messages of the type(class) <class_name>
*/
enum msg_id
{
    msgDefault,
    msg1,
    msg2,
};

struct msg_data
{
    msg_id _id;
    std::string _data;
};

#endif //MSG_STRUCT_HPP