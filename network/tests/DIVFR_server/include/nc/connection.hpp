/** @file connection.hpp
* The connection object handles the socket and read/write buffers of a connected socket.
*
* @author <name>
* @date   22-03-21
*
* @todo
*/

#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <functional> //callback functions

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */

#include "queue.hpp"
#include "msg_struct.hpp"
#include "msg_framing_strategy_one.hpp"
#include "i_msg_framing_strategy.hpp"

class connection
{
public:
    connection(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket,
            queue<msg_data> &msgInQueue);
    ~connection();

    bool is_socket_open();
    void stop_socket();

    void start_handshake();

    void start_reading();
    void start_write();
    void set_msg_framing_strategy(std::unique_ptr<i_msg_framing_strategy> obj);


private:
    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
    queue<msg_data> &_msgInQueue;
    std::unique_ptr<i_msg_framing_strategy> _msg_framing_strategy;

public:
    queue<msg_data> msgOutBuf;
};

#endif //CONNECTION_HPP