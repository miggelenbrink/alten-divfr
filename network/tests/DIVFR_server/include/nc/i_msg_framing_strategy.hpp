#ifndef I_MESSAGE_FRAMING_STRATEGY_HPP
#define I_MESSAGE_FRAMING_STRATEGY_HPP

#include <iostream>
#include "msg_struct.hpp"
#include "queue.hpp"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

//x #include "spdlog/spdlog.h"     /* add global space spdlog to this file, to use created loggers in this file*/
#include "../exception_types.hpp" /* custom exceptions for error handling */


class i_msg_framing_strategy
{
public:
    virtual void start_write() = 0;
    virtual void start_read() = 0;

    virtual void set_queue_and_socket(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *socket, queue<msg_data> *msgOutQueue, queue<msg_data> *msgInQueue) = 0;

private:
    virtual void write_header() = 0;
    virtual void write_id() = 0;
    virtual void write_payload() = 0;

    virtual void read_header() = 0;
    virtual void read_id() = 0;
    virtual void read_payload() = 0;

};

#endif //I_MESSAGE_FRAMING_STRATEGY_HPP