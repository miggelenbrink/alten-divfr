#include <iostream>
#include <signal.h> //signal handler ctrl + c
#include <ostream>
#include <fstream>

#include "../include/nc/server_api.hpp"
#include "../include/nc/msg1_handler.hpp"

#define OUT_PATH "../output/apple_out.jpg"

int main(int argc, char *argv[])
{
    server_api server_test(6000);
    msg_data msg_test;
    msg1_handler msg_handler;
    bool Exit_FLAG = false; // true, close the application
    
    while (true)
    {
        while (!Exit_FLAG)
        {
            // pass the specified number of message by the user to onMessage function! and if there are no message do nothing
            if (!server_test._msgInQueue.empty())
            {
                msg_test = server_test._msgInQueue.pop_front();
                break;
            }
        }
        std::cout << msg_test._id << std::endl;

        msg_handler.parse(msg_test);

        std::ofstream output(OUT_PATH, std::ios::binary);

        std::copy(
            msg_handler.image_data().begin(),
            msg_handler.image_data().end(),
            std::ostreambuf_iterator<char>(output));
    }

    return 0;
}