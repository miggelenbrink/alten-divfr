#include "../include/nc/server_handler.hpp"

server_handler::server_handler(short port)
    : _acceptor(_io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)),
      _ssl_context(boost::asio::ssl::context::sslv23)
{
    //x spdlog::debug("Construct server_handler");
    _ssl_context.set_options(
        boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 |
        // /Always create a new key when using tmp_dh parameters. = empahral key pair, always new keypair on new connection = forward secrecy
        boost::asio::ssl::context::single_dh_use);

    // load the certificates! we only use one certificate, it is not really a chain!
    boost::system::error_code ec;

    _ssl_context.use_certificate_chain_file(CERT_FILE, ec);
    if (ec)
    {
        //x SPDLOG_WARN("Path to .cert file is incorrect {0}", CERT_FILE);
        throw neh::network_lib_excp(ec.message(), neh::error_code::err_load_CERT_FILE);
    }
    _ssl_context.use_private_key_file(KEY_FILE, boost::asio::ssl::context::pem, ec);
    if (ec)
    {
        //x SPDLOG_WARN("Path to .key file is incorrect {0}", KEY_FILE);
        throw neh::network_lib_excp(ec.message(), neh::error_code::err_load_KEY_FILE);
    }

    start_server();
}

server_handler::~server_handler()
{
    //x spdlog::debug("Deconstruct server_handler");
    stop_server();
}

void server_handler::stop_server()
{
    if (!_io_context.stopped())
        _io_context.stop();

    if (_threadContext.joinable())
        _threadContext.join();

    if (!(_connection.use_count() == 0))
    {
        _connection->stop_socket();
        _connection.reset();
        //x spdlog::debug("Disconnected client succesfully");
    }
}

bool server_handler::start_server()
{
    try
    {
        // give the context work
        do_accept();
        //x spdlog::info("Wait for new connection");

        if (_io_context.stopped())
            _io_context.restart();

        // run context
        _threadContext = std::thread([this]() { _io_context.run(); });
    }
    catch (std::exception &e)
    {
        //x SPDLOG_WARN("Failed to start the server");
        throw neh::network_lib_excp(e.what(), neh::error_code::err_start_server);
        return false;
    }

    return true;
}

void server_handler::disconnect_client()
{
    // delete left messages.
    _msgInQueue.clear();

    // if no client is connected, then do nothing
    if (!(_connection.use_count() == 0))
    {
        _connection.reset();
        //x spdlog::debug("Disconnected client");
        //x spdlog::debug("Wait for new connection");
    }
    else
    {
        //x spdlog::debug("No client connected, thus disconnect call is unnecessary");
    }
}

bool server_handler::client_is_connected()
{
    if (_connection.use_count() == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void server_handler::send(const std::string data, const msg_id id)
{
    if (!(_connection.use_count() == 0))
    {
        if (_connection->is_socket_open())
        {
            _io_context.post([this, data, id]() {
                //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
                bool WriteInprocess = _connection->msgOutBuf.empty();

                // Write message into the outgoing message queue (buffer).
                msg_data temp;
                temp._data = data;
                temp._id = id;
                _connection->msgOutBuf.push_back(temp);

                // Tell the connection object to write all message in the message queue to the socket stream/server.
                if (WriteInprocess)
                    _connection->start_write();
            });
        }
        else
        {
            //x SPDLOG_WARN("Send failed, message to be send is not stored in the send queue");
            throw neh::network_lib_excp(neh::error_code::err_socket_is_open);
        }
    }
    else
    {
        //x SPDLOG_WARN("Send failed, because there is no client connected, message to be send is not stored in the send queue");
    }
}

// callback function, called on a new client connection/session
bool server_handler::onClientConnect()
{
    //default behavior virtual/abstract function
    //return true to accept the new client and delete the current one
    //return false to deny the client
    return true;
}

void server_handler::do_accept()
{

    _acceptor.async_accept(
        [this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
            if (!ec)
            {
                std::cerr << "[SERVER] client " << socket.remote_endpoint().address() << " is attempting to connect" << std::endl;
                std::shared_ptr<connection> tmp_conn =
                    std::make_shared<connection>(
                        boost::asio::ssl::stream<boost::asio::ip::tcp::socket>(
                            std::move(socket), _ssl_context),
                        _msgInQueue);

                if (onClientConnect())
                {
                    //x spdlog::debug("Accepted client connection");

                    // start session/connection object/ connection object start reading received messages and pushing them into a queue
                    tmp_conn->start_handshake();
                    _connection = tmp_conn;
                }
                else
                {
                    //x SPDLOG_WARN("Denied client connection");
                }
            }
            do_accept();
        });
}