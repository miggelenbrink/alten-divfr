#include "../include/nc/msg_framing_strategy_one.hpp"

msg_framing_strategy_one::msg_framing_strategy_one()
{
    //x spdlog::debug("Construct msg_framing_strategy_one")
}

msg_framing_strategy_one::~msg_framing_strategy_one()
{
    //x spdlog::debug("Deconstruct msg_framing_strategy_one")
}

void msg_framing_strategy_one::set_queue_and_socket(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *socket, queue<msg_data> *msgOutQueue, queue<msg_data> *msgInQueue)
{
    _socket = socket;
    _msgOutQueue = std::move(msgOutQueue);
    _msgInQueue = std::move(msgInQueue);
}

void msg_framing_strategy_one::start_write()
{
    write_header();
}

void msg_framing_strategy_one::start_read()
{
    read_header();
}

void msg_framing_strategy_one::write_header()
{
    // prepare header
    auto header = _msgOutQueue->front()._data.size();
    std::string header_serialized;
    header_serialized.resize(sizeof(header));
    header_serialized.assign(reinterpret_cast<const char *>(&(header)), sizeof(header));

    // write header
    boost::asio::async_write(*_socket, boost::asio::buffer(header_serialized, sizeof(header_serialized)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     write_id();
                                 }
                                 else
                                 {
                                     //x SPDLOG_WARN("Async_write header failed");
                                     throw neh::network_lib_excp(ec.message(), neh::error_code::err_write);
                                 }
                             });
}

void msg_framing_strategy_one::write_id()
{
    // prepare id
    auto id = _msgOutQueue->front()._id;
    std::string id_serialized;
    id_serialized.resize(sizeof(id));
    id_serialized.assign(reinterpret_cast<const char *>(&(id)), sizeof(id));

    // prepare payload
    auto payload = _msgOutQueue->front()._data;

    // write id
    boost::asio::async_write(*_socket, boost::asio::buffer(id_serialized, sizeof(id_serialized)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //x spdlog::debug("Write id Succesfully, wrote {0:d} bytes", length);
                                     if (payload.size() > 0)
                                     {
                                         write_payload();
                                     }
                                     else
                                     {
                                         _msgOutQueue->pop_front();

                                         if (!_msgOutQueue->empty())
                                         {
                                             start_write();
                                         }
                                     }
                                 }
                                 else
                                 {
                                     //x SPDLOG_WARN("Async_write id failed");
                                     throw neh::network_lib_excp(ec.message(), neh::error_code::err_write);
                                 }
                             });
}

void msg_framing_strategy_one::write_payload()
{
    auto payload = _msgOutQueue->front()._data;

    boost::asio::async_write(*_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //x spdlog::debug("Write payload Succesfully, wrote {0:d} bytes", length);

                                     _msgOutQueue->pop_front();

                                     if (!_msgOutQueue->empty())
                                     {
                                         //x spdlog::debug("Start new write because there are still messages in the write queue");
                                         start_write();
                                     }
                                 }
                                 else
                                 {
                                     //x SPDLOG_WARN("Async_write payload failed");
                                     throw neh::network_lib_excp(ec.message(), neh::error_code::err_write);
                                 }
                             });
}

void msg_framing_strategy_one::read_header()
{
    boost::asio::async_read(*_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&_header),
                                                sizeof(_header)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    //x spdlog::debug("Read header Succesfully, read {0:d} bytes and the header contains the value {1:d}", length, _header);
                                    read_id();
                                }
                                else
                                {
                                    _header = 0;
                                    //x SPDLOG_WARN("Async_read header failed");
                                    throw neh::network_lib_excp(ec.message(), neh::error_code::err_read);
                                }
                            });
}

void msg_framing_strategy_one::read_id()
{
    boost::asio::async_read(*_socket,
                            boost::asio::buffer(reinterpret_cast<char *>(&_msg._id),
                                                sizeof(_msg._id)),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    //x spdlog::debug("Read id Succesfully, read {0:d} bytes and the message id is: {1:d}", length, _msg._id);
                                    //check if message has payload
                                    if (_header > 0)
                                    {
                                        try
                                        {
                                            _msg._data.resize(_header);
                                            read_payload();
                                        }
                                        catch (const std::length_error &le)
                                        {
                                            _header = 0;
                                            _msg._data.erase();

                                            //x SPDLOG_WARN("Length error on received header");
                                            throw neh::network_lib_excp(ec.message(), neh::error_code::err_read);
                                        }
                                    }
                                    else
                                    {
                                        //x SPDLOG_WARN("Received message without payload, discard message and wait for new message");
                                        start_read();
                                    }
                                }
                                else
                                {
                                    //x SPDLOG_WARN("Async_read id failed");
                                    throw neh::network_lib_excp(ec.message(), neh::error_code::err_read);
                                }
                            });
}

void msg_framing_strategy_one::read_payload()
{
    boost::asio::async_read(*_socket,
                            boost::asio::buffer(_msg._data.data(),
                                                _header),
                            [this](std::error_code ec, std::size_t lenght) {
                                if (!ec)
                                {
                                    //x spdlog::debug("Read payload Succesfully, read {0:d} bytes", length);

                                    //tmp_msg complete, add to message IN queue
                                    _msgInQueue->push_back(_msg);

                                    //@todo implemetn reset function in struct, for now reset member variables here, ready for new read
                                    _header = 0;
                                    _msg._data.clear();

                                    //the server should always listen for messages, thus give the io_context the task to listing for a new header
                                    //read_header(); same as start but with is_connected check
                                    start_read();
                                }
                                else
                                {
                                    //x SPDLOG_WARN("Async_read payload failed");
                                    throw neh::network_lib_excp(ec.message(), neh::error_code::err_read);
                                }
                            });
}
