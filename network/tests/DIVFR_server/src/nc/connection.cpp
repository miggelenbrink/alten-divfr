#include "../include/nc/connection.hpp"

connection::connection(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> socket,
                       queue<msg_data> &msgInQueue)
    : _socket(std::move(socket)), _msgInQueue(msgInQueue)
{
    set_msg_framing_strategy(std::move(std::make_unique<msg_framing_strategy_one>()));
    //x spdlog::debug("Construct connection");
}

connection::~connection()
{
    //x spdlog::debug("Deconstruct connection");
}

bool connection::is_socket_open()
{
    return _socket.lowest_layer().is_open();
}

// Disconnect function
void connection::stop_socket()
{
    if (is_socket_open())
    {
        _socket.lowest_layer().close();
        //x spdlog::debug("Socket is closed");
    }
    else
    {
        //x SPDLOG_WARN("Stop socket failed, because socket was not open");
        throw neh::network_lib_excp(neh::error_code::err_socket_is_open);
    }
}

void connection::start_reading()
{
    if (is_socket_open())
    {
        _msg_framing_strategy->start_read();
    }
    else
    {
        //x SPDLOG_WARN("Start reading failed because there is no socket/connection open");
        throw neh::network_lib_excp(neh::error_code::err_socket_is_open);
    }
}

void connection::set_msg_framing_strategy(std::unique_ptr<i_msg_framing_strategy> obj)
{
    _msg_framing_strategy = std::move(obj);
    _msg_framing_strategy->set_queue_and_socket(&_socket, &msgOutBuf, &_msgInQueue);
}

void connection::start_write()
{
    if (_socket.lowest_layer().is_open())
    {
        _msg_framing_strategy->start_write();
    }
    else
    {
        //x SPDLOG_WARN("Start write failed because there is no socket/connection open");
        throw neh::network_lib_excp(neh::error_code::err_socket_is_open);
    }
}

void connection::start_handshake()
{
    _socket.async_handshake(boost::asio::ssl::stream_base::server,
                            [this](const boost::system::error_code &error) {
                                if (!error)
                                {
                                    start_reading();
                                }
                                else
                                {
                                    //x SPDLOG_WARN("Handshake with client failed");
                                    throw neh::network_lib_excp(neh::error_code::err_handshake);
                                }
                            });
}
