#set library name!
set(TARGET_NAME _network_communication_server)

#find_package(Boost 1.75.0 REQUIRED)
#see below for boost!
find_package(Protobuf REQUIRED)
find_package(OpenSSL REQUIRED)

set(HEADERS
../include/nc/connection.hpp
../include/nc/i_msg_framing_strategy.hpp
../include/nc/msg_framing_strategy_one.hpp
../include/nc/msg_struct.hpp
../include/nc/queue.hpp
../include/nc/server_api.hpp
../include/nc/server_handler.hpp
../include/nc/msg1_handler.hpp
../include/nc/message.pb.h
)

set(SOURCES
nc/connection.cpp
nc/msg_framing_strategy_one.cpp
nc/server_handler.cpp 
nc/msg1_handler.cpp
nc/message.pb.cc
)

set(LINK_LIBRARIES
    pthread
    ${Protobuf_LIBRARIES}
   #${Boost_LIBRARIES}
    ${OPENSSL_LIBRARIES}
)


set(COMPILE_OPTIONS)

add_library(${TARGET_NAME} ${SOURCES})
target_link_libraries(${TARGET_NAME} ${LINK_LIBRARIES})

# Must use the newest boost version! otherwise SSL stream sockets/ acceptor function does not work
target_include_directories(${TARGET_NAME} PUBLIC /usr/local/boost_1_75_0)

target_include_directories(${TARGET_NAME} PUBLIC .)
target_compile_options(${TARGET_NAME} INTERFACE ${COMPILE_OPTIONS})

# set output folder CMAKE_SOURCE_DIR is the path where the root CMakeLists.txt is.
set_target_properties(${TARGET_NAME}
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}"
)

# g++ -I include -I /usr/local/boost_1_75_0 -o main main.cpp ../src/message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread -g -lcrypto -lssl



#  sudo apt-get install libboost-all-dev



