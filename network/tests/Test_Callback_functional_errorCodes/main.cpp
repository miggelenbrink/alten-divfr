#include <iostream>
#include <functional>

enum errorCodes
{
    connErr, //must be non-zero otherwise collision with std::error_code
    writeErr,
    readErr,
};

std::function<void()> _func1;

void set_errCallback(std::function<void()> func1)
{
    _func1 = func1;
}

errorCodes onError(int ec)
{
    switch (ec)
    {
    case errorCodes::connErr:

        if (_func1 == nullptr)
        {
            std::cout << "[CLIENT onError()] connection failed" << std::endl;
        }
        else
        {
            _func1();
        }
        break;

    case errorCodes::writeErr:

        std::cout << "[CLIENT onError()] write failed" << std::endl;
        // @todo retry send msg,
        // if fails again, delete front of msgOutbuf and retry
        break;

    case errorCodes::readErr:

        std::cout << "[CLIENT onError()] read failed" << std::endl;
        break;

    default:
        std::cout << "[CLIENT onError()] no error code available" << std::endl;
        break;
    }
}

int main()
{
    set_errCallback([](){ std::cout << "if you read this the callback function is called" ; });

    onError(errorCodes::connErr);

    return 0;
}