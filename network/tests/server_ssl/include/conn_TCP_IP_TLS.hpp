#ifndef CONN_TCP_IP_TLS_HPP
#define CONN_TCP_IP_TLS_HPP

#include <iostream>
#include "i_api_server_comm.hpp"
#include "i_msg_framing_strategy.hpp"
#include "msg_struct.hpp"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#define CERT_FILE "/home/guido/Desktop/DIVFR/network/network_libs/server_ssl_lib/certificate/server.crt"
#define KEY_FILE "/home/guido/Desktop/DIVFR/network/network_libs/server_ssl_lib/certificate/server.key"

class conn_TCP_IP_TLS : public i_api_server_comm
{
public:
    conn_TCP_IP_TLS(boost::asio::ip::tcp::socket socket,
                    boost::asio::io_context io_context);
    ~conn_TCP_IP_TLS();

    virtual void disconnect() override;
    virtual bool is_connected() override;
    virtual bool write(msg_data data) override;
    virtual bool start_server() override;
    void set_msg_framing_strategy(std::unique_ptr<i_msg_framing_strategy> obj);

    void handshake();

private:
    virtual void start_reading() override;
    virtual void start_write() override;
    virtual bool stop_server() override;


    boost::asio::io_context &_io_context;
    boost::asio::ssl::context _ssl_context;

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _socket;
    boost::asio::ip::tcp::acceptor _acceptor;

    std::thread _threadContext;
    queue<msg_data> _msgOutQueue;
    queue<msg_data> &_msgInQueue;

    std::unique_ptr<i_msg_framing_strategy> _msg_framing_strategy;
};

#endif //CONN_TCP_IP_TLS_HPP