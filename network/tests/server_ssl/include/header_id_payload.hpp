#ifndef HEADER_ID_PAYLOAD_HPP
#define HEADER_ID_PAYLOAD_HPP

#include <iostream>
#include "i_msg_framing_strategy.hpp"
#include "msg_struct.hpp"
#include "queue.hpp"
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

class header_id_payload : public i_msg_framing_strategy
{
public:
    header_id_payload(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> &socket,
                      queue<msg_data> &msgOutQueue);
    ~header_id_payload();

    virtual void start_write() override;
    virtual void start_read() override;

private:
    virtual void write_header() override;
    virtual void write_id() override;
    virtual void write_payload() override;

    virtual void read_header() override;
    virtual void read_id() override;
    virtual void read_payload() override;

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> &_socket;
    queue<msg_data> &_msgOutQueue;
};

#endif //HEADER_ID_PAYLOAD_HPP