#ifndef I_MESSAGE_FRAMING_STRATEGY_HPP
#define I_MESSAGE_FRAMING_STRATEGY_HPP

#include <iostream>
#include "msg_struct.hpp"

class i_msg_framing_strategy
{
public:
    virtual void start_write() = 0;
    virtual void start_read() = 0;

private:
    virtual void set_msg_struct(const std::string data, const msg_id id) = 0;

    virtual void write_header() = 0;
    virtual void write_id() = 0;
    virtual void write_payload() = 0;

    virtual void read_header() = 0;
    virtual void read_id() = 0;
    virtual void read_payload() = 0;

    msg_struct _msg;
    std::string _serialized_data;
};

#endif //I_MESSAGE_FRAMING_STRATEGY_HPP