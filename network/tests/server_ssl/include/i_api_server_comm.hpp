#ifndef I_API_SERVER_COMM_HPP
#define I_API_SERVER_COMM_HPP

#include <iostream>
#include "queue.hpp"
#include "i_msg_framing_strategy.hpp"

class i_api_server_comm
{
public:
    virtual void disconnect() = 0;
    virtual bool is_connected() = 0;
    virtual bool write(msg_data data) = 0;
    //virtual void set_msg_framing_strategy(std::unique_ptr<i_msg_framing_strategy> obj) = 0;

private:
    virtual void start_reading() = 0;
    virtual void start_write() = 0;
    virtual bool start_server() = 0;
    virtual bool stop_server() = 0;

    queue<std::string> _msgOutQueue;
    queue<std::string> &_msgInQueue;
};

#endif //I_API_SERVER_COMM_HPP