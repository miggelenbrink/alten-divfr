#ifndef API_SERVER_COMM_HPP
#define API_SERVER_COMM_HPP

#include <iostream>
#include "queue.hpp"
#include "conn_TCP_IP_TLS.hpp"
#include "msg_struct.hpp"

class api_server_comm
{
public:
    api_server_comm(const short port);
    ~api_server_comm();

    bool start_server();
    void disconnect();
    bool is_connected();
    void send(const std::string data, const msg_id id);
    void on_update();

bool onClientConnect();


private:
    void do_accept();

    boost::asio::io_context _io_context;
    boost::asio::ip::tcp::acceptor _acceptor;

    std::shared_ptr<conn_TCP_IP_TLS> _connection;
    queue<msg_data> _msgInQueue;
};

#endif //API_SERVER_COMM_HPP