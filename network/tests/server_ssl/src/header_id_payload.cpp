#include "../include/header_id_payload.hpp"

header_id_payload::header_id_payload(boost::asio::ssl::stream<boost::asio::ip::tcp::socket> &socket,
                                     queue<msg_data> &msgOutQueue)
    : _socket(socket), _msgOutQueue(msgOutQueue)
{
}

header_id_payload::~header_id_payload()
{
}

void header_id_payload::start_write()
{

    write_header();
}

void header_id_payload::start_read()
{
}

void header_id_payload::write_header()
{
    // prepare header
    auto header = _msgOutQueue.front()._data.size();
    std::string header_serialized;
    header_serialized.resize(sizeof(header));
    header_serialized.assign(reinterpret_cast<const char *>(&(header)), sizeof(header));

    // write header
    boost::asio::async_write(_socket, boost::asio::buffer(header_serialized, sizeof(header_serialized)),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     //std::cout << "[SERVER connection.hpp write_header()] async_write succesfully, wrote " << length << " bytes" << std::endl;
                                     write_id();
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_header()] async_write failed  " << ec.message() << std::endl;
                                     //_callback_func(write_ERR);
                                 }
                             });
}

void header_id_payload::write_id()
{
    // prepare id
    auto id = _msgOutQueue.front()._id;
    std::string id_serialized;
    id_serialized.resize(sizeof(id));
    id_serialized.assign(reinterpret_cast<const char *>(&(id)), sizeof(id));

    // prepare payload
    auto payload = _msgOutQueue.front()._data;

    // write id
    boost::asio::async_write(_socket, boost::asio::buffer(id_serialized, sizeof(id_serialized)),
                             [this, payload](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     if (payload.size() > 0)
                                     {
                                         //std::cerr << "[SERVER connection.hpp write_id()] write id succesfully, wrote " << length << " bytes" << std::endl;
                                         write_payload();
                                     }
                                     else
                                     {
                                         _msgOutQueue.pop_front();

                                         if (!_msgOutQueue.empty())
                                         {
                                             //write_header();
                                             start_write();
                                         }
                                         std::cout << "[SERVER] write succesfull, Warning: message has no body" << std::endl;
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_id()] async_write failed  " << ec.message() << std::endl;
                                     //_callback_func(write_ERR);
                                 }
                             });
}

void header_id_payload::write_payload()
{
    auto payload = _msgOutQueue.front()._data;

    boost::asio::async_write(_socket, boost::asio::buffer(payload, payload.size()),
                             [this](std::error_code ec, std::size_t length) {
                                 if (!ec)
                                 {
                                     std::cerr << "[SERVER] write succesful! Payload is " << length << " bytes." << std::endl;

                                     _msgOutQueue.pop_front();

                                       if (!_msgOutQueue.empty())
                                       {
                                           std::cerr << "[SERVER] Message out queue not empty, start new write." << std::endl;

                                           start_write();
                                       }
                                 }
                                 else
                                 {
                                     std::cout << "[SERVER connection.hpp write_payload()] async_write failed  " << ec.message() << std::endl;
                                     //_callback_func(write_ERR);
                                 }
                             });
}

void read_header()
{
}

void read_id()
{
}

void read_payload()
{
}
