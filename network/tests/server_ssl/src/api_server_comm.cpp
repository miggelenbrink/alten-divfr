#include "../include/api_server_comm.hpp"

api_server_comm::api_server_comm(const short port) : _acceptor(_io_context, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
}

api_server_comm::~api_server_comm()
{
}

bool api_server_comm::start_server()
{
    // give server work to do/ give io_context work to do
    do_accept();
    _connection->start_server();
}

void api_server_comm::disconnect()
{
    _connection->disconnect();
}

bool api_server_comm::is_connected()
{
    return _connection->is_connected();
}

void api_server_comm::send(const std::string data, const msg_id id)
{
    msg_data temp_data;
    temp_data._id = id;
    temp_data._data = data;

    if (!(_connection.use_count() == 0))
    {
        if (_connection->is_connected())
        {
            _connection->write(temp_data);
        }
        else
        {
            std::cout << "[SERVER] server_interface: send failed because socket to client is not open, msg is NOT stored in msgOutbuf " << std::endl;
            // onErrorFromSession(errorCodes::socket_is_open_ERR);
        }
    }
    else
    {
        std::cout << "[SERVER] server_interface: send failed because there is no client connected, msg is NOT stored in msgOutbuf " << std::endl;
    }
}

void api_server_comm::on_update()
{
}

// callback function, called on a new client connection/session
bool api_server_comm::onClientConnect()
{
    //return true to accept the new client and delete the current one
    //return false to deny the client
    return true;
}

void api_server_comm::do_accept()
{
    _acceptor.async_accept(
        [this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket) {
            if (!ec)
            {
                std::cerr << "[SERVER] client " << socket.remote_endpoint() << " attempting to connect" << std::endl;

                std::shared_ptr<conn_TCP_IP_TLS> tmp_conn =
                    std::make_shared<conn_TCP_IP_TLS>(
                        std::move(socket), _io_context);

                if (onClientConnect())
                {
                    std::cerr << "[SERVER] client accepeted" << std::endl;
                    // start session/connection object/ connection object start reading received messages and pushing them into a queue
                    tmp_conn->handshake();
                    _connection = tmp_conn;
                }
                else
                {
                    std::cerr << "[SERVER] client denied" << std::endl;
                }
            }
            do_accept();
        });
}