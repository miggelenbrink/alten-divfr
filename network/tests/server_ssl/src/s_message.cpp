#include "../include/msg1_handler.hpp"

msg_handler::msg_handler(msg_ids id) : protoMsg1Struct{0, id}
{
}

msg_handler::~msg_handler()
{
}

size_t msg_handler::size_header_id_payload()
{
    return ByteSizeLong() + sizeof(protoMsg1Struct.header) + sizeof(protoMsg1Struct.id);
}

size_t msg_handler::sizeof_id()
{
    return sizeof(protoMsg1Struct.id);
}

size_t msg_handler::sizeof_header()
{
    return sizeof(protoMsg1Struct.header);
}

size_t msg_handler::size_payload_serialized()
{
    return protoMsg1Struct.payload_serialized.size();
}

bool msg_handler::parseprotoMsg1Struct(msg_struct data)
{
    if (!ParseFromString(data.payload_serialized))
    {
        std::cout << "[SERVER] Parse payload failed - ParseFromString()" << std::endl;
        return false;
    }
    else
    {
        std::cout << "[SERVER] Payload succesfully parsed - ParseFromString() " << std::endl;
        return true;
    }
}

void msg_handler::setprotoMsg1Struct()
{
    //make const if everything works fine
    size_t sizeofHeader = sizeof_header();
    size_t sizeofId = sizeof_id();

    protoMsg1Struct.header = ByteSizeLong();
    // allocate space for serialized msg_handler
    protoMsg1Struct.payload_serialized.resize(protoMsg1Struct.header);

    protoMsg1Struct.header_serialized.resize(sizeofHeader);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.header_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.header)), sizeof(protoMsg1Struct.header));

    protoMsg1Struct.id_serialized.resize(sizeofId);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.id_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.id)), sizeof(protoMsg1Struct.id));

    if (!SerializeToString(&protoMsg1Struct.payload_serialized))
    {
        std::cout << "[CLIENT msg_handler.hpp setprotoMsg1Struct()] SerializeToString() failed " << std::endl;
    }
    else
    {
        std::cout << "[CLIENT msg_handler.hpp setprotoMsg1Struct()] SerializeToString() succesfully " << std::endl;
    }
}

msg_struct msg_handler::getprotoMsg1Struct()
{
    //optional, but easy to forget to do a setprotoMsg1Struct before calling getprotoMsg1Struct
    return protoMsg1Struct;
}

std::string msg_handler::get_serialized_id()
{
    return protoMsg1Struct.id_serialized;
}

std::string msg_handler::get_serialized_header()
{
    return protoMsg1Struct.header_serialized;
}

std::string msg_handler::get_serialized_payload()
{
    return protoMsg1Struct.payload_serialized;
}