#include "../include/conn_TCP_IP_TLS.hpp"

conn_TCP_IP_TLS::conn_TCP_IP_TLS(boost::asio::ip::tcp::socket socket,
                                 boost::asio::io_context io_context)
    : _ssl_context(boost::asio::ssl::context::sslv23), _io_context(io_context), _socket(std::move(socket), _ssl_context)
    {
        _ssl_context.set_options(
            boost::asio::ssl::context::default_workarounds |
            boost::asio::ssl::context::no_sslv2 |
            // /Always create a new key when using tmp_dh parameters. = empahral key pair, always new keypair on new connection = forward secrecy
            boost::asio::ssl::context::single_dh_use);

        // load the certificates! we only use one certificate, it is not really a chain!
        boost::system::error_code ec;

        _ssl_context.use_certificate_chain_file(CERT_FILE, ec);
        if (ec)
        {
            std::cout << ec.message() << std::endl;
            std::cout << "check path to .cert file in common.hpp file CERT_FILE" << std::endl;
        }
        _ssl_context.use_private_key_file(KEY_FILE, boost::asio::ssl::context::pem, ec);
        if (ec)
        {
            std::cout << ec.message() << std::endl;
            std::cout << "check path to .cert file in common.hpp file CERT_FILE" << std::endl;
        }

        start_server();
    }

    conn_TCP_IP_TLS::~conn_TCP_IP_TLS()
    {
        stop_server();
        std::cout << "[SERVER] server closed, deconstructor server_interface" << std::endl;
    }

    bool conn_TCP_IP_TLS::start_server()
    {
        try
        {
            std::cerr << "[SERVER] Waiting for incoming connection.." << std::endl;

            if (_io_context.stopped())
                _io_context.restart();

            // run context
            _threadContext = std::thread([this]() { _io_context.run(); });
        }
        catch (std::exception &e)
        {
            std::cerr << "[SERVER] Failed to start the server, server_interface, start_server(): " << e.what() << std::endl;
            return false;
        }

        return true;
    }

    bool conn_TCP_IP_TLS::stop_server()
    {
    }

    void conn_TCP_IP_TLS::disconnect()
    {
        if (!_io_context.stopped())
            _io_context.stop();

        if (_threadContext.joinable())
            _threadContext.join();

        if (_socket.lowest_layer().is_open())
        {
            _socket.lowest_layer().close();
        }
        else
        {
            //_callback_func(socket_is_open_ERR);
        }
        //std::cout << "[SERVER] disconnected client" << std::endl;
    }

    bool conn_TCP_IP_TLS::is_connected()
    {
        return _socket.lowest_layer().is_open();
    }

    bool conn_TCP_IP_TLS::write(msg_data data)
    {
    _io_context.post([this, data]() 
    {
        //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
        bool WriteInprocess = _msgOutQueue.empty();

        // Write message into the outgoing message queue (buffer).
        _msgOutQueue.push_back(data);

        // Tell the connection object to write all message in the message queue to the socket stream/server.
        if (WriteInprocess)
            start_write();
    };
    }

    void conn_TCP_IP_TLS::start_reading()
    {
        if (_socket.lowest_layer().is_open())
        {
            // read_header();
        }
        else
        {
            //_callback_func(socket_is_open_ERR);
        }
    }

    void conn_TCP_IP_TLS::start_write()
    {
        if (_socket.lowest_layer().is_open())
        {
            _msg_framing_strategy->start_write();
        }
        else
        {
            //_callback_func(socket_is_open_ERR);
        }
    }

    void conn_TCP_IP_TLS::set_msg_framing_strategy(std::unique_ptr<i_msg_framing_strategy> obj)
    {
        _msg_framing_strategy = std::move(obj);
    }

    void conn_TCP_IP_TLS::handshake()
    {
        _socket.async_handshake(boost::asio::ssl::stream_base::server,
                                [this](const boost::system::error_code &error) {
                                    if (!error)
                                    {
                                        start_reading();
                                    }
                                    else
                                    {
                                        std::cout << "[SERVER} handshake failed: " << error.message() << std::endl;
                                        _callback_func(handshake_ERR);
                                    }
                                });
    }