// g++ -o write_protobuf write_protobuf.cpp message.pb.cc -pthread -lprotobuf -lpthread -g

#include <iostream>
#include <fstream>
#include <string>
#include "message.pb.h"
using namespace std;

int main(int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    std::ifstream file("input/apple.jpg", std::ios::binary);
    std::vector<unsigned char> buffer(std::istreambuf_iterator<char>(file), {});
    //vector to string (binary conversion for protobuf)
    //const
    const std::string str(buffer.begin(), buffer.end());
    //std::string str1(std::istreambuf_iterator<char>(file), {});
    //no formatting happens, only when you print something out (printf/cout) then formattingi is applied
    //aslong as you use iterators and do no output anything, but just copy string in antoher string nothing happens to the data,
    //with iterators you can copy the exact bytes back and forth!
    //do you strings, because they do no append /0 charactesr! thus no formatting

    /*

    for (auto x : buffer)
        std::cout << signed(x);

*/

    //cout << "\n\n\n\n\n\n string:" << endl;

    //  cout << str << endl;

    // cl = client
    // se = server
    divfr::data cl_message;

    //add data
    cl_message.set_age(22);
    cl_message.set_img_fname("<name>");
    cl_message.set_img_fformat("<format>");
    //cl_message.set_image_data(buffer.data(), buffer.size());
    cl_message.set_image_data(str);
    std::cout << " Bytesizelong, actual size " << cl_message.ByteSizeLong() << std::endl;

    //get #bytes serialized messsage would take up
    size_t size_cl_message = cl_message.ByteSizeLong();

    cout << "wrote " << size_cl_message << " bytes" << endl;

    //write cl_message to disk
    fstream output("input/cl_message", ios::out | ios::trunc | ios::binary);

    //write header to socket
    //size_t is at leat 32 bits
    // do binart write
    size_t data_size{cl_message.ByteSizeLong()};
    cout << "data size: " << data_size << endl;

    //

    //char temp[data_size];
    std::string temp;
    temp.resize(data_size);
    if (!cl_message.SerializeToString(&temp) )
    {
        cerr << "Failed to write address book." << endl;
        return -1;
    }

    std::string temp2;
    temp2.resize(sizeof(data_size));
    temp2.assign(reinterpret_cast<const char *>(&data_size), sizeof(data_size));

    // char temp2[sizeof(data_size)];
    // mempcpy(temp2, reinterpret_cast<const char *>(&data_size), sizeof(data_size));

    //write HEADER as binary to file
    //output.write(reinterpret_cast<const char *>(&data_size), sizeof(data_size));
    output.write(temp2.data(), sizeof(data_size));
    ;
    //write Message/body/payload
    output.write((char*)temp.data(), data_size);

    /*
    if (!cl_message.SerializeToOstream(&output))
    {
        cerr << "Failed to write address book." << endl;
        return -1;
    }
*/
    //output.write(reinterpret_cast<const char *>(&data_size), sizeof(data_size));
}
