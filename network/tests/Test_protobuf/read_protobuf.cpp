// g++ -o read_protobuf read_protobuf.cpp message.pb.cc -pthread -lprotobuf -lpthread -g

#include <iostream>
#include <fstream>
#include <string>
#include "message.pb.h"

using namespace std;

int main(int argc, char *argv[])
{
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    // cl = client
    // se = server
    divfr::data sr_message;


    // Read the existing cl_message file
    fstream input("input/cl_message", ios::in | ios::binary);

    // std::vector<unsigned char> buffer1(std::istreambuf_iterator<char>(input), {});


    // Basically when you type cast from higher type to lower type truncation happens. i.e Loss of data happens
    // type order:
    //

    // string str;
    //str.data();
    //read functions keeps reading till # bytes have been read or end of file condition (sockets closes)
    //input.read(&str[0], 8);
    //read binary data

    //read binary data into size_t data_size. you must know how many bytes are written, we know, because we wrote the same size_t
    size_t data_size;
    input.read(reinterpret_cast<char *>(&data_size), sizeof(data_size));

    cout << "test data size: " << data_size << endl;

    //cout << byte << endl;

    //make this a temporary buffer till parse is done
    std::string data;
    data.resize(data_size);
    input.read((char*)data.data(),data_size);

    //char temp[data_size];
    //input.read(temp, data_size);

    if (!sr_message.ParseFromString(data) )
    {
        cerr << "Failed to parse address book." << endl;
        return -1;
    }

    /*
    //parse function must have exactly the bytes from te protobuf, otherwise it can parse it, if parser gets more bytes. protobuf does no inlcude delimantors or framing
    if (!sr_message.ParseFromIstream(&input))
    {
        cerr << "Failed to parse address book." << endl;
        return -1;
    }
*/
    std::cout << sr_message.age() << std::endl;
    std::cout << sr_message.img_fname() << std::endl;
    std::cout << sr_message.img_fformat() << std::endl;
    //std::cout << sr_message.image_data() << std::endl;

    //cout << "byte protobuf after read: " << sr_message.ByteSizeLong() << endl;

    //string to vector (binary conversion for protobuf)
    //std::vector<unsigned char> buffer(sr_message.image_data().begin(), sr_message.image_data().end());
    /*
    for (auto x : buffer)
        std::cout << signed(x);
*/
    // cout << "\n\n\n\n\n\n string:" << endl;

    // cout << sr_message.image_data() << endl;

    //get #bytes serialized messsage would take up
    //size_t size_sr_message = sr_message.ByteSizeLong();

    //cout << "Read " << size_sr_message << " bytes" << endl;

    std::ofstream output("output/out_apple.jpg", std::ios::binary);

    std::copy(
        sr_message.image_data().begin(),
        sr_message.image_data().end(),
        std::ostreambuf_iterator<char>(output));

    size_t data_size1;
    input.read(reinterpret_cast<char *>(&data_size1), 20000000);

    cout << "test data size: " << data_size1 << endl;

    size_t sss = 0;
    input.read(reinterpret_cast<char *>(&sss), 20000000);

    cout << "test data size: " << sss << endl;

        size_t xxx = 0;
    input.read(reinterpret_cast<char *>(&xxx), 20000000);

    cout << "test data size: " << xxx << endl;
}
