
Write application:
-----------------
Takes a .jpeg image as input and stores it as binary in memory.
Next it stores the binary in a protobuf message object with meta data (name image, format, size).
Next the protobuf message object is serialized to binary and stored in a file (cl_message.txt).

Read application:
-----------------
Read the cl_message and parses it into a protobuf message.
Display the protobuf message object member variables (name_image, format, size) an
recover the image and save it as out_apple.jpeg.

compile command message.protot
------------------------------
protoc -I="." --cpp_out="." <file_name>.proto

compile commands source files
-----------------------------
read_protobuf.cpp (write application):
g++ -I include -o read_protobuf read_protobuf.cpp src/message.pb.cc -pthread -lprotobuf -lpthread -g

write_protobuf.cpp (read application):
g++ -I include -o write_protobuf write_protobuf.cpp src/message.pb.cc -pthread -lprotobuf -lpthread -g

*compile in the folder where the src file is.