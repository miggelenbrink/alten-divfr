/* Acceptance test behavior message.hpp library
*  15-3-21
*
*/

//compile: g++ -I include -I /usr/local/boost_1_75_0 -o main main.cpp src/message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread -g -lcrypto -lssl

#include <iostream>
#include "../src/queue.hpp"
#include "../src/message.hpp"
#include "../src/connection.hpp"

using namespace std;

int main()
{

    message msg;
    queue<message> buf;
    connection conn("51.38.81.49", 80);

    msg.set_age(18);
    msg.set_img_name("name");
    msg.set_img_format("JPEG");
    msg.set_img_data("data");

    conn.send(msg);

    //debug purpose
    using namespace std::chrono_literals;
    std::this_thread::sleep_for(20000ms);

    return 0;
}
