#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <memory> // enable_shared_from_this<>
#include <string>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp> //boost::asio::ssl
#include <thread>
#include "queue.hpp"
#include "message.hpp"

class connection : public std::enable_shared_from_this<connection>
{
public:
    connection();
    connection(const std::string &ip_addr, const int &port);

    ~connection();
    void disconnect();

    void send(message &msg);
    void read();

    void readHeader();
    void writeHeader();

private:
    boost::asio::io_context _context;
    boost::asio::ip::tcp::socket _socket;
    boost::asio::ip::tcp::endpoint _endp;

    std::thread _thrContext;

    queue<message> _writeMsgQueue;
};


connection::connection() : _context(), _socket(_context)
{
}

connection::connection(const std::string &ip_addr, const int &port) : _socket(_context), _endp(boost::asio::ip::address::from_string(ip_addr), port)
{

    boost::asio::io_context::work idleWork(_context);
    _thrContext = std::thread([this]() { _context.run(); });

    _socket.async_connect(_endp, [this](std::error_code ec) { //pass this otherwise the lambda cannot acces member func/var of connection object
        if (!ec)
        {
            std::cout << "Connected" << std::endl;
        }
        else
        {
            std::cout << "Failed to connect" << ec.message() << std::endl;
        }

    });
}

connection::~connection()
{
    disconnect();
}

void connection::disconnect()
{
    // check if socket is connected
    if (_socket.is_open())
    {
        //synchronous function? blocking
        //_socket.close();
        //asynchronous function? non-blocking
        _context.post([this]() { _socket.close(); });
    }

    //stop running the context
    _context.stop();

    if (_thrContext.joinable())
    {
        _thrContext.join();
    }
}

void connection::readHeader()
{
    /*@to do
    * the client/server have only 1 kind of message, that is the protobuf message, see message.proto
    * in the future add more message types, create a enum with message types, in the header determine which message type you receive!
    * such that the server can sent a useful message to the client, and not the current protobuf message
    */
}

void connection::writeHeader()
{
    boost::asio::async_write(_socket, boost::asio::buffer(_writeMsgQueue.front().get_ptr_2_serialized_img_data(), _writeMsgQueue.front().size_header_body()),
                             [this](std::error_code ec, std::size_t /*not used, don't edit*/) {
                                 if (!ec)
                                 {  
                                     std::cout << "-- log: async_write() succesfull " <<std::endl;
                                     //message is sent, thus delete out queue
                                     _writeMsgQueue.pop_front();
                                     //check if all data is send, if not then send data in the queue, thus redo the function.
                                     if (!_writeMsgQueue.empty())
                                     {
                                         std::cout << "-- log: messagequeue not empty, do writeheader " <<std::endl;
                                         writeHeader();
                                     }
                                 }
                                 else
                                 {
                                     std::cout << "Failed async_write " << ec.message() << std::endl;
                                     /*@to do 
                                     * close socket
                                     * inform user
                                     * retry connection
                                     */
                                 }
                             });
                             
}

void connection::send(message &msg)
{
    //make message ready (serialization)
    msg.serialize();

    //place message into queue ready for transmission (copied see docs)(reference or acutal msg?)
    _writeMsgQueue.push_back(msg);

    //write header, takes all message from the queue and writes it to the endpoint
    writeHeader();
}

void connection::read()
{
    readHeader();
}

#endif //CONNECTION_HPP