#ifndef CONNECTION_HPPz
#define CONNECTION_HPPz

#include <iostream>
#include <memory> // enable_shared_from_this<>
#include <string>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp> //boost::asio::ssl functions
#include "queue.hpp"
#include "message.hpp"

//using boost::asio en dan boost::asio overal voor verwijderen = leesbaarheid

//make this class such that you can more than one message type, maybe protobuf has a feature, make this class more general such that you can handle more than one type of protobuf message


class connection : public std::enable_shared_from_this<connection>
{
public:
    connection();
    // connect immediatly to server ip:port = addr, port_num
    connection(const std::string &ip_addr, const int &port);
    ~connection();

    //boost::asio::ip::tcp::endpoint endpoint(boost::asio:ip::make_address("ip", ec), port);
    void connectToSever(const std::string &ip_addr, const int &port)
    {
        const boost::asio::ip::tcp::endpoint _endpoint(boost::asio::ip::address::from_string(ip_addr), port);

        boost::asio::async_connect(_socket, _endpoint, [this](std::error_code ec) {if (!ec){/*read header();*/} });

        // async_connect (socket, endpoint, connect_handler)
        //for the connect_handler we use a lambda function, the async_connect function itself passes an error code to the connect_handler.
        //boost::asio::async_connect(socket, _endpoint, [](std::error_code ec){if (!ec){/*read header();*/}});
    }

    void DisconnectServer()
    {
        // check if socket is connected
        if (_socket.is_open())
            //synchronous function? blocking
            //_socket.close();
            //asynchronous function? non-blocking
            _context.post([this]() { _socket.close(); });
    }

    //must msg outlive these operations?
    void send(message &msg)
    {
        //make message ready (serialization)
        msg.serialize();

        //place message into queue ready for transmission (copied see docs)(reference or acutal msg?)
        _writeMsgqueue.push_back(msg);

        //write header, takes all message from the queue and writes it to the endpoint
        writeHeader();
    }
    //write header
    //write body
    void read(/*message*/);
    //read header
    //read body and put in message queue

    void readHeader();
    void readBody();

    //for now we write the whole message in 0ne time!
    void writeHeader()
    {
        boost::asio::async_write(_socket,
                                 boost::asio::buffer(_writeMsgqueue.front().get_ptr_2_serialized_img_data(), _writeMsgqueue.front().size_header_body()),
                                 [this](std::error_code ec) {
                                     if (!ec)
                                     {
                                        //check if all data is send, if not then send data in the queue.
                                         if (!_writeMsgqueue.empty())
                                         {
                                             writeHeader();
                                         }
                                     }
                                     else
                                     {
                                         //@todo analyze ec message!
                                         std::cout << "error async_write file connection.hpp line 80" <<std::endl;
                                         //@todo _socket.close();
                                     } 
                                 });
    }

    void writeBody();

    void AddMessageToqueue();
    void getMessageFromqueue();

private:
    boost::asio::ip::tcp::socket _socket;
    boost::asio::io_context _context;
    boost::asio::ip::tcp::endpoint _endp;

    //boost::asio::ssl::context _ctx;

    // asio context

    // TLS socket

    // queue with message to be sent to the server
    queue<message> _writeMsgqueue;

    // queue with message received from the server

    // temporary buffer with received data, received data is parsed into messages and pushed to the message queue
};

//const boost::asio::ip::address &addr, unsigned short port_num)

connection::connection() : _socket(_context)
{
}

connection::connection(const std::string &ip_addr, const int &port) : _socket(_context), _endp(boost::asio::ip::address::from_string(ip_addr), port)
{
    //try to connect to the endpoint
    boost::asio::async_connect(_socket, _endp, [this](std::error_code ec) {if (!ec){/*read header();*/} });
}

connection::~connection()
{
}

#endif //CONNECTION_HPPz