#ifndef CONNECTION_HPP
#define CONNECTION_HPP

#include <iostream>
#include <memory> // enable_shared_from_this<>
#include <string>
#include <functional>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp> //boost::asio::ssl functions
#include "queue.hpp"
#include "message.hpp"


#endif //CONNECTION_HPP