#ifndef MESSAGE_HPP
#define MESSAGE_HPP

#include <iostream>
#include <vector>
#include "message.pb.h"

//????? add mutexes because a client can use message asynchronous for write and read at the same time? yes
// to do add mutexes

class message
{
public:
    message();
    ~message();

    void set_age(const int &age);
    void set_img_name(const std::string &name);
    void set_img_format(const std::string &format);
    void set_img_data(const std::string &data);
    void clear();
    //each time the message is changed, recalcualte the header size! by function call set_header()
    size_t set_header();
    //size in bytes, 
    size_t size_header_body();
    size_t size_header();
    size_t size_body();

    void serialize();
    char *get_ptr_2_serialized_img_data();

    //function that writes message to outputstream?

private:
    size_t _header;
    divfr::data _body;
    //delete on memory you did not allocate or deleted before is undefined.  there initilize with nullptr
    //@todo change into vector or string
    //char *tmpdata;
    std::string tmpdata;

};

message::message() : _header{0}
{
}

message::~message()
{
    _body.Clear();
}

void message::clear()
{
    _body.Clear();
}

void message::set_age(const int &age)
{
    _body.set_age(age); 
}
void message::set_img_name(const std::string &name)
{
    _body.set_img_fname(name);
}
void message::set_img_format(const std::string &format)
{
    _body.set_img_fformat(format);
}
void message::set_img_data(const std::string &data)
{
    _body.set_image_data(data);
}

size_t message::set_header()
{
    _header = size_body();
    return _header;
}

size_t message::size_header_body()
{
    return _body.ByteSizeLong() + sizeof(_header);
}

size_t message::size_header()
{
    return sizeof(_header);
}

size_t message::size_body()
{
    return _body.ByteSizeLong();
}

void message::serialize()
{
    const size_t header_size = size_header();
    const size_t body_size = size_body();
    const size_t b_h_size = size_header_body();
    //resize buffer (serialized data is stored in the buffer)
    tmpdata.clear();
    tmpdata.resize(b_h_size);
    //tmpdata = new char[b_h_size];

    //do a check @todo replace with assert?
    if (b_h_size != (header_size + body_size))
    {
        std::cerr << "FAILED in message.hpp line 87: size conflict " << std::endl;
    }

    //write header into temp
    tmpdata.assign(reinterpret_cast<const char *>(&header_size), sizeof(header_size));
    //mempcpy(tmpdata.data(), reinterpret_cast<const char *>(&header_size), sizeof(header_size));
   
    //write body into temp
    //if (!_body.SerializeToArray(&tmpdata[0 + header_size], body_size))
    if (!_body.SerializeToArray((tmpdata.data()+header_size), body_size))
    {
        std::cerr << "FAILED to serialize message body in message.hpp line 93" << std::endl;
    }

/*  debug purpose, print out what is stored on the tmpdata_ memory location
    //--------------------------------------------------------------------------------------
    for (int i = 0; i < b_h_size; i++)
    {
        printf("%c ", tmpdata[i]);
    }
    std::cout << std::endl;

    std::cout << "print protobuf memory as hex" << std::endl;
    for (int i = 0; i < b_h_size; i++)
    {
        printf("%x ", tmpdata[i]);
    }
    std::cout << std::endl;
    //--------------------------------------------------------------------------------------
*/

}

char *message::get_ptr_2_serialized_img_data()
{
    return tmpdata.data();
}

#endif //MESSAGE_HPP