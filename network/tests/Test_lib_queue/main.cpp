
#include <iostream>

/* Acceptance test behavior queue.hpp library
*  14-3-21
*/

// g++ main.cpp -o main -std=c++17

#include "queue.hpp"

int main()
{
    std::string message;
    message = "test";

    queue<std::string> buff_queue;

    std::cout << "empty:"<< buff_queue.empty() << std::endl;

    std::cout << buff_queue.size() << std::endl;

    buff_queue.push_back("hallo");
    buff_queue.push_back("Guido");
    buff_queue.push_back("Sophie");
    buff_queue.push_back(message);

    std::cout << buff_queue.size() << std::endl;
    std::cout << buff_queue.pop_front() << std::endl;
    std::cout << buff_queue.pop_back() << std::endl;
    std::cout << buff_queue.size() << std::endl;
    std::cout << buff_queue.empty() << std::endl;

    buff_queue.clear();

    std::cout << buff_queue.empty() << std::endl;


    //segentation fault becasue trying to pop form the queue while empty
    std::cout << buff_queue.pop_front() << std::endl;
    std::cout << buff_queue.front() << std::endl;

    std::cout << buff_queue.front() << std::endl;

    return 0;
}
