/* Acceptance test behavior message.hpp library
*  15-3-21
*
*/

//compile: g++ -I include -o main main.cpp src/message.pb.cc -pthread -lprotobuf -lpthread -g

#include <iostream>
#include "include/message.hpp"

using namespace std;

int main()
{

    message msg;

    msg.set_age(18);
    msg.set_img_name("name");
    msg.set_img_format("JPEG");
    msg.set_img_data("data");

    msg.set_header();

    cout << "header size size_header()     :" << msg.size_header() << endl;
    cout << "header size set_header()      :" << msg.set_header() << endl;
    cout << "header size size_header()     :" << msg.size_header() << endl;
    cout << "body/payload size size_body() :" << msg.size_body() << endl;

    msg.serialize();

    char *ptr_body_data;
    ptr_body_data = msg.get_ptr_2_serialized_img_data();

    std::cout << "print protobuf memory as char/text" << std::endl;

    for (int i = 0; i < msg.size_header_body(); i++)
    {
        printf("%c ", ptr_body_data[i]);
    }
    std::cout << std::endl;

    std::cout << "print protobuf memory as hex" << std::endl;
    for (int i = 0; i < msg.size_header_body(); i++)
    {
        printf("%x ", ptr_body_data[i]);
    }
    std::cout << std::endl;

    msg.clear();

    msg.set_header();

    cout << "header size size_header()     :" << msg.size_header() << endl;
    cout << "header size set_header()      :" << msg.set_header() << endl;
    cout << "header size size_header()     :" << msg.size_header() << endl;
    cout << "body/payload size size_body() :" << msg.size_body() << endl;

    msg.serialize();

    ptr_body_data = msg.get_ptr_2_serialized_img_data();

    std::cout << "print protobuf memory as char/text" << std::endl;

    for (int i = 0; i < msg.size_header_body(); i++)
    {
        printf("%c ", ptr_body_data[i]);
    }
    std::cout << std::endl;

    std::cout << "print protobuf memory as hex" << std::endl;
    for (int i = 0; i < msg.size_header_body(); i++)
    {
        printf("%x ", ptr_body_data[i]);
    }
    std::cout << std::endl;

    return 0;
}
