#include <cstdlib>
#include <functional>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

using boost::asio::ip::tcp;

// https://quuxplusone.github.io/blog/2020/01/26/openssl-part-3/
// https://m.heise.de/developer/artikel/SSL-TLS-Netzwerkprogrammierung-mit-Boost-Asio-Teil-2-Server-Programmierung-3102667.html?seite=all

// g++ -I /usr/local/boost_1_75_0 server.cpp -o server -std=c++17 -pthread -lcrypto -lssl -lprotobuf


class session : public std::enable_shared_from_this<session>
{
public:
  session(boost::asio::ssl::stream<tcp::socket> socket)
      : socket_(std::move(socket))
  {
  }

  void start()
  {
    do_handshake();
  }

private:
  void do_handshake()
  {
    auto self(shared_from_this());
    // do a handshake, if handshake passes then start reading
    socket_.async_handshake(boost::asio::ssl::stream_base::server,
                            [this, self](const boost::system::error_code &error) {
                              if (!error)
                              {
                                do_read();
                              }
                            });
  }

  void do_read()
  {
    auto self(shared_from_this());
    //echo read data
    socket_.async_read_some(boost::asio::buffer(data_),
                            [this, self](const boost::system::error_code &ec, std::size_t length) {
                              if (!ec)
                              {
                                do_write(length);
                              }
                            });
  }

  void do_write(std::size_t length)
  {
    auto self(shared_from_this());
    boost::asio::async_write(socket_, boost::asio::buffer(data_, length),
                             [this, self](const boost::system::error_code &ec,
                                          std::size_t /*length*/) {
                               if (!ec)
                               {
                                 do_read();
                               }
                             });
  }
  // SSL socket
  boost::asio::ssl::stream<tcp::socket> socket_;
  char data_[1024];
};

class server
{
public:
  server(boost::asio::io_context &io_context, unsigned short port)
      : acceptor_(io_context, tcp::endpoint(tcp::v4(), port)),
        //set protocol versions that may be used sslv23= TLS1.2 or TLS 1.3
        context_(boost::asio::ssl::context::sslv23)
  {
    //st options, study them!
    //default_workarounds biedt tijdelijke ondersteuning voor bekende SSL-bugs. 
    /*
    no_sslv2 schakelt ondersteuning uit voor het verouderde en niet langer veilige SSLv2. 
    single_dh_use genereert een nieuwe sleutel voor elke Diffie-Hellman-uitwisseling . 
    Verdere opties zijn te vinden in de Boost-documentatie .
    empharal keypair, geeft forward secrecy!, if client and server decide to use DH. ciphersuites may use ephemeral Diffie-Hellman (DH) key exchange.
    */
    context_.set_options(
        boost::asio::ssl::context::default_workarounds | 
        boost::asio::ssl::context::no_sslv2 |
       // /Always create a new key when using tmp_dh parameters. = empahral key pair, always new keypair on new connection = forward secrecy
        boost::asio::ssl::context::single_dh_use);

    // delete password?!
    context_.set_password_callback(std::bind(&server::get_password, this));

    // load the certificates! we only use one certificate, it is not really a chain!
    context_.use_certificate_chain_file("server.crt");

    context_.use_private_key_file("server.key", boost::asio::ssl::context::pem);

    //For DH generating parameters is a time consuming process and so servers allow an external file to load DH parameters from
    //context_.use_tmp_dh_file("dh2048.pem");

    do_accept();
  }

  tcp::acceptor acceptor_;
  //create SSL context
  boost::asio::ssl::context context_;

private:
  std::string get_password() const
  {
    return "test";
  }

  void do_accept()
  {
    acceptor_.async_accept(
        [this](const boost::system::error_code &error, tcp::socket socket) {
          if (!error)
          {
            std::make_shared<session>(
                boost::asio::ssl::stream<tcp::socket>(
                    std::move(socket), context_))
                ->start();
          }

          do_accept();
        });
  }
};

int main(int argc, char *argv[])
{
  try
  {
    if (argc != 2)
    {
      std::cerr << "Usage: server <port>\n";
      return 1;
    }

    boost::asio::io_context io_context;

    using namespace std; // For atoi.
    server s(io_context, atoi(argv[1]));

    io_context.run();
  }
  catch (std::exception &e)
  {
    std::cerr << "Exception: " << e.what() << "\n";
  }

  return 0;
}