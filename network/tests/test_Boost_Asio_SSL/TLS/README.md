## OpenSSL command line interface to generate keypairs

we generate a key pair using P-256 elliptic curve algorithm: 

openssl ecparam -genkey -name prime256v1 -noout -out <fname_private_key>.pem
openssl ec -in server-private-key.pem -pubout -out <fname_public_key>.pem

We used:
openssl ecparam -genkey -name prime256v1 -noout -out server-private-key.pem
openssl ec -in server-private-key.pem -pubout -out server-public-key.pem

Next we create we create a certificate. It is a self signed certificate by the server, with the server's private key.

openssl req -new -x509 -sha256 -key <fname_private_key>.pem -subj "/CN=Description" -out <fname_certificate>.pem

we used:
openssl req -new -x509 -sha256 -key server-private-key.pem -subj "/CN=DIVFR_server" -out server-certificate.pem

import the keypaoir and certificate into the SSL_CTX context.  



### Handy resources:
https://quuxplusone.github.io/blog/2020/01/26/openssl-part-3/  
https://m.heise.de/developer/artikel/SSL-TLS-Netzwerkprogrammierung-mit-Boost-Asio-Teil-2-Server-Programmierung-3102667.html?seite=all  

notes:  
Recall that before we can create an SSL connection, we need to fill out an SSL_CTX. On the server side, the SSL_CTX holds the server’s certificate and private key, so that the server can authenticate itself to clients. On the client side, the SSL_CTX holds a trust store — a set of certificates that our client considers trustworthy.
https://quuxplusone.github.io/blog/2020/01/27/openssl-part-4/
Our trivial HTTPS server merely presents its certificate to the client and waits to see if the client accepts it. This is a non-interactive process from the programmer’s point of view. We just set up the server’s SSL_CTX with the appropriate certificate and private key, and then we could immediately call BIO_read on the SSL BIO; the process of presenting our certificate and negotiating encryption parameters all happened “under the hood.” If the client happens to reject our certificate, no problem — BIO_read returns zero bytes, we drop the connection and move on to service the next client.
A single certificate may be valid for many different domain names. One of those names occupies a privileged position known as the “Common Name” (CN); the rest must be relegated to an extension field called “Subject Alternative Names” (SAN). However, because storing names in two different places is a pain in the neck, the industry has essentially deprecated the “Common Name” field in favor of putting all the names in the SAN field.

wat betekdn dit?
ssl::rfc2818_verification: This class is the easy way to verify a
certificate against a hostname according to the rules from RFC 2818
verwerken in verslag


default_workarounds biedt tijdelijke ondersteuning voor bekende SSL-bugs. no_sslv2 schakelt ondersteuning uit voor het verouderde en niet langer veilige SSLv2. single_dh_use genereert een nieuwe sleutel voor elke Diffie-Hellman-uitwisseling . Verdere opties zijn te vinden in de Boost-documentatie .

he one thing to really watch out for — and this bit me multiple times during the writing of this series — is that integer 0 argument to BIO_new_ssl. It means “act like a server.” If you put a 1 there instead, it means “act like a client.” The TLS protocol is not symmetrical! If you write 0 when you mean 1, or 1 when you mean 0, your code will probably just hang, or error out with some cryptic message if you’re lucky. Be very careful when cutting-and-pasting between examples!

OpenSSL doesn’t provide any scalable or secure way to import certificate or key data from memory; it wants everything as paths to disk files. Not the most secure approach in the world; but suitable for our purposes.