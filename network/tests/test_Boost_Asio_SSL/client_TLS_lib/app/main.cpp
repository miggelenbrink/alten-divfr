/** @file main.cpp
* Client application with divfr_TLS_client library.
*
* The client application opens an image and sends it to the server.
*
* @author <name>
* @date   22-03-21
* 
* @note compile: 
*       g++ -I include -I /usr/local/boost_1_75_0 -o main main.cpp src/message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread -g -lcrypto -lssl 
*
* @todo 
*/

#include <iostream>
#include "../include/common.hpp"
#include "../include/c_message.hpp"
#include "../include/queue.hpp"
#include "../include/connection.hpp"
#include "../include/client_interface.hpp"
#include <fstream> //delete in release, test purpose for input image

#define IN_PATH "/home/guido/Desktop/DIVFR/network/tests/client_TLS_lib/app/apple.jpg"

/* 
* The client class is the client application.
* The client inherits from the client_interface, 
* and the client_interface is the divfr_TLS_client library API.
* 
* The client_interface has callback functions, which can be set:
*   set_connErrCallback:      callback is triggerend when the connect() or reconnect() function fails to connect to a server.
*   set_writeErrCallback:     callback is triggerend when asyn_read fails
*   set_readErrCallback:      callback is triggerend when async_write fails
*   set_dissConnectCallback:  callback is triggerend when there is no socket is open anymore! thus the server is closed/offline.
* Do change the default behavior!!
*/
class client : public client_interface
{
private:
    c_message msg1;

public:
    bool Exit_FLAG;

public:
    client() : Exit_FLAG(false)
    {
    }

    ~client()
    {
    }

    void connect_to_server(const std::string ip, const std::string port)
    {
        connect(ip, port);
    }

    void load_img()
    {
        // open a file as binary
        std::ifstream file(IN_PATH, std::ios::binary);
        // store the file in string
        const std::string str(std::istreambuf_iterator<char>(file), {});

        // set data in message template
        msg1.set_age(18);
        msg1.set_img_fname("test_name");
        msg1.set_img_fformat("format_test");
        msg1.set_image_data(str);
    }

    void console()
    {
        char c_input;
        while (c_input != 'q')
        {
            std::cout << "[console] press 'q' to exit the application" << std::endl;
            std::cout << "[console] press 'e' to send empty message" << std::endl;
            std::cout << "[console] press 's' to send the test message" << std::endl;
            std::cout << "[console] press 'c' to connect the server" << std::endl;
            std::cout << "[console] press 'd' to disconnect from the server" << std::endl;

            std::cin >> c_input;
            switch (c_input)
            {
            case 's':
            {
                load_img();
                send(msg1.get_protoMsg1Struct());
                break;
            }
            case 'e':
            {
                c_message msg1;
                msg1.set_protoMsg1Struct();
                send(msg1.get_protoMsg1Struct());
                break;
            }
            case 'd':
            {
                disconnect();
                break;
            }
            case 'c':
            {
                connect_to_server("127.0.0.1", "6000");
                break;
            }
            case 'q':
            {
                Exit_FLAG = true;
                std::cout << "[console] bye" << std::endl;
                break;
            }
            default:
            {
                std::cout << "[console] key not recognised, use the keys 'd', 's' or 'q' " << std::endl;
                break;
            }
            }
        }
    }

    //check if new messages have been received? and do something
    void update()
    {
        while (!get_msgInQueu().empty())
        {
            auto msg = get_msgInQueu().pop_front();

            switch (msg.id)
            {
            case messageTypes::protoMsg1:
            {
                c_message msgIn;
                msgIn.parse_protoMsg1Struct(msg);

                std::cout << msgIn.age() << std::endl;
                std::cout << msgIn.img_fname() << std::endl;
                std::cout << msgIn.img_fformat() << std::endl;
                std::cout << msgIn.image_data() << std::endl;

                break;
            }
            case messageTypes::protoMsg2:
            {
                std::cout << "HOE DAN, Dit bericht bestaat nog niet" << std::endl;
                break;
            }
            default:
            {
                break;
            }
            }
        }
    }
};

int main()
{
    client client_;

    // create thread to processes the console/user interaction
    auto thread_console = std::thread([&]() {
        while (!client_.Exit_FLAG)
        {
            client_.console();
            std::cout << "[main] close console_thread" << std::endl;
        }
    });

    // get received message and call onMessage callback to handle the msg
    while (!client_.Exit_FLAG)
    {
        client_.update();
    }

    if (thread_console.joinable())
    {
        thread_console.join();
    }

    std::cout << "[main] exit application, bye..." << std::endl;
    return 0;
}
