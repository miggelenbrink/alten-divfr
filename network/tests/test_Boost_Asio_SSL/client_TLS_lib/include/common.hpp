/** @file common.hpp
* common structure and enum for the client and server used for message exchange. 
*
* @author <name>
* @date   22-03-21
*
* @todo 
*/

#ifndef COMMON_HPP
#define COMMON_HPP

#include <iostream>

/**
* Enum class that stores error codes which are returned by the onError function.
*
* connErr:      callback is triggerend when the connect() or reconnect() function fails to connect to a server.
* writeErr:     callback is triggerend when asyn_read fails
* readErr:      callback is triggerend when async_write fails
* dissConnect:  callback is triggerend when there is no socket is open anymore! thus the server is closed/offline
*/
enum errorCodes
{
    default_ERR,
    socket_is_open_ERR,
    write_ERR,
    read_ERR,
    connection_ERR,
};

/*
*   Enum messageTypes, message types used in message exchange client and server.
*
*   Contains all possible message types(classes) which the client can use to send to a server.
*   A user can create a new message type by generating a protobuf c++ class and wrap it into 
*   the c_message class. Subsequently the user must manualy add the created message type here 
*   in the Enum. The message type is used as an ID number and send along with the data.Because 
*   of the ID(messageType) the server knows how to parse the received data. Therefore it is 
*   IMPORTANT that the message types here are equal to and have the same order as the message 
*   types in the divfr_TLS_server library Enum.
*   
*   - id/type protoMsg1 stands for messages of the type(class) c_message
*   - id/type <ID_name> stands for messages of the type(class) <class_name>
*/
enum messageTypes
{
    defaultMsg,
    protoMsg1,
    protoMsg2,
};

/*
*   Structure to exchange messages between client and server.
*   
*   This structure is used to exchange messages between a client and server. Because it makes 
*   message independent of their type. Therefore it is possible to exchange messages of any type
*   with this structure. The client/server creates a message with as type one of the messageTypes.
*   The client/server must serialize/parse the message before sending/reading a message. 
*   The serialize/parse functionality is in the client_interface class. Message that are serialized are 
*   stored in a msg_struct instance and the message structure is used to write data to a socket-stream.
*   Data that is received on a socket-stream is stored into a msg_struct and the client/server can parse 
*   the msg_struct into the corresponding message object(type, one of the messageTypes).
*/
struct msg_struct
{
    // The header stores the payload size in bytes.
    size_t header;
    // The id indicates the message type (type of the message object the serialized payload must be parsed into)
    messageTypes id;
    // All data in binary format, used to send/receive over socket-streams
    std::string id_serialized;
    std::string header_serialized;
    std::string payload_serialized;
};

#endif //COMMON_HPP