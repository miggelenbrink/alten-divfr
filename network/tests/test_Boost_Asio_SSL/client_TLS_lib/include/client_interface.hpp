/** @file client_interface.cpp
* client_interface class.
*
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*
* @author <name>
* @date   22-03-21
*
* @todo create function that can open a socket again and connecxt to server, in progress. // void client_interface::reopen_socket_and_connect_to_server(const std::string _ip, const short _port)
*/

#ifndef CLIENT_INT_HPP
#define CLIENT_INT_HPP

#include <iostream>
#include "common.hpp"
#include "c_message.hpp"
#include "queue.hpp"
#include "connection.hpp"
//
#include <cstdlib>
#include <cstring>
#include <functional>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

/* 
* The client_interface class.
* The client interface provides a user with the 
* divfr_TLS_client library functionality.
*/
class client_interface
{
public:
    /**
    * Constructor. 
    * Creates and initializes an io_contet, tcp::ip::socket and
    * a thread that runs the io_context to handle async boost::asio functions.
    */
    client_interface();
    /**
    * Deconstructor. 
    * Stops the running io_context, joins the io_context thread and destory the client_interface.
    */
    ~client_interface();

    /**
    * Disconnect from server. 
    * Stops the running io_context, joins the io_context thread
    * and destory the connection object.
    */
    void disconnect();

    /**
    * Connect to a server. 
    * @param _ip The server's IP address. 
    * @param _port The port # the server listens on for new connections.
    * @return true on succesfully connected, false on failed to connect.
    */
    bool connect(const std::string _ip, const std::string _port);

    /**
    * A
    */
    bool is_connected();

    /**
    * send msg_struct message to the server.
    * The send function places the message into a msgOut queue and the connection object writes messages in the msgOut queue on the socket stream.
    * @param msg A msg_struct instance.
    */
    void send(msg_struct msg);

    /**
    * A
    */
    queue<msg_struct> &get_msgInQueu();



protected:
    /**
    * A
    */
    virtual void onErrorFromSession(errorCodes ec);

private:
public:
    // Buffer for messages read by the connection object which are ready to be processed by the client application.
    queue<msg_struct> _msgInQueue;

    boost::asio::io_context _io_context;
    boost::asio::ssl::context _ssl_context;

    //boost::asio::ssl::stream<boost::asio::ip::tcp::socket> _ssl_socket;

    std::thread _threadContext;
    std::unique_ptr<connection> _connectionSession;
    std::string _port_;
    std::string _ip_;
};

#endif //CLIENT_INT_HPP