#include "../include/c_message.hpp"

c_message::c_message(messageTypes id) : protoMsg1Struct{0, id}
{
}

c_message::~c_message()
{
}

size_t c_message::sizeof_id_type()
{
    return sizeof(protoMsg1Struct.id);
}

size_t c_message::sizeof_header()
{
    return sizeof(protoMsg1Struct.header);
}

bool c_message::parse_protoMsg1Struct(msg_struct data)
{
    if (!ParseFromString(data.payload_serialized))
    {
        std::cout << "[CLIENT c_message.hpp parseprotoMsg1Struct()] ParseFromString() failed " << std::endl;
        return false;
    }
    else
    {
        std::cout << "[CLIENT c_message.hpp parseprotoMsg1Struct()] ParseFromString() succesfully " << std::endl;
        return true;
    }
}

void c_message::set_protoMsg1Struct()
{
    //make const if everything works fine
    size_t sizeofHeader = sizeof_header();
    size_t sizeof_id = sizeof_id_type();

    protoMsg1Struct.header = ByteSizeLong();
    // allocate space for serialized c_message
    protoMsg1Struct.payload_serialized.resize(protoMsg1Struct.header);

    protoMsg1Struct.header_serialized.resize(sizeofHeader);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.header_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.header)), sizeof(protoMsg1Struct.header));

    protoMsg1Struct.id_serialized.resize(sizeof_id);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.id_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.id)), sizeof(protoMsg1Struct.id));

    if (!SerializeToString(&protoMsg1Struct.payload_serialized))
    {
        std::cerr << "[ERROR] FAILED to serialize c_message.hpp object" << std::endl;
    }
}

msg_struct c_message::get_protoMsg1Struct()
{
    //optional, but easy to forget to do a set_protoMsg1Struct() before calling get_protoMsg1Struct() otherwise the data is not updated into the struct
    set_protoMsg1Struct();
    return protoMsg1Struct;
}

std::string c_message::get_serialized_id()
{
    return protoMsg1Struct.id_serialized;
}

std::string c_message::get_serialized_header()
{
    return protoMsg1Struct.header_serialized;
}

std::string c_message::get_serialized_payload()
{
    return protoMsg1Struct.payload_serialized;
}