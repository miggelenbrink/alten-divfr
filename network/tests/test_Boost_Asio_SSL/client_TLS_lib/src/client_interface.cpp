#include "../include/client_interface.hpp"

client_interface::client_interface() : _ssl_context(boost::asio::ssl::context::sslv23)//, _ssl_socket(_io_context, _ssl_context)
{
 _ssl_context.load_verify_file("/home/guido/Desktop/DIVFR/network/tests/client_TLS_lib/certificate/rootca.crt");
}

client_interface::~client_interface()
{
    if (_threadContext.joinable())
        _threadContext.join();

    disconnect();

    std::cout << "[CLIENT] Client closed, deconstructor client_interface" << std::endl;
}

void client_interface::disconnect()
{
    // destroy connection/session object

    if (_connectionSession)
    {
        _connectionSession->disconnect();
        _connectionSession.reset();
    }

    if (_msgInQueue.empty())
        _msgInQueue.clear();

    if (!_io_context.stopped())
        _io_context.stop();

    std::cerr << "[CLIENT] disconnected from server" << std::endl;
}

bool client_interface::connect(const std::string _ip, const std::string _port)
{
    //cache ip and port, for reconnect, if connect failes
    _ip_ = _ip;
    _port_ = _port;
    try
    {
        // Create a connection and socket object and pass the socket to the connectoin object
        if (!_connectionSession)
        {
            _connectionSession = std::make_unique<connection>(_io_context, _ssl_context, _msgInQueue, [this](errorCodes erc) { this->onErrorFromSession(erc); });

            // Create boost::asio::endpoint
            //auto _endpoint = boost::asio::ip::tcp::endpoint(boost::asio::ip::address::from_string(_ip), _port);
            boost::asio::ip::tcp::resolver resolver(_io_context);
            auto _endpoint = resolver.resolve(_ip_, _port_);

            // Make connection object and start reading
            _connectionSession->connect_to_server(_endpoint);

            if (_io_context.stopped())
                _io_context.restart();

            // Run the _io_context in a thread, the _io_context handles all function called on the _socket instance.
            if (_threadContext.joinable())
                _threadContext.join();

            _threadContext = std::thread([this]() { _io_context.run(); });
        }
        else
        {
            std::cerr << "[CLIENT client_interface.hpp] Do a disconnect before a new connect" << std::endl;
            return false;
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "[CLIENT client_interface.hpp] failed to connect to the server: " << e.what() << std::endl;
        onErrorFromSession(connection_ERR);

        return false;
    }
    return true;
}

bool client_interface::is_connected()
{
    if (_connectionSession)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void client_interface::send(msg_struct msg)
{
    if (_connectionSession)
    {
        if (_connectionSession->is_socket_open())
        {
            // async send
            _io_context.post([this, msg]() {
                //if the writeprocess, write_header, write_id etc is busy then do not start a new one! write process will write the whole buffer, this anyway the message will be sent.
                bool WriteInprocess = _connectionSession->msgOutBuf.empty();

                // Write message into the outgoing message queue (buffer).
                _connectionSession->msgOutBuf.push_back(msg);

                // Tell the connection object to write all message in the message queue to the socket stream/server.
                if (WriteInprocess)
                    _connectionSession->start_write();
            });
        }
        else
        {
            std::cout << "[CLIENT] client_interface: send failed because socket to server is not open " << std::endl;
            onErrorFromSession(errorCodes::socket_is_open_ERR);
        }
    }
    else
    {
        std::cout << "[CLIENT] client_interface: send failed because there is no client connected " << std::endl;
    }
}

queue<msg_struct> &client_interface::get_msgInQueu()
{
    return _msgInQueue;
}

void client_interface::onErrorFromSession(errorCodes ec)
{
    std::cout << "[CLIENT] Default onErrorFromSession()" << std::endl;
    switch (ec)
    {
    case errorCodes::connection_ERR:
        std::cout << "[CLIENT] Errorcode: connection_ERR, do a reconnect (not active in release) " << std::endl;
        //must do a disconnect to clean up the failed connect try
        disconnect();

        ///@todo add a deadline, that after 3 attempts this function quits! and control goes back to the console
        // otherwise reconnect will spam
        // using namespace std::chrono_literals;
        // std::this_thread::sleep_for(4000ms);
        // connect(_ip_, _port_);

        break;
    case errorCodes::socket_is_open_ERR:
        std::cout << "[CLIENT] Errorcode: socket_is_open_ERR, probably/assume the server is disconnected" << std::endl;
        disconnect();

        break;
    case errorCodes::read_ERR:
        std::cout << "[CLIENT] Errorcode: read_ERR, probably/assume the server is disconnected" << std::endl;
        disconnect();

        break;
    case errorCodes::write_ERR:
        std::cout << "[CLIENT] Errorcode: write_ERR" << std::endl;

        // don't try to acces the pointer if it points to the unkown.
        if (_connectionSession)
        {
            //if you don't do a disconnect_client(), then manualy delete the message that caused the error from the msgOutBuf! because alsong the msg is in the buffer it will probably gerenate te same error on a next write.

            if (!_connectionSession->msgOutBuf.empty())
                _connectionSession->msgOutBuf.pop_front();

            if (!_connectionSession->msgOutBuf.empty())
                _connectionSession->start_write();
        }

        break;
    default:
        std::cout << "[CLIENT] Errorcode: error code does not exists..." << std::endl;

        break;
    }
}

