#ifndef S_MESSAGE_HPP
#define S_MESSAGE_HPP

#include <iostream>
#include <vector>
#include "message.pb.h"
#include "common.hpp"

/*
* delete final keyword in protobuf classes!
*/

/*
*  s_message type: protoMsg1
*/
class s_message : public divfr::data
{
public:
    s_message(messageTypes id = messageTypes::protoMsg1);
    ~s_message();

    //total size of data in the msg_struct
    size_t size_header_id_payload();
    // size of the header variable in bytes
    size_t sizeof_header();
    // size of the id variable in bytes
    size_t sizeof_id();
    // size of the payload variable in bytes
    size_t size_payload_serialized();

    // input: a s_message struct
    // reconstruct protobuf s_message
    // return true on sucesfull, fail on error
    bool parseprotoMsg1Struct(msg_struct data);

    // place protobuf data in the msg_stuct object
    void setprotoMsg1Struct();
    // get msg_struct object, always perform a set msg_struct before a getprotoMsg1Struct()
    msg_struct getprotoMsg1Struct();

    // get serialized id. for transmission over a socket stram
    // get serialized header. for transmission over a socket stram
    // get serialized payload. for transmission over a socket stram
    std::string get_serialized_id();
    std::string get_serialized_header();
    std::string get_serialized_payload();

private:
    msg_struct protoMsg1Struct;
};

s_message::s_message(messageTypes id) : protoMsg1Struct{0, id}
{
}

s_message::~s_message()
{
}

size_t s_message::size_header_id_payload()
{
    return ByteSizeLong() + sizeof(protoMsg1Struct.header) + sizeof(protoMsg1Struct._id);
}

size_t s_message::sizeof_id()
{
    return sizeof(protoMsg1Struct._id);
}

size_t s_message::sizeof_header()
{
    return sizeof(protoMsg1Struct.header);
}

size_t s_message::size_payload_serialized()
{
    return protoMsg1Struct.payload_serialized.size();
}

bool s_message::parseprotoMsg1Struct(msg_struct data)
{
    if (!ParseFromString(data.payload_serialized))
    {
        std::cerr << "[ERROR] FAILED to parse msg_struct into a s_message.hpp object" << std::endl;
        return false;
    }
    else
    {
        return true;
    }
}

void s_message::setprotoMsg1Struct()
{
    //make const if everything works fine
    size_t sizeofHeader = sizeof_header();
    size_t sizeofId = sizeof_id();

    protoMsg1Struct.header = ByteSizeLong();
    // allocate space for serialized s_message
    protoMsg1Struct.payload_serialized.resize(protoMsg1Struct.header);

    protoMsg1Struct.header_serialized.resize(sizeofHeader);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.header_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct.header)), sizeof(protoMsg1Struct.header));

    protoMsg1Struct.id_serialized.resize(sizeofId);
    // write header variable as binary into headerIdPayload
    protoMsg1Struct.id_serialized.assign(reinterpret_cast<const char *>(&(protoMsg1Struct._id)), sizeof(protoMsg1Struct._id));

    if (!SerializeToString(&protoMsg1Struct.payload_serialized))
    {
        std::cerr << "[ERROR] FAILED to serialize s_message.hpp object" << std::endl;
    }
}

msg_struct s_message::getprotoMsg1Struct()
{
    //optional, but easy to forget to do a setprotoMsg1Struct before calling getprotoMsg1Struct
    //setprotoMsg1Struct();
    return protoMsg1Struct;
}

std::string s_message::get_serialized_id()
{
    return protoMsg1Struct.id_serialized;
}

std::string s_message::get_serialized_header()
{
    return protoMsg1Struct.header_serialized;
}

std::string s_message::get_serialized_payload()
{
    return protoMsg1Struct.payload_serialized;
}

#endif //S_MESSAGE_HPP