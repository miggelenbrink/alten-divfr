#ifndef COMMON_HPP
#define COMMON_HPP

#include <iostream>

enum messageTypes 
{                         //CLASS/TYPE to identified with "messageType"
    protoMsg1,  // is type [class message : public divfr::data class message] 
    protoMsg2,  // is type <>
    protoMsg3,  // is type <>
};

struct msg_struct
{
    // variable header contains the size of payload(size of the protobuf serialized data)
    size_t header;
    //make _id const if everything works
    messageTypes _id;
    // serialized data to transmit over socket stream
    std::string id_serialized;
    std::string header_serialized;
    std::string payload_serialized;
};

#endif //COMMON_HPP