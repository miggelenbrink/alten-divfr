#ifndef QUEUE_HPP
#define QUEUE_HPP

/*
*   thread save queue, because the queue can be accesed by more than one object simultatoulsy.
*   The connection and client object work both on the queue
*
*   wrap vector function with mutexes
*
*   @take care of segmentation fautls, do a empty() check before pop_front, front, pop_back etc..
*
*/

#include <iostream>
#include <mutex>
#include <deque>            // deque is a double sided buffer, pop_front, pop_back etc.

template <class T>
class queue
{
public:
    //no initializer needed because mutex and vector have initializers themself
    queue() = default;
    //delete copy constuctor, because you never want to copy the buffer, you will always want to work on this buffer and not a copy
    queue(const queue<T> &) = delete;
    ~queue();

    //acces front or back element
    T &front();
    T &back();

    bool empty();
    std::size_t size();

    void push_back(const T &value);
    T pop_back();

    T pop_front();

    void resize(const std::size_t size);
    void clear();

private:
    std::mutex mutexQueue;
    std::deque<T> _queue;
    //https://www.cplusplus.com/reference/deque/deque/
};

//define member function outside class, use scope resolution operator (::)

/*
template <class T>
queue<T>::queue()
{
}
*/

template <class T>
queue<T>::~queue()
{
    clear();
}

// function cannot be const because: Precisely, the _mutex object will change it's internal state from say "unlocked" to "locked" state
template <class T>
//const T& queue<T>::front()
T &queue<T>::front()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.front();
}

template <class T>
//const T& queue<T>::back() otheriwse works not in connection.hpp with boost::asio::buffer(). @todo remove const char
T &queue<T>::back()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.back();
}

template <class T>
bool queue<T>::empty()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.empty();
}

//number of elements in the queue
template <class T>
std::size_t queue<T>::size()
{
    std::scoped_lock lock(mutexQueue);
    return _queue.size();
}

template <class T>
void queue<T>::push_back(const T &value)
{
    std::scoped_lock lock(mutexQueue);
    _queue.push_back(value);
}

template <class T>
T queue<T>::pop_back()
{
    std::scoped_lock lock(mutexQueue);
    //back)() returns reference, the reference (the actual value. not the address) is copied into x
    auto x = _queue.back();
    _queue.pop_back();
    return x;
}

// if called and no data in queue then segmenattion fault if no data in the queue! take care of this
template <class T>
T queue<T>::pop_front()
{
    if (!_queue.empty())
    {
        std::scoped_lock lock(mutexQueue);
        auto x = std::move(_queue.front());
        _queue.pop_front();
        return x;
    }
    else
    {
        std::cout << "-- log [queue.hpp] [pop_front()] queue is empty thus not possible to pop anything" << std::endl;
        //@todo, what to return?
    }
}

template <class T>
void queue<T>::clear()
{
    std::scoped_lock lock(mutexQueue);
    _queue.clear();
}

#endif //QUEUE_HPP