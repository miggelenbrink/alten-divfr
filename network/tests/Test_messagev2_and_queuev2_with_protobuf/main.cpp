#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <boost/asio.hpp>
#include "s_message.hpp"
#include "message.pb.h"
#include "queue.hpp"

//compile: g++ -o main main.cpp message.pb.cc -std=c++17 -pthread -lprotobuf -lpthread

/*
messageTypes:
for in msg_struct.

  protobufMessage type | class   | object |
  ---------------------+---------+--------+
  protoMsg1            | message |  msg1  |
  protoMsg2            |         |        |

*/

int main(int argc, char *argv[])
{
  s_message msg1;
  s_message msg2;

  msg1.set_age(22);
  msg1.set_img_fname("test_name");
  msg1.set_img_fformat("test_jpeg");
  msg1.set_image_data("data_data data");
  msg1.setprotoMsg1Struct();

  queue<msg_struct> queueServer;

  queueServer.push_back(msg1.getprotoMsg1Struct());
  msg2.parseprotoMsg1Struct(queueServer.pop_front());

  std::cout << msg2.age() << std::endl;
  std::cout << msg2.img_fname() << std::endl;
  std::cout << msg2.img_fformat() << std::endl;
  std::cout << msg2.image_data() << std::endl;

// ENABLE THIS ONLY, MOST EASY VERSION OF THE APPLICATION  
  // s_message msg1;
  // s_message msg2;

  // msg1.set_age(22);
  // msg1.set_img_fname("test_name");
  // msg1.set_img_fformat("test_jpeg");
  // msg1.set_image_data("data_data data");
  // msg1.setprotoMsg1Struct();

  // msg2.parseprotoMsg1Struct(msg1.getprotoMsg1Struct()); 

  // std::cout << msg2.age() << std::endl;
  // std::cout << msg2.img_fname() << std::endl;
  // std::cout << msg2.img_fformat() << std::endl;
  // std::cout << msg2.image_data() << std::endl;

  return 0;
}