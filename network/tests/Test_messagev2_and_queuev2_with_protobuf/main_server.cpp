#include <cstdlib>
#include <iostream>
#include <memory>
#include <utility>
#include <boost/asio.hpp>

#include "message.hpp"

//       g++ main.cpp -o main -std=c++17 -pthread

using boost::asio::ip::tcp;

class session
{
public:
    session(tcp::socket socket)
        : socket_(std::move(socket))
    {
    }

    void start()
    {
        do_read();
    }

private:
    void do_read()
    {
        socket_.async_read_some(boost::asio::buffer(data_, max_length),
                                [this](boost::system::error_code ec, std::size_t length) {
                                    if (!ec)
                                    {
                                        do_write(length);
                                    }
                                });
    }

    void do_write(std::size_t length)
    {
        boost::asio::async_write(socket_, boost::asio::buffer(data_, length),
                                 [this](boost::system::error_code ec, std::size_t /*length*/) {
                                     if (!ec)
                                     {
                                         do_read();
                                     }
                                 });
    }

    tcp::socket socket_;
    enum
    {
        max_length = 1024
    };
    char data_[max_length];
};

class server
{
public:
    server(short port)
        : acceptor_(_context, tcp::endpoint(tcp::v4(), port))
    {
        start_server();
    }

    ~server()
    {
        //close context
        _context.stop();

        if (m_threadContext.joinable())
            m_threadContext.join();

        std::cout << "-- log: server stopped" << std::endl;
    }

    bool onconnectClient()
    {
        return true;
    }

    void start_server()
    {
        do_accept();
        m_threadContext = std::thread([this]() { _context.run(); });
    }

private:
    void do_accept()
    {
        acceptor_.async_accept(
            [this](boost::system::error_code ec, tcp::socket socket) {
                if (!ec)
                {
                    std::shared_ptr<session> tmp_conn = std::make_shared<session>(std::move(socket));

                    if (onconnectClient())
                    {
                        std::cout << "connection client ACCEPTED" << std::endl;
                        //start session/connection object
                        tmp_conn->start();
                        session_client = tmp_conn;
                    }
                    else
                    {
                        std::cout << "connection client DENIED" << std::endl;
                    }
                }

                do_accept();
            });
    }

    boost::asio::io_context _context;
    tcp::acceptor acceptor_;
    std::thread m_threadContext;
    std::shared_ptr<session> session_client;
};

int main(int argc, char *argv[])
{
    try
    {
        if (argc != 2)
        {
            std::cerr << "Usage: async_tcp_echo_server <port>\n";
            return 1;
        }

        server server_(std::atoi(argv[1]));

        while (1)
        {
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}