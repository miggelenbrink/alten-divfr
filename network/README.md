# DIVFR network communication library 
A client and server library that enables structured data exchange over TCP/IP sockets. The client and server lib use both Protocol buffers to serialize structured data and parse binary data back into structured data. Custom protobuf data structures can easily be added to the library, therefore a client and server application can exchange any kind of data, aslong as protobuf supports it.  

The repository contians 4 libraries:   

- client_lib    
- server_lib   
- client_ssl_lib    
- server_ssl_lib    

The client_lib and server_lib can be used to exchange data over the TCP/IP transport protocols. The client_ssl_lib and server_ssl_lib also use the TCP/IP transport protocols. But, with on top of that the Transport Layer Security (TLS) protocol for an encrypted data channel.   

In the examples folder is an example application for each library. The example applications use one of the libaries to exchange messages. 

The data variables that it exchanges is in the c_message (see c_message.hpp and c_message.cpp) class which is an wrapper around a generated protobuf data structure, see message.proto. This message can be exchanged between a client* and server* example application. The message is a data structure that holds an age, image name, image format and an image. The protobuf data structure message.proto is not included in the library itself. A developer must generate a protobuf C++ class and wrap it with a c_message.hpp file. In this way a developer can add as much custom protobuf data structure as he wants to exchange between a client and server application. 

In this example the client can send an image to the server (see the concolse, which is printed to the screen at startup), the image is in the /client_*/app folder. The server receives the image and stores it in the /server_*/app folder. 

The 4 libraries are in the network_libs folder. Each library has a include folder which contains a *_interface.hpp file. This is the API of the library. For more information about the API's, see the Doxygen documentation. The doxygen documentation is only generated for the client_ssl_lib and server_ssl_lib, but it is also a good reference for the client_lib and server_lib.   
  
**gitlab (Displays the generated README.md):**
https://gitlab.com/miggelenbrink/alten-divfr  
  
## Installation
This section describes the build of the DIVFR network communication library and the example applications. The libraries were tested on x86_64 with as operating system Ubuntu 20.04. 

**Requirements**

 - Visual Studio 2019
 - OpenSSL 1.1.1f
 - Boost (Boost.Asio) 1.75.0
 - Protocol Buffers 3.15.5
 - CMake 3.16.3
 - GCC 9.3.0 compiler  
   
After installing, make sure the library versions are as above specified.   

**Configure Visual Studio and CMake**  
Make sure you set the GCC 9.3.0 compiler and make sure that CMake can find all library directories, also the dev-libraries.  
We installed the libraries in the default location '/usr', which is part of LD_LIBRARY_PATH. LD_LIBRARY_PATH tells the linker where to find shared libraries.

**OpenSSL**  
The easiest way to get OpenSSL for ubuntu is:  
```
sudo apt-get install openssl
sudo apt-get install -y libssl-dev
```

**Boost**  
The easiest way to get Boost for ubuntu is:  
```
sudo apt install libboost-dev
sudo apt install libboost-all-dev

go to the boost site, download boost_1_76_0 or version 75
go to the usr/local folder
and extract the .tar.bz2 here! 
example
sudo tar --bzip2 -xf /home/guido/Downloads/boost_1_76_0.tar.bz2 


```

**Protocol Buffers**   
or



This project is fully C++, therefore we install the C++ Protocol Buffers.  
The easiest way to get Protocol Buffers for ubuntu is to following the install, see, [c++ install](https://github.com/protocolbuffers/protobuf/blob/master/src/README.md)


If you create a custom data structure with Protobuf in a .proto file, then compile the .proto file with the command:  
`protoc -I=$SRC_DIR --cpp_out=$DST_DIR $SRC_DIR/<fname>.proto`
- SRC_DIR:  folder where the .proto file is.  
- DST_DIR:  folder where you want the generated data structure (.cpp file)  
  
Easiest is to generate the .cpp file in the same location as the .proto file and copy the files manualy if needed.  
`protoc -I="." --cpp_out="." <file_name>.proto`  

**CMake**  
The easiest way to get CMake for ubuntu is:    
```
sudo apt-get update
sudo apt-get install cmake
```

**DIVFR network communication library**  
1) Pull this git repository.  
2) Add the client_lib, server_lib, client_ssl_lib or server_ssl_lib to your project folder.  
3) Create an application file `<lib_name>/app/<app_name.cpp>`.  
4) Set the below includes in your `<app_name>.cpp` application, make sure your compiler can find the files.  
```
#include "common.hpp"
#include "c_message.hpp"
#include "queue.hpp"
#include "connection.hpp"
#include "*_interface.hpp"
```
5) Set the variable TARGET_NAME in the CMakeLists.txt to `<app_name>`.  
6) Create an application class (child class) that inherits the *_interface class (API).  

   Or use the API directly by creating an object of the type *_interface. Then you only have to include the "*_interface.hpp". 

See in the Doxgen documentation how to use the API.  
And inspect the examples, which can be used as framework for your application.    

7) Generate a '<name>.proto file', see [Protobuf](https://developers.google.com/protocol-buffers).  
   Compile the .proto file as explaind in the _Protocol Buffers_ header. Place the source file in the src folder and the header file in the include folder.
   Go to the c_message.hpp if you use the client* library or go to the s_message.hpp if you use the server* library and include the generated protobuf header file `#include <name>.pb.h`.
   Next let the c_message class inherit from the generated protobuf data strucuture.
   This can be done by changing the line: `class c_message : public divfr::data`
   to `class c_message : public <class_name>`.
   The `class_name` is the name of the generated protobuf C++ class in the `<name>.pb.h` file.   

Now the application is ready to be build and executed.  

**Build target and execute**  
If you setup the environment, there are two options to build and execute the target.
1) Go to the 'build' folder which is in the application's root folder, and run the following commands:
 
```
    > cmake ..   
    > cmake build .   
    > ./../main  
```

2) Open the library's root folder in Visual Studio and run a CMake Delete Cache and Reconfigure, subsequently run CMake clean build.  

```
    > In Visual Studio do: ctrl + shift + p and search for "CMake Delete  
    > Cache and Reconfigure" and hit enter. Next, in the blue status bar at the bottom set  
    > the active target. Next, also in the blue status bar, hit the play button to build and launch the selected target.   
```

**Build target on command line**  
If you want to build the target on the command line, include the following flags:  

`g++ -I /usr/local/boost_1_75_0 main.cpp -o main -std=c++17 -pthread -lcrypto -lssl -lprotobuf  `

**COMPILER FLAGS**  
```
-std=c++17                    C++ standard, mutex/locking functionality is in the c++17 standard  
-pthread                      Adds support for multithreading with the pthreads library, This option sets flags for both the preprocessor and linker.  
-I /usr/local/boost_1_75_0    (*optional) Compiler needs the boost library path  
_Do this for all libraries that the compiler cannot find_
```

**LINKER FLAGS**  
```
-lcrypto    boost depends on crypto library  
-lssl       boost depends on the OpenSSL library  
-lprotobuf  the application depends/uses the protobuf library, thus link it into the executable  
-lpthread   optional, if you don't use -pthread  
```

## File structure
The general file structure for each part (library or application) in this project is:

/\<name>      
/\<name>/app  
/\<name>/include  
/\<name>/build  
/\<name>/src  
/\<name>/test  


'\<name>' can be the project-, library- or application-name. The 'app' directory contains the main application code. The 'include' directory contains all header files the library needs and the 'src' directory contains all corresponding source files. CMake stores all generated files in the build folder. Also the build system is stored here, which is 'Make' in this case. The target will be build in the root folder of the library, this is specefied in the 'src/CMakeList.txt' file.

This project uses CMake to generate a build system, therefore all directories with source code have a 'CMakeList.txt' file.   

In this repository, the file structure is:   

``` 
DIVFR  
+-- face_recognition  
    +-- docs  
    +-- examples  
    +-- face_recognition_app  
    +-- tests  
    +-- README.md  
+-- network  
    +-- docs  
    +-- examples  
    +-- network_libs  
    +-- tests  
    +-- README.md  
+-- README.md  
+-- .gitignore  
```

## License     
The '.gitignore' file is generated by https://www.toptal.com/developers/gitignore  


## Links  
[Boost.Asio](https://www.boost.org/doc/libs/1_75_0/doc/html/boost_asio.html)  
[Protobuf](https://developers.google.com/protocol-buffers)  
[CMake](https://cmake.org/)  
[OpenSSL](https://www.openssl.org/)  


## Note      
**Add include path in visual studio:**  

If include paths to the boost.asio library are not recognised.   
In visual studio code, add the include path to the boost.asio library "boost_1_75_0".  

`Go to: file --> preferences --> settings --> Extensions --> C/C++ --> c_cpp Default:Include path --> Edit in settings.json.`  

The json file is opend, subsequent add the path to the boost library to the file.      

In my case i added:     
> "/usr/local/",  
> "/usr/local/boost_1_75_0/"  


The settings.json file has to following content:     

```
{
    "explorer.confirmDelete": false,
    "C_Cpp.default.includePath": [
        "/usr/local/",
        "/usr/local/boost_1_75_0/"
    ]
}
```

**Delete "final" keyword in protobuf's generated classes**  

only if the compiler complains about the "final" keyword.

**Install boost.asio 1.75.1 manualy**  
--------------------------------
1   Download latest boost library: https://www.boost.org/users/history/version_1_75_0.html  
2   In the directory usr/local/ execute "tar --bzip2 -xf /path/to/boost_1_75_0.tar.bz2"  

done!  
  
How to compile:  
c++ -I /usr/local/boost_1_75_0 <name_app>.cpp -o <name_app>




